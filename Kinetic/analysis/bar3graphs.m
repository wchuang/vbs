function bar3graphs
% This function produces 3D graphs for OC or OM, vapor or suspended.
% This function is currently commented out in the experiment files.

global Cs Param

%% Test to get 3D bar graph for vapor OM
%{
Cs.C_OM_vap_2Dsize_all = reshape(Cs.C_OM_vap_2D_all, Cs.cols2D, [])';

% 0N vapor
figure
bar3(Cs.C_OM_vap_2Dsize_all(1:Cs.rows2D, 1:Cs.cols2D));
% Standard axes
axis([0 Cs.cols2D 0 Cs.rows2D+1 0 max(max(Cs.C_OM_vap_2Dsize_all(1:Cs.rows2D, 1:Cs.cols2D)))*1.5]);
xlabel('log_{10}C* (\mug m^{-3})');
ylabel('O:C');
zlabel('C_{OM} (\mug m^{-3})');
set(gca, 'XTickLabel', Param.volatilityRange(1):1:Param.volatilityRange(2));
set(gca, 'YTickLabel', Cs.O2C);
% Save function
saveas(gcf,['figs/' '3Dbar_vapor_0N' '.pdf'],'pdf');

% 1N vapor
figure
bar3(Cs.C_OM_vap_2Dsize_all(Cs.rows2D+1 : Cs.rows2D*(Param.layersOfN+1), 1:Cs.cols2D));
% Standard axes
axis([0 Cs.cols2D 0 Cs.rows2D+1 0 max(max(Cs.C_OM_vap_2Dsize_all(Cs.rows2D+1 : Cs.rows2D*(Param.layersOfN+1), 1:Cs.cols2D)))*1.5]);
xlabel('log_{10}C* (\mug m^{-3})');
ylabel('O:C');
zlabel('C_{OM} (\mug m^{-3})');
set(gca, 'XTickLabel', Param.volatilityRange(1):1:Param.volatilityRange(2));
set(gca, 'YTickLabel', Cs.O2C);
% Save function
saveas(gcf,['figs/' '3Dbar_vapor_1N' '.pdf'],'pdf');
%}
%% Test to get 3D bar graph for susp OM
Cs.C_OM_susp_2Dsize_all = reshape(Cs.C_OM_susp_2D_all, Cs.cols2D, [])';

% 0N susp
figure
bar3(Cs.C_OM_susp_2Dsize_all(1:Cs.rows2D, 1:Cs.cols2D));
% Standard axes
axis([0 Cs.cols2D 0 Cs.rows2D+1 0 max(max(Cs.C_OM_susp_2Dsize_all(1:Cs.rows2D, 1:Cs.cols2D)))*1.5]);
xlabel('log_{10}C* (\mug m^{-3})', 'Fontsize', 18);
ylabel('O:C', 'Fontsize', 18);
zlabel('C_{OM} (\mug m^{-3})', 'Fontsize', 18);
set(gca, 'XTickLabel', Param.volatilityRange(1):1:Param.volatilityRange(2));
set(gca, 'YTickLabel', Cs.O2C);
title('10 Hours of Aging, 0N layer', 'Fontsize', 18);
% Save function
saveTightFigure(gcf,['figs/' '3Dbar_susp_0N_600min_100' '.pdf']);

% 1N susp
Cs.C_OM_susp_2Dsize_all = reshape(Cs.C_OM_susp_2D_all, Cs.cols2D, [])';
figure
bar3(Cs.C_OM_susp_2Dsize_all(Cs.rows2D+1 : Cs.rows2D*(Param.layersOfN+1), 1:Cs.cols2D));
% Standard axes
axis([0 Cs.cols2D 0 Cs.rows2D+1 0 max(max(Cs.C_OM_susp_2Dsize_all(Cs.rows2D+1 : Cs.rows2D*(Param.layersOfN+1), 1:Cs.cols2D)))*1.5]);
%axis([0 15 0 12 0 0.6]);
xlabel('log_{10}C* (\mug m^{-3})', 'Fontsize', 18);
ylabel('O:C', 'Fontsize', 18);
zlabel('C_{OM} (\mug m^{-3})', 'Fontsize', 18);
set(gca, 'XTickLabel', Param.volatilityRange(1):1:Param.volatilityRange(2));
set(gca, 'YTickLabel', Cs.O2C);
title('10 Hours of Aging, 1N layer', 'Fontsize', 18);
% Save function
saveTightFigure(gcf,['figs/' '3Dbar_susp_1N_600min_100' '.pdf']);

%{
%% Test to get 3D bar graph for tot OM
Cs.C_OM_tot_2Dsize_all = reshape(Cs.C_OM_tot_2D_all, Cs.cols2D, [])';

% 0N vapor
figure
bar3(Cs.C_OM_tot_2Dsize_all(1:Cs.rows2D, 1:Cs.cols2D));
% Standard axes
axis([0 Cs.cols2D 0 Cs.rows2D+1 0 max(max(Cs.C_OM_tot_2Dsize_all(1:Cs.rows2D, 1:Cs.cols2D)))*1.5]);
xlabel('log_{10}C* (\mug m^{-3})');
ylabel('O:C');
zlabel('C_{OM} (\mug m^{-3})');
set(gca, 'XTickLabel', Param.volatilityRange(1):1:Param.volatilityRange(2));
set(gca, 'YTickLabel', Cs.O2C);
set(get(ax,'Xlabel'),'FontSize',18);
set(get(ax,'Ylabel'),'FontSize',18);
% Save function
saveas(gcf,['figs/' '3Dbar_tot_0N' '.pdf'],'pdf');

% 1N vapor
figure
bar3(Cs.C_OM_tot_2Dsize_all(Cs.rows2D+1 : Cs.rows2D*(Param.layersOfN+1), 1:Cs.cols2D));
% Standard axes
axis([0 Cs.cols2D 0 Cs.rows2D+1 0 max(max(Cs.C_OM_tot_2Dsize_all(Cs.rows2D+1 : Cs.rows2D*(Param.layersOfN+1), 1:Cs.cols2D)))*1.5]);
xlabel('log_{10}C* (\mug m^{-3})');
ylabel('O:C');
zlabel('C_{OM} (\mug m^{-3})');
set(gca, 'XTickLabel', Param.volatilityRange(1):1:Param.volatilityRange(2));
set(gca, 'YTickLabel', Cs.O2C);
set(get(ax,'Xlabel'),'FontSize',18);
set(get(ax,'Ylabel'),'FontSize',18);
set(get(ax,'Zlabel'),'FontSize',18);
% Save function
saveas(gcf,['figs/' '3Dbar_tot_1N' '.pdf'],'pdf');
%}