function PSIExperiment

global results

commonChamberConstants


% PSI High COA 19-Jan-09 Experiment
C_prec = 225;   % 40 ppb to ug/m3
%C_prec = 325;   % ug/m3  about 1.44 x nominal
C_prec = C_prec * 1.04;


% Physical loss rate consatnts for AIDA
k_wall = 1/(6*oneHour);  % particle wall loss rate constant
k_vap = 0;   % irreversible vapor deposition to wall 
k_dil = 0;% dilution relative to k_basis



timeArray = (-5:.02:5);
startsArray = timeArray(1:length(timeArray)-1);

injectHONO = startsArray >= 0;

C_O3 = 75e-9 * oneAtm;
C_O3 = 60e-9 * oneAtm; % make it low to simulate slowish addition of apin
C_OH = 1e7 * injectHONO;
C_OH = 5e6 * injectHONO;

aPin_OH_Yield = 0.8;

aPineneAgingTimeseries(timeArray*oneHour,C_prec,C_O3,C_OH,n_frag,...
    k_prec,k_basis,k_het,k_vap,k_wall,k_dil,aPin_OH_Yield);

results.tau_Array = timeArray;

[results.t_exp results.COA_exp results.nPart_exp] = textread('data/PSI.dat',...
    '%f %f %f','headerlines',1);
[results.t_OtoC_exp results.OtoC_exp] = textread('data/PSI_OtoC.dat',...
    '%f %f','headerlines',1);

%results.t_OtoC_exp=nan;  results.OtoC_exp=nan;
graphNames;
% bar3graphs

figure
exptPlots_N(0,'psi','MUCHACHAS_Paper/',...
    'time from start of photochemistry (h)',[-5,5]);

figure

chamberPlot_N('PSI_Chamber','MUCHACHAS_Paper/',...
    'time from start of photochemistry (h)',[-5,5],'g-');


aPineneAgingTimeseries(timeArray*oneHour,C_prec,C_O3,0,n_frag,...
    k_prec,k_basis,k_het,k_vap,k_wall,k_dil,aPin_OH_Yield);
results.tau_Array = timeArray;

results.t_exp = nan;
results.COA_exp = nan;

chamberPlot_N('PSI_Chamber','MUCHACHAS_Paper/',...
    'time from start of OH aging (h)',[-5,5],'g--','PSIBoxes');