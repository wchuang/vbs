% AIDA Boxes

grayLevel = 0.8;


% Draw boxses on the plot for the ideal experiment indicating no OH 


dark1 = rectangle('Position',[-2, 0, 2, 150],'Curvature',[0,0],...
          'FaceColor',[1 1 1]* grayLevel,'LineStyle','none');
uistack(dark1, 'bottom'); 


%text(5,50,'Ideal Experiment','HorizontalAlignment','center','FontSize',18);
text(-1.5,140,'b)','HorizontalAlignment','center','FontSize',18);


set(gca,'Layer','top');  

ylim([0,150]);
