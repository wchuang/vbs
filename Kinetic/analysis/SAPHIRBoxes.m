% SAPHIR Boxes

grayLevel = 0.8;


% Draw boxses on the plot for the SAPHIR experiment indicating the times
% when the roof was open or closed and some other random things like the
% fan on.


fan1 = rectangle('Position',[-2, 0, 3, 25],'Curvature',[0,0],...
          'FaceColor',[.5 1 1]* grayLevel,'LineStyle','none');
uistack(fan1, 'bottom'); 

dark1 = rectangle('Position',[-5, 0, 6, 60],'Curvature',[0,0],...
          'FaceColor',[1 1 1]* grayLevel,'LineStyle','none');
uistack(dark1, 'bottom'); 

dark2 = rectangle('Position',[4, 0, 6, 60],'Curvature',[0,0],...
          'FaceColor',[1 1 1]* grayLevel,'LineStyle','none');
uistack(dark2, 'bottom'); 



text(8,55,'SAPHIR','HorizontalAlignment','center','FontSize',18);
text(-4,55,'a)','HorizontalAlignment','center','FontSize',18);


text(-0.5,10,'fan','HorizontalAlignment','center','FontSize',14);


set(gca,'Layer','top');  

ylim([0,60]);



