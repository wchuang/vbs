function OM_OC_plot

global results

%% Plot of OM:OC 
% for all orgs together
figure
plot(results.tau_Array, results.OM_OC_susp_Array_all);
legend('OM/OC susp');

ax = gca;
set(ax,'FontSize',14);
set(get(ax,'Xlabel'),'String','time from start of OH aging (h)');
set(get(ax,'Xlabel'),'FontSize',18);
set(get(ax,'ylabel'),'String','OM:OC');
set(get(ax,'Ylabel'),'FontSize',18);
set(ax,'LineWidth',1.5);
set(ax,'XMinorTick','on');
set(ax,'TickLength',[.02,0.05]);

saveas(gcf,['figs\' 'OM_OC_susp_all' '.pdf'],'pdf');

%% for each N
figure
plot(results.tau_Array, results.OM_OC_susp_Array_N);
legend(results.suspOM_OCNames);

ax = gca;
set(ax,'FontSize',14);
set(get(ax,'Xlabel'),'String','time from start of OH aging (h)');
set(get(ax,'Xlabel'),'FontSize',18);
set(get(ax,'ylabel'),'String','OM:OC');
set(get(ax,'Ylabel'),'FontSize',18);
set(ax,'LineWidth',1.5);
set(ax,'XMinorTick','on');
set(ax,'TickLength',[.02,0.05]);

saveas(gcf,['figs\' 'OM_OC_susp_N' '.pdf'],'pdf');

%% Plot of OM
figure
plot(results.tau_Array, results.C_OM_susp_Array_N);
legend(results.OMsuspNames);

ax = gca;
set(ax,'FontSize',14);
set(get(ax,'Xlabel'),'String','time from start of OH aging (h)');
set(get(ax,'Xlabel'),'FontSize',18);
set(get(ax,'ylabel'),'String','C_{OM} (\mug m^{-3})');
set(get(ax,'Ylabel'),'FontSize',18);
set(ax,'LineWidth',1.5);
set(ax,'XMinorTick','on');
set(ax,'TickLength',[.02,0.05]);
title('10 Hours of Aging, \beta = 0.5', 'Fontsize', 18);

saveas(gcf,['figs\' 'OM_susp_N_beta0,5' '.pdf'],'pdf');

%% Plot of OC
figure
plot(results.tau_Array, results.C_OC_susp_Array_N);
legend(results.OCsuspNames);

ax = gca;
set(ax,'FontSize',14);
set(get(ax,'Xlabel'),'String','time from start of OH aging (h)');
set(get(ax,'Xlabel'),'FontSize',18);
set(get(ax,'ylabel'),'String','C_{OC} (\mug m^{-3})');
set(get(ax,'Ylabel'),'FontSize',18);
set(ax,'LineWidth',1.5);
set(ax,'XMinorTick','on');
set(ax,'TickLength',[.02,0.05]);

saveas(gcf,['figs\' 'OC_susp_N' '.pdf'],'pdf');
