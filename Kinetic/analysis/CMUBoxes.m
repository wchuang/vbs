% CMU Boxes

grayLevel = 0.8;


% Draw boxses on the plot for the ideal experiment indicating no OH 


dark1 = rectangle('Position',[-3, 0, 3, 60],'Curvature',[0,0],...
          'FaceColor',[1 1 1]* grayLevel,'LineStyle','none');
uistack(dark1, 'bottom'); 




text(1.5,55,'CMU','HorizontalAlignment','center','FontSize',18);
text(-2.75,55,'d)','HorizontalAlignment','center','FontSize',18);


set(gca,'Layer','top');  




