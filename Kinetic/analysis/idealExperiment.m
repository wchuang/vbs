function idealExperiment()

global results

commonChamberConstants

if nargin < 1, C_prec = 200; end;  % ug/m3

k_het = 6e-12;  % Heterogeneous rate constant on gas-phase basis

% Ideal High COA  Experiment

% Physical loss rate constants for AIDA
k_wall = 0;  % particle wall loss rate constant
k_vap = 0;   % irreversible vapor deposition to wall 
k_dil = 0;  % dilution relative to k_basis

timeArray = (-2:.02:10); % Chamber time from -2 hours to 10 hours, 
% where 0 hour marks the time UV lights are turned on in the chamber.
startsArray = timeArray(1:length(timeArray)-1);

injectOH = startsArray >= 0;

C_O3 = 300e-9 * oneAtm; % mlc O3/cm3 air
C_OH = 1e7 * injectOH; % mlc OH/cm3 air
aPin_OH_Yield = 0.8;

aPineneAgingTimeseries(timeArray*oneHour,C_prec,C_O3,C_OH,n_frag,...
    k_prec,k_basis,k_het,k_vap,k_wall,k_dil,aPin_OH_Yield);

results.tau_Array = timeArray;

results.t_exp      = nan;  results.COA_exp  = nan;
results.t_OtoC_exp = nan;  results.OtoC_exp = nan;

figure
exptPlots_N(0,'ideal','MUCHACHAS/','time from start of photochemistry (h)');

figure
exptPlots_N(1,'ideal_C','MUCHACHAS/','time from start of photochemistry (h)');


figure
chamberPlot_N('ideal_Chamber','MUCHACHAS_Paper/',....
    'time from start of photochemistry (h)',[-2,10],'g--');

%-----------------------------------

k_het = 0e-12;  % Heterogeneous rate constant on gas-phase basis

aPineneAgingTimeseries(timeArray*oneHour,C_prec,C_O3,C_OH,n_frag,...
    k_prec,k_basis,k_het,k_vap,k_wall,k_dil,aPin_OH_Yield);

results.tau_Array = timeArray;

chamberPlot_N('ideal_Chamber','MUCHACHAS_Paper/',....
    'time from start of OH aging (h)',[-2,10],'g-');

%------------------------------------------------------------------------

C_prec = 50;

% A Robinson-like mechanism
n_frag = 10;   % frag exponent
k_het = 0e-12;  % Heterogeneous rate constant on gas-phase basis

aPineneAgingTimeseries(timeArray*oneHour,C_prec,C_O3,C_OH,n_frag,...
    k_prec,k_basis,k_het,k_vap,k_wall,k_dil,aPin_OH_Yield);

results.tau_Array = timeArray;

chamberPlot_N('ideal_Chamber','MUCHACHAS_Paper/',....
    'time from start of OH aging (h)',[-2,10],'b-.','idealBoxes');

n_frag = 1/4;   % frag exponent
k_het = 6e-12;  % Heterogeneous rate constant on gas-phase basis

aPineneAgingTimeseries(timeArray*oneHour,C_prec,C_O3,C_OH,n_frag,...
    k_prec,k_basis,k_het,k_vap,k_wall,k_dil,aPin_OH_Yield);

results.tau_Array = timeArray;

chamberPlot_N('ideal_Chamber','MUCHACHAS_Paper/',....
    'time from start of OH aging (h)',[-2,10],'g--');

%----------------------------------

k_het = 0e-12;  % Heterogeneous rate constant on gas-phase basis

aPineneAgingTimeseries(timeArray*oneHour,C_prec,C_O3,C_OH,n_frag,...
    k_prec,k_basis,k_het,k_vap,k_wall,k_dil,aPin_OH_Yield);

results.tau_Array = timeArray;

chamberPlot_N('ideal_Chamber','MUCHACHAS_Paper/',....
    'time from start of OH aging (h)',[-2,10],'g-','idealBoxes');





