function barVolatilityGraphs

global Cs Param
%{
%% Vapor Phase
C_OM_vap_Cstar_N = reshape(Cs.C_OM_vap_2D_all, Cs.cols2D, [])';
C_OM_vap_Cstar_0N = sum(C_OM_vap_Cstar_N(1:Cs.rows2D, 1:Cs.cols2D), 1);
C_OM_vap_Cstar_1N = sum(C_OM_vap_Cstar_N(Cs.rows2D+1:Cs.rows2D*2, 1:Cs.cols2D), 1);
C_OM_vap_Cstar_all = sum(C_OM_vap_Cstar_N, 1);

figure
bar(Param.volatilityRange(1):1:Param.volatilityRange(2), C_OM_vap_Cstar_0N);
xlabel('log_{10}C* (\mug m^{-3})');
ylabel('C_{OM} (\mug m^{-3})');

figure
bar(Param.volatilityRange(1):1:Param.volatilityRange(2), C_OM_vap_Cstar_1N);
xlabel('log_{10}C* (\mug m^{-3})');
ylabel('C_{OM} (\mug m^{-3})');

figure
bar(Param.volatilityRange(1):1:Param.volatilityRange(2), C_OM_vap_Cstar_all);
xlabel('log_{10}C* (\mug m^{-3})');
ylabel('C_{OM} (\mug m^{-3})');
%}
%% Suspended Phase
global Cs Param
C_OM_susp_Cstar_N = reshape(Cs.C_OM_susp_2D_all, Cs.cols2D, [])';
C_OM_susp_Cstar_0N = sum(C_OM_susp_Cstar_N(1:Cs.rows2D, 1:Cs.cols2D), 1);
C_OM_susp_Cstar_1N = sum(C_OM_susp_Cstar_N(Cs.rows2D+1:Cs.rows2D*2, 1:Cs.cols2D), 1);
C_OM_susp_Cstar_all = sum(C_OM_susp_Cstar_N, 1);

figure
bar(Param.volatilityRange(1):1:Param.volatilityRange(2), C_OM_susp_Cstar_0N);
xlabel('log_{10}C* (\mug m^{-3})', 'FontSize', 18);
ylabel('C_{OM} (\mug m^{-3})','FontSize', 18);

figure
bar(Param.volatilityRange(1):1:Param.volatilityRange(2), C_OM_susp_Cstar_1N);
xlabel('log_{10}C* (\mug m^{-3})','FontSize', 18);
ylabel('C_{OM} (\mug m^{-3})','FontSize', 18);

figure
bar(Param.volatilityRange(1):1:Param.volatilityRange(2), C_OM_susp_Cstar_all);
xlabel('log_{10}C* (\mug m^{-3})','FontSize', 18);
ylabel('C_{OM} (\mug m^{-3})','FontSize', 18);

bar(Param.volatilityRange(1):1:Param.volatilityRange(2), C_OM_susp_Cstar_all);
bar(C_OM_susp_Cstar_all');
xlabel('log_{10}C* (\mug m^{-3})','FontSize', 18);
ylabel('C_{OM} (\mug m^{-3})','FontSize', 18);
legend('$\frac{[VOC]_0}{[NO_x]_0} = 10$', '$\frac{[VOC]_0}{[NO_x]_0} = 4$', '$\frac{[VOC]_0}{[NO_x]_0} = 1$', '$\frac{[VOC]_0}{[NO_x]_0} = 0.5$')
set(legend(),'interpreter','latex');
