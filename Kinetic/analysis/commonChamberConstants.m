% commonChamberConstants

% Common values to all experiments

oneHour = 3600;
oneAtm = 2.5e19; % mlc/cm3

%C_prec = 300;   % ug/m3
n_frag = 1/6;   % frag exponent


% Rate constants are either 1/s or cm3/molec-s
k_prec = 8e-17; % precursor rate constant
k_basis = 3e-11;% rate constant in basis set 
k_het = 6e-12;  % Heterogeneous rate constant on gas-phase basis


% Physical loss rate constants for chamber
k_wall = 1/(3*oneHour);  % particle wall loss rate constant
k_vap = 0;   % irreversible vapor deposition to wall 
k_dil = 0;% dilution relative to k_basis
