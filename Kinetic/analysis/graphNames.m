function graphNames

global Param results

NArray = 0:Param.layersOfN;

NArrayCell = num2cell(num2str(NArray, '%-d'));

results.OCsuspNames = strcat('OC susp', {' '}, NArrayCell, 'N');
results.OCvapNames  = strcat('OC vap' , {' '}, NArrayCell, 'N');
results.OCwallNames = strcat('OC wall', {' '}, NArrayCell, 'N');
results.OCadsNames  = strcat('OC ads' , {' '}, NArrayCell, 'N');
results.OCtotNames  = strcat('OC tot' , {' '}, NArrayCell, 'N');

results.OMsuspNames = strcat('OM susp', {' '}, NArrayCell, 'N');
results.OMvapNames  = strcat('OM vap' , {' '}, NArrayCell, 'N /10');
results.OMwallNames = strcat('OM wall', {' '}, NArrayCell, 'N');
results.OMadsNames  = strcat('OM ads' , {' '}, NArrayCell, 'N');
results.OMtotNames  = strcat('OM tot' , {' '}, NArrayCell, 'N');

results.suspOtoCNames  = strcat('susp O:C', {' '}, NArrayCell, 'N');
results.suspOM_OCNames  = strcat('susp OM:OC', {' '}, NArrayCell, 'N');