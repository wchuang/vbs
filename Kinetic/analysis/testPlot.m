function testPlot

global results Cs Param

%% Plot of vap and susp OM for each N layer with respect to time
%{
figure
plot(results.tau_Array, results.C_OM_susp_Array_N, results.tau_Array, results.C_OM_vap_Array_N/10);

legend([results.OMsuspNames results.OMvapNames]);

% Plot vap and susp OM for all N wrt time
figure
plot(results.tau_Array, results.C_OM_susp_Array_all);
legend('OM susp');

%% Plot of carbon mass reacted wrt time
%{
figure
plot(results.tau_Array, results.C_OC_tot_Array_all, 'r');
legend('total OC reacted');
%}
%% Plot of susp O:C wrt time

figure
plot(results.tau_Array, results.OtoC_susp_Array_N);
legend(results.suspOtoCNames);

figure
plot(results.tau_Array, results.OtoC_susp_Array_all);
legend('O:C susp');

%% Plot of vap and susp OC wrt time
figure
plot(results.tau_Array, results.C_OC_susp_Array_all, results.tau_Array, results.C_OC_vap_Array_all/10);
legend('OC susp', 'OC vap/10');

%% Plot of OM:OC 
figure
plot(results.tau_Array, results.OM_OC_susp_Array_all);
legend('OM/OC susp');
saveas(gcf,['figs/' 'OM_OC_susp_all' '.pdf'],'pdf');

figure
plot(results.tau_Array, results.OM_OC_susp_Array_N);
legend(results.suspOM_OCNames);
saveas(gcf,['figs/' 'OM_OC_susp_N' '.pdf'],'pdf');

%% Summing over all O:C to get mass in each C*

C_OM_vap_Cstar = sum(reshape(Cs.C_OM_vap_2D_all, Cs.cols2D, [])', 1);
OM_susp_vol = sum(reshape(Cs.C_OM_susp_2D_all, Cs.cols2D, [])',1);
bar(Param.volatilityRange(1):1:Param.volatilityRange(2), OM_susp_vol);
ylabel('C_{OM} (\mug m^{-3})');
xlabel('log_{10}C* (\mug m^{-3})');

bar3graphs; % separate file
% bar(Param.volatilityRange(1):1:Param.volatilityRange(2), uniform, Param.volatilityRange(1):1:Param.volatilityRange(2), SAR);
%% Plot of susp OM for each N
figure
plot(results.tau_Array, results.C_OM_susp_Array_N);
legend(results.OMsuspNames);
ax = gca;
set(ax,'FontSize',14);
set(get(ax,'Xlabel'),'String','time from start of OH aging (h)');
set(get(ax,'Xlabel'),'FontSize',18);
set(get(ax,'ylabel'),'String','C_{OM} (\mug m^{-3})');
set(get(ax,'Ylabel'),'FontSize',18);
set(ax,'LineWidth',1.5);
set(ax,'XMinorTick','on');
set(ax,'TickLength',[.02,0.05]);
saveas(gcf,['figs/' 'OM_susp_N' '.pdf'],'pdf');
%}

%% Graph for paper: graph 3D bar graph; 10 hour aging; 0N, 1N layers
%{
precursor_loading = 100;
beta = .15;
Cs.beta = beta;
time = 10; % hours of OH aging
idealExperiment_plotting(precursor_loading, time);

Cs.C_OM_susp_2Dsize_all = reshape(Cs.C_OM_susp_2D_all, Cs.cols2D, [])';

NVOCmin = Param.volatilityRange(1) - 0.5;
NVOCwidth = -3.5 - Param.volatilityRange(1) + 0.5;
LVOCmin = -3.5;
LVOCwidth = 3.0;
SVOCmin = -0.5;
SVOCwidth = 3.0;
IVOCmin = 2.5;
IVOCwidth = 4.0;

OScmin = -2.15;
OScwidth = 6;

grayLevel = 0.75;

% 0N susp
figure
a = bar3(Cs.C_OM_susp_2Dsize_all(1:Cs.rows2D, 1:Cs.cols2D));
set(a,'linewidth', 1.5);
% Standard axes
axis([0 Cs.cols2D 0 Cs.rows2D+1 0 max(max(Cs.C_OM_susp_2Dsize_all(1:Cs.rows2D, 1:Cs.cols2D)))*1.5]);

xlabel('log_{10}C* (\mug m^{-3})', 'Fontsize', 20);
ylabel('O:C', 'Fontsize', 18);
zlabel('C_{OM} (\mug m^{-3})', 'Fontsize', 20);
set(gca, 'XTickLabel', Param.volatilityRange(1):1:Param.volatilityRange(2));
set(gca, 'YTickLabel', Cs.O2C);
set(gca, 'fontsize', 14);
% set colors of the bar graph
set(a(1: NVOCwidth), 'facecolor', [1 1 1]*grayLevel);
set(a(NVOCwidth+1: NVOCwidth+LVOCwidth), 'facecolor', [1 grayLevel grayLevel]);
set(a(NVOCwidth+LVOCwidth+1: NVOCwidth+LVOCwidth+SVOCwidth), 'facecolor', [grayLevel 1 grayLevel]);  
set(a(NVOCwidth+LVOCwidth+SVOCwidth+1: NVOCwidth+LVOCwidth+SVOCwidth+IVOCwidth), 'facecolor', [grayLevel grayLevel 1]);
set(a(NVOCwidth+LVOCwidth+SVOCwidth+IVOCwidth+1: Cs.cols2D), 'facecolor', [1 1 1]);
% title('10 Hours of Aging, 0N layer', 'Fontsize', 18);
% Save function
saveTightFigure(gcf,['figs/' '3Dbar_susp_0N_600min_100_beta15_untitled_presentation' '.pdf']);

% 1N susp
Cs.C_OM_susp_2Dsize_all = reshape(Cs.C_OM_susp_2D_all, Cs.cols2D, [])';
figure
a = bar3(Cs.C_OM_susp_2Dsize_all(Cs.rows2D+1 : Cs.rows2D*(Param.layersOfN+1), 1:Cs.cols2D));
set(a,'linewidth', 1.5);
% Standard axes
%axis([0 Cs.cols2D 0 Cs.rows2D+1 0 max(max(Cs.C_OM_susp_2Dsize_all(Cs.rows2D+1 : Cs.rows2D*(Param.layersOfN+1), 1:Cs.cols2D)))*1.5]);
axis([0 Cs.cols2D 0 Cs.rows2D+1 0 1.5]);
%axis([0 15 0 12 0 0.6]);
xlabel('log_{10}C* (\mug m^{-3})', 'Fontsize', 18);
ylabel('O:C', 'Fontsize', 18);
zlabel('C_{OM} (\mug m^{-3})', 'Fontsize', 20);
set(gca, 'XTickLabel', Param.volatilityRange(1):1:Param.volatilityRange(2));
set(gca, 'YTickLabel', Cs.O2C);
set(gca, 'fontsize', 6);
% set colors of the bar graph
set(a(1: NVOCwidth), 'facecolor', [1 1 1]*grayLevel);
set(a(NVOCwidth+1: NVOCwidth+LVOCwidth), 'facecolor', [1 grayLevel grayLevel]);
set(a(NVOCwidth+LVOCwidth+1: NVOCwidth+LVOCwidth+SVOCwidth), 'facecolor', [grayLevel 1 grayLevel]);  
set(a(NVOCwidth+LVOCwidth+SVOCwidth+1: NVOCwidth+LVOCwidth+SVOCwidth+IVOCwidth), 'facecolor', [grayLevel grayLevel 1]);
set(a(NVOCwidth+LVOCwidth+SVOCwidth+IVOCwidth+1: Cs.cols2D), 'facecolor', [1 1 1]);
% title('10 Hours of Aging, 1N layer', 'Fontsize', 18);
% Save function
%saveTightFigure(gcf,['figs/' '3Dbar_susp_1N_600min_100_beta15_untitled_presentation' '.pdf']);
saveas(gcf,['figs/' '3Dbar_susp_1N_600min_100_beta15_untitled_presentation' '.pdf'],'pdf');
%}

%% Graph for paper: graph 3D bar graph; 0 hour aging; susp, tot layers, Param.layersOfN = 1
%{
precursor_loading = 100; % ug/m3
beta = 0.15;
Cs.beta = beta;
time = 0; % hours of OH aging
idealExperiment_plotting(precursor_loading, time);

Cs.C_OM_susp_2Dsize_all = reshape(Cs.C_OM_susp_2D_all, Cs.cols2D, [])';

NVOCmin = Param.volatilityRange(1) - 0.5;
NVOCwidth = -3.5 - Param.volatilityRange(1) + 0.5;
LVOCmin = -3.5;
LVOCwidth = 3.0;
SVOCmin = -0.5;
SVOCwidth = 3.0;
IVOCmin = 2.5;
IVOCwidth = 4.0;

OScmin = -2.15;
OScwidth = 6;

grayLevel = 0.75;

% Total susp
Cs.C_OM_susp_2Dsize_all = reshape(Cs.C_OM_susp_2D_all, Cs.cols2D, [])';

figure
if Param.layersOfN > 0
    a = bar3(Cs.C_OM_susp_2Dsize_all(1:Cs.rows2D, 1:Cs.cols2D) + Cs.C_OM_susp_2Dsize_all(Cs.rows2D+1 : Cs.rows2D*(Param.layersOfN+1), 1:Cs.cols2D));
else
    a = bar3(Cs.C_OM_susp_2Dsize_all(1:Cs.rows2D, 1:Cs.cols2D));
end
set(a,'linewidth', 1.5);
% Standard axes
axis([0 Cs.cols2D 0 Cs.rows2D+1 0 0.2]);
%axis([0 Cs.cols2D 0 Cs.rows2D+1 0 max(max(Cs.C_OM_susp_2Dsize_all(1:Cs.rows2D, 1:Cs.cols2D) + Cs.C_OM_susp_2Dsize_all(Cs.rows2D+1 : Cs.rows2D*(Param.layersOfN+1), 1:Cs.cols2D)))*1.1]);
%axis([0 15 0 12 0 0.6]);
xlabel('log_{10}C* (\mug m^{-3})', 'Fontsize', 18);
ylabel('O:C', 'Fontsize', 18);
zlabel('C_{OM} (\mug m^{-3})', 'Fontsize', 18);
set(gca, 'XTickLabel', Param.volatilityRange(1):1:Param.volatilityRange(2));
set(gca, 'YTickLabel', Cs.O2C);
% set colors of the bar graph
set(a(1: NVOCwidth), 'facecolor', [1 1 1]*grayLevel);
set(a(NVOCwidth+1: NVOCwidth+LVOCwidth), 'facecolor', [1 grayLevel grayLevel]);
set(a(NVOCwidth+LVOCwidth+1: NVOCwidth+LVOCwidth+SVOCwidth), 'facecolor', [grayLevel 1 grayLevel]);  
set(a(NVOCwidth+LVOCwidth+SVOCwidth+1: NVOCwidth+LVOCwidth+SVOCwidth+IVOCwidth), 'facecolor', [grayLevel grayLevel 1]);
set(a(NVOCwidth+LVOCwidth+SVOCwidth+IVOCwidth+1: Cs.cols2D), 'facecolor', [1 1 1]);
%title('Ozone Aging Only, Condensed Phase', 'Fontsize', 18);
% Save function
saveTightFigure(gcf,['figs/' '3Dbar_susp_totalN_0min_100_beta100_untitled_test' '.pdf']);


% Total organics
Cs.C_OM_tot_2Dsize_all = reshape(Cs.C_OM_tot_2D_all, Cs.cols2D, [])';

figure
a = bar3(Cs.C_OM_tot_2Dsize_all(1:Cs.rows2D, 1:Cs.cols2D) + Cs.C_OM_tot_2Dsize_all(Cs.rows2D+1 : Cs.rows2D*(Param.layersOfN+1), 1:Cs.cols2D));
set(a,'linewidth', 1.5);
% Standard axes
axis([0 Cs.cols2D 0 Cs.rows2D+1 0 21]);
%axis([0 Cs.cols2D 0 Cs.rows2D+1 0 max(max(Cs.C_OM_tot_2Dsize_all(1:Cs.rows2D, 1:Cs.cols2D) + Cs.C_OM_tot_2Dsize_all(Cs.rows2D+1 : Cs.rows2D*(Param.layersOfN+1), 1:Cs.cols2D)))*1.1]);
%axis([0 15 0 12 0 0.6]);
xlabel('log_{10}C* (\mug m^{-3})', 'Fontsize', 18);
ylabel('O:C', 'Fontsize', 18);
zlabel('C_{OM} (\mug m^{-3})', 'Fontsize', 18);
set(gca, 'XTickLabel', Param.volatilityRange(1):1:Param.volatilityRange(2));
set(gca, 'YTickLabel', Cs.O2C);
% set colors of the bar graph
set(a(1: NVOCwidth), 'facecolor', [1 1 1]*grayLevel);
set(a(NVOCwidth+1: NVOCwidth+LVOCwidth), 'facecolor', [1 grayLevel grayLevel]);
set(a(NVOCwidth+LVOCwidth+1: NVOCwidth+LVOCwidth+SVOCwidth), 'facecolor', [grayLevel 1 grayLevel]);  
set(a(NVOCwidth+LVOCwidth+SVOCwidth+1: NVOCwidth+LVOCwidth+SVOCwidth+IVOCwidth), 'facecolor', [grayLevel grayLevel 1]);
set(a(NVOCwidth+LVOCwidth+SVOCwidth+IVOCwidth+1: Cs.cols2D), 'facecolor', [1 1 1]);
%title('Ozone Aging Only, Total Organics', 'Fontsize', 18);
% Save function
saveTightFigure(gcf,['figs/' '3Dbar_tot_totalN_0min_100_beta15_untitled' '.pdf']);
%}

% plot first gen volatility graph, 1D graph
%{
Cs.C_OM_susp_2Dsize_all = reshape(Cs.C_OM_susp_2D_all, Cs.cols2D, [])';
Cs.C_OM_tot_2Dsize_all = reshape(Cs.C_OM_tot_2D_all, Cs.cols2D, [])';
aerosols = sum(Cs.C_OM_susp_2Dsize_all, 1);
all = sum(Cs.C_OM_tot_2Dsize_all, 1) - aerosols;
tograph = [aerosols;all]';
tograph1 = tograph(4:13,:);
tograph2 = tograph(4:7,:);

%{
figure
hbar = bar(tograph2, 'stacked');
set(hbar,{'FaceColor'},{'g';'w'});
set(gca,'fontsize',14);
set(gca, 'XTickLabel', -2:1:1);
%xlabel('log_{10}C* (\mug m^{-3})', 'Fontsize', 18);
%ylabel('C_{OM} (\mug m^{-3})', 'Fontsize', 18);
%legend('Aerosol', 'Vapor', 'Location', 'Northwest');
saveTightFigure(gcf,['figs/' 'firstGen_volatility_shortened_untitled' '.pdf']);

figure
hbar = bar(tograph1, 'stacked');
set(gca,'fontsize',14);
set(hbar,{'FaceColor'},{'g';'w'});
set(gca, 'XTickLabel', -2:1:7);
set(gca,'XLim',[0.5 10.5]);
xlabel('log_{10}C* (\mug m^{-3})', 'Fontsize', 18);
ylabel('C_{OM} (\mug m^{-3})', 'Fontsize', 18);
legend('Aerosol', 'Vapor', 'Location', 'Northwest');
saveTightFigure(gcf,['figs/' 'firstGen_volatility_untitled' '.pdf']);
%}

figure
hbar = bar(tograph, 'stacked');
set(gca,'fontsize',14);
set(hbar,{'FaceColor'},{'g';'w'});
set(gca, 'XTickLabel', -5:1:9);
set(gca,'XLim',[0.5 15.5]);
xlabel('log_{10}C* (\mug m^{-3})', 'Fontsize', 18);
ylabel('C_{OM} (\mug m^{-3})', 'Fontsize', 18);
legend('Aerosol', 'Vapor', 'Location', 'Northwest');
saveTightFigure(gcf,['figs/' 'firstGen_lowNOx_volatility_untitled' '.pdf']);
%}
%}

%% Graph for paper: Plot 0 hour dark aging results for various beta 
%{
precursor_loading = [1e2 1e4 1e6]; % ug/m3
beta = [.001 .003 .01 .03 .05 .1 .2 .3 .4 .5 .6 .7 .8 .9 1];
time = 0; % hours of OH aging
OM = [];

for i = (1:length(precursor_loading))
    for j = (1:length(beta))
        Cs.beta = beta(j);
        idealExperiment_plotting(precursor_loading(i), time);
        OM_susp_vol = sum(sum(reshape(Cs.C_OM_susp_2D_all, Cs.cols2D, [])',1));
        OM(j,i) = OM_susp_vol;
    end
    OM(:,i) = OM(:,i) ./ OM(length(beta), i); % To get the ratio of beta = x to beta = 1
end

% After plotting in Matlab, use following code to get axes/labels/legends
figure('units','normalized','position',[0 0 .35 .5])
color = distinguishable_colors(5,'w');
Gx = plot(beta, OM, 'LineWidth', 4);
for i = 1:length(Gx)
    set(Gx(i),'Color',color(length(Gx)-i+1,:));
end
ax = gca;
set(ax,'FontSize',22);
ylabel('C_{OM} / C_{OM, low-NO_x}');
set(get(ax,'Ylabel'),'FontSize',18);
xlabel('low-NO_x                                                        high-NO_x');
set(get(ax,'Xlabel'),'FontSize',18);
axis([0 1 0 1.4]);
% add labels for NOx levels
%{
ylim=get(gca,'YLim');
xlim=get(gca,'XLim');
text(xlim(1),ylim(1),'Outside bottom left corner', 'Extent', [1,1,1,1],...
   'VerticalAlignment','top',...
   'HorizontalAlignment','right');
%}
% reverses the x-axis
set(gca, 'XDir', 'reverse');
legend('10^2 \mug m^{-3}', '10^4 \mug m^{-3}', '10^6 \mug m^{-3}', 'Location', 'SouthWest');

saveas(gcf,['figs/' 'OM_varyConc_varyBeta_0hours_presentation' '.pdf'],'pdf');
%saveTightFigure(gcf,['figs/' 'OM_varyConc_varyBeta_0hours' '.pdf']);
%}

%% Graph for paper: Plot 10 hour OH aging results for various beta 
%{
precursor_loading = [1e2 1e4 1e6]; % ug/m3
beta = [.001 .003 .01 .03 .05 .1 .2 .3 .4 .5 .6 .7 .8 .9 1];
time = 10; % hours of OH aging
OM = [];

for i = (1:length(precursor_loading))
    for j = (1:length(beta))
        Cs.beta = beta(j);
        idealExperiment_plotting(precursor_loading(i), time);
        OM_susp_vol = sum(sum(reshape(Cs.C_OM_susp_2D_all, Cs.cols2D, [])',1));
        OM(j,i) = OM_susp_vol;
    end
    OM(:,i) = OM(:,i) ./ OM(length(beta), i); % To get the ratio of beta = x to beta = 1
end

% After plotting in Matlab, use following code to get axes/labels/legends
figure('units','normalized','position',[0 0 .35 .5])
color = distinguishable_colors(7,'w');
Gx = plot(beta, OM, 'LineWidth', 4);
for i = 1:length(Gx)
    set(Gx(i),'Color',color(length(Gx)-i+1,:));
end
ax = gca;
set(ax,'FontSize',20);
ylabel('C_{OM} / C_{OM, low-NO_x}');
set(get(ax,'Ylabel'),'FontSize',18);
xlabel('low-NO_x                                                        high-NO_x');
set(get(ax,'Xlabel'),'FontSize',18);
axis([0 1 0 1.2]);
set(gca, 'XDir', 'reverse');
legend('10^2 \mug m^{-3}', '10^4 \mug m^{-3}', '10^6 \mug m^{-3}', ...
    'Location', 'SouthWest');
saveas(gcf,['figs/' 'OM_varyConc_varyBeta_10hours_presentation' '.pdf'],'pdf');
%saveTightFigure(gcf,['figs/' 'OM_varyConc_varyBeta_10hours' '.pdf']);
%}

%% Graph for paper: Plot total OM mass, OM 0N mass, OM 1N mass, NO2 mass for beta = 1 and beta = 0.15, 10 hour OH aging

precursor_loading = 1e2; % ug/m3
beta = [1 .15];
OM = [];
time = 10; % hours of OH aging

Cs.beta = 1;
idealExperiment_plotting(precursor_loading, time);
C_OM_beta1 = results.C_OM_susp_Array_all;

Cs.beta = 0.1;
idealExperiment_plotting(precursor_loading, time);

figure('units','normalized','position',[0 0 .45 .6])
plot(results.tau_Array, C_OM_beta1, 'k', 'LineWidth', 4);

hold on;
plot(results.tau_Array, results.C_OM_susp_Array_all, 'g', 'LineWidth', 4);
hold on;
plot(results.tau_Array, results.C_OM_susp_Array_N(1,:), 'Color', [0 0.5 0.5],'LineWidth', 4);
hold on;
plot(results.tau_Array, results.C_OM_susp_Array_N(2,:),'cyan','LineWidth', 4);
hold on;
plot( results.tau_Array,results.massOfNO2,'Color','blue','LineStyle','--','LineWidth', 4);

ax = gca;
set(ax,'FontSize',19);
legend('Total Mass low-NOx', 'Total Mass high-NOx', 'OM of non-nitrates', 'OM of organonitrates', 'Mass of Nitrates');

dark = rectangle('Position',[-2, 0, 2, 60],'Curvature',[0,0],...
          'FaceColor',[0.8 0.8 0.8],'LineStyle','none');
uistack(dark, 'bottom');

ax = gca;
set(gca,'layer','top')
set(get(ax,'Xlabel'),'String','time from start of OH aging (h)');
set(get(ax,'ylabel'),'String','C_{OM} (\mug m^{-3})');

set(get(ax,'Xlabel'),'FontSize',26);
set(get(ax,'Ylabel'),'FontSize',26);
set(ax,'LineWidth',1.5);
set(ax,'XMinorTick','on');
set(ax,'TickLength',[.02,0.05]);

set(ax,'LineWidth',1.5);

saveTightFigure(gcf,['figs/' 'OM_compare_wrt_t_beta15_untitled1' '.pdf']);

%% Plotting 1D volatility 1st generation yields
%{
precursor_loading = 100; % ug/m3
beta = 1;
Cs.beta = beta;
time = 0; % hours of OH aging
idealExperiment_plotting(precursor_loading, time);

OM_susp_vol = sum(reshape(Cs.C_OM_susp_2D_all, Cs.cols2D, [])',1);
OM_tot_vol = [sum(reshape(Cs.C_OM_susp_2D_all, Cs.cols2D, [])',1); sum(reshape(Cs.C_OM_vap_2D_all, Cs.cols2D, [])',1)];

figure
h = bar(Param.volatilityRange(1):1:Param.volatilityRange(2), OM_tot_vol', 'stacked');
ax = gca;
set(h(2), 'facecolor','white')
set(h(1), 'facecolor','green')
set(get(ax,'xlabel'),'String','log_{10} (C*) (saturation concentration, \mug m^{-3}');
set(get(ax,'ylabel'),'String','Organic Mass Conc (\mug m^{-3})');

figure
h = bar(Param.volatilityRange(1):1:Param.volatilityRange(2), OM_susp_vol', 'stacked');
ax = gca;
set(h(1), 'facecolor','green')
set(get(ax,'xlabel'),'String','log_{10} (C*) (saturation concentration, \mug m^{-3}');
set(get(ax,'ylabel'),'String','Organic Mass Conc (\mug m^{-3})');

%}

%% Plot empty 3d bar graph 
%{
NVOCmin = Param.volatilityRange(1) - 0.5;
NVOCwidth = -3.5 - Param.volatilityRange(1) + 0.5;
LVOCmin = -3.5;
LVOCwidth = 3.0;
SVOCmin = -0.5;
SVOCwidth = 3.0;
IVOCmin = 2.5;
IVOCwidth = 4.0;

OScmin = -2.15;
OScwidth = 6;

grayLevel = 0.75;

empty = zeros(11,15);

figure
a = bar3(empty);
set(a,'linewidth', 1.5);
% Standard axes
axis([0 Cs.cols2D 0 Cs.rows2D+1 0 0.001]);
%axis([0 15 0 12 0 0.6]);
%xlabel('log_{10}C* (\mug m^{-3})', 'Fontsize', 18);
%ylabel('O:C', 'Fontsize', 18);
%zlabel('C_{OM} (\mug m^{-3})', 'Fontsize', 18);
set(gca, 'XTickLabel', Param.volatilityRange(1):1:Param.volatilityRange(2));
set(gca, 'YTickLabel', Cs.O2C);
% set colors of the bar graph
set(a(1: NVOCwidth), 'facecolor', [1 1 1]*grayLevel);
set(a(NVOCwidth+1: NVOCwidth+LVOCwidth), 'facecolor', [1 grayLevel grayLevel]);
set(a(NVOCwidth+LVOCwidth+1: NVOCwidth+LVOCwidth+SVOCwidth), 'facecolor', [grayLevel 1 grayLevel]);  
set(a(NVOCwidth+LVOCwidth+SVOCwidth+1: NVOCwidth+LVOCwidth+SVOCwidth+IVOCwidth), 'facecolor', [grayLevel grayLevel 1]);
set(a(NVOCwidth+LVOCwidth+SVOCwidth+IVOCwidth+1: Cs.cols2D), 'facecolor', [1 1 1]);
%title('Ozone Aging Only, Condensed Phase', 'Fontsize', 18);
% Save function
saveTightFigure(gcf,['figs/' '3Dbar_empty_noaxis' '.pdf']);
%}
end