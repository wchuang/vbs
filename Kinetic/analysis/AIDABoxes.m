% AIDA Boxes

grayLevel = 0.8;


% Draw boxses on the plot for the AIDA experiment indicating when TME was
% absent



dark1 = rectangle('Position',[-3, 0, 3, 60],'Curvature',[0,0],...
          'FaceColor',[1 1 1]* grayLevel,'LineStyle','none');
uistack(dark1, 'bottom'); 

text(4,55,'AIDA','HorizontalAlignment','center','FontSize',18);
text(-2.5,55,'b)','HorizontalAlignment','center','FontSize',18);


set(gca,'Layer','top');  




