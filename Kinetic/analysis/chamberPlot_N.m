function chamberPlot_N( plotName, plotDir, xLabelStr, xlimits, linestyle, touchupCommand )

global results

if nargin < 1, plotName = ''; end;
if nargin < 2, plotDir = 'apin_exp/'; end;
if nargin < 3, xLabelStr = 'nondimensional time (lifetimes)'; end;
if nargin < 4, xlimits = []; end;
if nargin < 5, linestyle = 'g-'; end;
if nargin < 6, touchupCommand = []; end;

%hold off;

set(gcf,'Units','inches');
set(gcf, 'PaperUnits', 'inches');
pos = get(gcf,'Position');
set(gcf,'Position',[pos(1) pos(2) 7 4]);
set(gcf,'PaperPosition',[0 0 7 4]);
set(gcf, 'PaperSize', [7 4]);


% Plot the data array (if it is [nan nan] nothing shows).

pl_2 = plot(results.t_exp, results.COA_exp,'go');
set(pl_2,'MarkerSize',8);
set(pl_2,'Linewidth',1.5);
set(pl_2,'MarkerEdgeColor',[0 .5 0]);

hold on

% Just plot the suspended OM
    
pl_1 = plot(results.tau_Array, results.C_OM_susp_Array_all,linestyle);
%set(pl_1,'Color','g');
set(pl_1,'LineWidth',3);



if xlimits, xlim(xlimits); end;
   
ax = gca;
set(ax,'FontSize',14);
set(get(ax,'Xlabel'),'String',xLabelStr);
set(get(ax,'Xlabel'),'FontSize',18);
set(get(ax,'ylabel'),'String','C ({\mu}g m^{-3})');
set(get(ax,'Ylabel'),'FontSize',18);
set(ax,'LineWidth',1.5);
set(ax,'XMinorTick','on');
set(ax,'TickLength',[.02,0.05]);

if touchupCommand, eval(touchupCommand); end;

if plotName, saveas(gcf,['figs/' plotDir plotName '.pdf'],'pdf'); end;
