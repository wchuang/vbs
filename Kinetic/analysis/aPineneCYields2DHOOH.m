function [CStarBasis, O2CBasis, dHvapBasis, s1D_all] = aPineneCYields2DHOOH

global Cs Param
% Here are the basis parameters for the two dimensions.
% C* range dependent declaration of these variables are within
% CNum2DModified.
%%% CStarBasis = [1e-5 1e-4 1e-3 1e-2 1e-1 1e0 1e1 1e2 1e3 1e4 1e5 1e6 1e7 1e8 1e9];
%%% O2CBasis = [1; .9; .8; .7; .6; .5; .4; .3; .2; .1; 0];
%%% dHvapBasis = [120  114  108  102  96   90  84  78  72  66  60  54  48  42  36 ];

[CStarBasis, O2CBasis, dHvapBasis] = CNum2DModified; % create discrete arrays for VBS axes

oxOH2D_0N; % oxidation matrix for non-nitrates
oxOH2D_1N; % oxidation matrix for organonitrates
splitOH2D_0N; % fragmentation matrix for non-nitrates
splitSplitOH2D_1N; % fragmentation of nitrate group and subsequent smaller fragmentation
splitOxOH2D_1N; % fragmentation of nitrate group and subsequent smaller oxidation

%%

switch Param.firstGen
    case 'v0.1'
        % This is the original carbon stoichiometry matrix spread very wide...
        %  -5     -4     -3     -2    -1     0     1     2     3     4     5     6     7     8     9
         s2D = [ ...
             0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % 1
             0      .001   0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .9
             0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .8
             0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .7
             0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .6
             0      0      0      0     0.003 0.009 0.007 0.003 0     0     0     0     0     0     0 ;  ... % .5
             0      0      0      0     0     0.019 0.027 0.025 0.014 0.010 0     0     0     0     0 ;  ... % .4
             0      0      0      0     0     0.008 0.021 0.046 0.064 0.074 0.050 0     0     0     0 ;  ... % .3  
             0      0      0      0     0     0     0.010 0.020 0.065 0.135 0.170 0.029 0     0     0 ;  ... % .2  
             0      0      0      0     0     0     0     0.001 0.007 0.020 0.030 0.132 0     0     0 ;  ... % .1
             0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % 0
         ];
        %sum 0      .001   0      0     .003  .036  .065  .095  .150  .239  .250  .161
    case 'v0.2'
        % This is the new carbon stoichiometry matrix spread very wide...
        %  -5     -4     -3     -2    -1       0     1     2     3     4     5     6     7     8     9
        s2D = [ ...
            0      0      0      0     0       0     0     0     0     0     0     0     0     0     0 ;  ... % 1
            0      .001   0      0     0       0     0     0     0     0     0     0     0     0     0 ;  ... % .9
            0      0      0      0     0       0     0     0     0     0     0     0     0     0     0 ;  ... % .8
            0      0      0      0     0       0     0     0     0     0     0     0     0     0     0 ;  ... % .7
            0      0      0      0     0       0     0     0     0     0     0     0     0     0     0 ;  ... % .6
            0      0      0      0     0.003   0.006 0.003 0.012 0     0     0     0     0     0     0 ;  ... % .5
            0      0      0      0     0       0.012 0.013 0.100 0.016 0.004 0     0     0     0     0 ;  ... % .4
            0      0      0      0     0       0.005 0.010 0.184 0.073 0.030 0.044 0     0     0     0 ;  ... % .3  
            0      0      0      0     0       0     0.005 0.080 0.075 0.054 0.151 0.023 0     0     0 ;  ... % .2  
            0      0      0      0     0       0     0     0.004 0.008 0.008 0.027 0.106 0     0     0 ;  ... % .1
            0      0      0      0     0       0     0     0     0     0     0     0     0     0     0 ;  ... % 0
        ];
        %}
    case 'v0.3'
        % This is a carbon stoichiometry matrix spread very wide...
    %  -5     -4     -3     -2    -1     0     1     2     3     4     5     6     7     8     9
    s2D = [ ...
        0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % 1
        0      .001   0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .9
        0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .8
        0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .7
        0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .6
        0      0      0      0     0.003 0.009 0.007 0.003 0     0     0     0     0     0     0 ;  ... % .5
        0      0      0      0     0     0.019 0.027 0.025 0.014 0.010 0     0     0     0     0 ;  ... % .4
        0      0      0      0     0     0.008 0.021 0.046 0.064 0.074 0.050 0     0     0     0 ;  ... % .3  
        0      0      0      0     0     0     0.010 0.020 0.065 0.135 0.170 0.029 0     0     0 ;  ... % .2  
        0      0      0      0     0     0     0     0.001 0.007 0.020 0.030 0.132 0     0     0 ;  ... % .1
        0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % 0
    ];
    case 'v1.0'
        % This is built from the original a-pinene mass yields
        %            -2     -1    0     1     2     3     4     5     6
        % alpha =  [0.004 0.000 0.051 0.086 0.120 0.183 0.400 0.350 0.200]; 1.394

        %  -5     -4     -3     -2    -1     0     1     2+.12 3+.05 4:-.1 5-.05 6-.05 7     8     9
        s2D = [ ...
            0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % 1		+1.0  sum
            0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .9	+0.7
            0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .8	+0.4
            0      .001   0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .7	+0.1 .001
            0      0      0      0     0.002 0.010 0.005 0     0     0     0     0     0     0     0 ;  ... % .6	-0.2 .017 
            0      0      0      0     0     0.017 0.015 0.003 0     0     0     0     0     0     0 ;  ... % .5	-0.5 .035
            0      0      0      0     0     0.004 0.045 0.035 0.011 0.013 0.023 0     0     0     0 ;  ... % .4	-0.8 .131
            0      0      0      0     0     0     0.021 0.106 0.110 0.096 0.044 0.041 0     0     0 ;  ... % .3  	-1.1 .418
            0      0      0      0     0     0     0     0.071 0.105 0.084 0.066 0.072 0     0     0 ;  ... % .2  	-1.4 .398
            0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .1	-1.7	
            0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % 0		-2
        ];
        %sum0      .001   0      0     .002  .031  .086  .215  .226  .193  .133  .113
    otherwise
        warning('VBS:parameters','Please enter valid choice for first generation distribution in parameters.m')
end
        
%% Added by WKC 7/2/12 to increase s2D size
if Param.volatilityRange(1) < -5
    s2D = [zeros(size(s2D,1),-5 - Param.volatilityRange(1)),s2D];
end

if Param.volatilityRange(2) > 9 % This is an unlikely case, and is unaccounted for in the fragmentation kernel. Will need additional code to 
    s2D = [s2D, zeros(size(s2D,1), Param.volatilityRange(2) - 9)];
end

if Param.volatilityRange(2) < 9 % Collapse the upper volatilites down to the upper limit set in parameters
    s2D = [s2D(:, 1:Param.volatilityRange(2) - Param.volatilityRange(1) ) sum(s2D(:,Param.volatilityRange(2)-Param.volatilityRange(1)+1:9-Param.volatilityRange(1)+1),2)];
end

if Param.O2CRange(2) > 1
    s2D = [zeros((O2CBasis(1,1)-1)/.1, size(s2D,2));s2D];
end

% Make the s2D matrix into a 1D array.
s2D = reshape(s2D'  , 1, Cs.rows2D * Cs.cols2D);

if Param.layersOfN > 0
    % a-pinene mass yields for high NOx conditions, bins -2 to 3 are from Pathak et al., 2007. 
    %                       -2    -1     0     1     2     3     4     5     6     7     8
    % alpha_high_NOx =    [0.000 0.002 0.003 0.065 0.080 0.250 0.800 0.380 0.220 0.035]; 1.835 From the average addition of ONO2 and 2O's 

    %  -5     -4     -3     -2    -1     0     1     2     3     4     5     6     7     8     9
    s2D_NOx_0N = [ ...
        0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % 1		+1.0
        0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .9	+0.7
        0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .8	+0.4
        0      0      0      .0003 0     0     0     0     0     0     0     0     0     0     0 ;  ... % .7	+0.1 
        0      0      0      0     0     0     .0006 0.003 .0018 0     0     0     0     0     0 ;  ... % .6	-0.2  
        0      0      0      0     0     0     0     .0051 .0048 .0012 0     0     0     0     0 ;  ... % .5	-0.5 
        0      0      0      0     0     0     0     .0012 .0153 .0129 .0077 .0091 .0063 0     0 ;  ... % .4	-0.8 
        0      0      0      0     0     0     0     0     .0114 .0543 0.077 .0525 .0126 0     0 ;  ... % .3  	-1.1 
        0      0      0      0     0     0     0     0     .0063 .0504 .0735 .0427 .0252 0     0 ;  ... % .2  	-1.4 
        0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .1	-1.7	
        0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % 0		-2.0
    ];

    s2D_NOx_1N = [ ...
        0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % 1		+1.0
        0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .9	+0.7
        0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .8	+0.4
        0      0      0      .0007 0     0     0     0     0     0     0     0     0     0     0 ;  ... % .7	+0.1 
        0      0      0      0     0     0     .0014 0.007 .0042 0     0     0     0     0     0 ;  ... % .6	-0.2  
        0      0      0      0     0     0     0     .0119 .0112 .0028 0     0     0     0     0 ;  ... % .5	-0.5 
        0      0      0      0     0     0     0     .0028 .0357 .0301 .0033 .0039 .0027 0     0 ;  ... % .4	-0.8 
        0      0      0      0     0     0     0     0     .0266 .1267 0.033 .0225 .0054 0     0 ;  ... % .3  	-1.1 
        0      0      0      0     0     0     0     0     .0147 .1176 .0315 .0183 .0108 0     0 ;  ... % .2  	-1.4 
        0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .1	-1.7	
        0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % 0		-2.0
    ];

    if Param.volatilityRange(1) < -5
        s2D_NOx_0N = [zeros(size(s2D_NOx_0N,1),-5 - Param.volatilityRange(1)),s2D_NOx_0N];
        s2D_NOx_1N = [zeros(size(s2D_NOx_1N,1),-5 - Param.volatilityRange(1)),s2D_NOx_1N];
    end
    
    if Param.volatilityRange(2) > 9
        s2D_NOx_0N = [s2D_NOx_0N, zeros(size(s2D_NOx_0N,1), Param.volatilityRange(2) - 9)];
        s2D_NOx_1N = [s2D_NOx_1N, zeros(size(s2D_NOx_1N,1), Param.volatilityRange(2) - 9)];
    end
    
    if Param.volatilityRange(2) < 9
        s2D_NOx_0N = [s2D_NOx_0N(:, 1:Param.volatilityRange(2) - Param.volatilityRange(1) ) sum(s2D_NOx_0N(:,Param.volatilityRange(2)-Param.volatilityRange(1)+1:9-Param.volatilityRange(1)+1),2)];
        s2D_NOx_1N = [s2D_NOx_1N(:, 1:Param.volatilityRange(2) - Param.volatilityRange(1) ) sum(s2D_NOx_1N(:,Param.volatilityRange(2)-Param.volatilityRange(1)+1:9-Param.volatilityRange(1)+1),2)];
    end
    
    if Param.O2CRange(2) > 1
        s2D_NOx_0N = [zeros((O2CBasis(1,1)-1)/.1, size(s2D_NOx_0N,2));s2D_NOx_0N];
        s2D_NOx_1N = [zeros((O2CBasis(1,1)-1)/.1, size(s2D_NOx_1N,2));s2D_NOx_1N];
    end
    
    % Make the s2D matrix into an array.
    s2D = reshape(s2D', 1, Cs.rows2D * Cs.cols2D) * Cs.beta + reshape(s2D_NOx_0N', 1, Cs.rows2D * Cs.cols2D) * (1-Cs.beta);
    s2D_1N = reshape(s2D_NOx_1N', 1, Cs.rows2D * Cs.cols2D) * (1-Cs.beta);
    
else 
    
    s2D_1N = [];
end

% New NO2 distribution yields. Accounts for different yields for 1N layer
% and 0N layer, based on Pathak's experiments.
s1D_all = [s2D s2D_1N, zeros(1, Cs.cols2D * Cs.rows2D * (Param.layersOfN - 1))];