function CMUExperiment

global results

commonChamberConstants

% CMU TME Experiment
C_prec = 179; % 32 ppb x 5.59
%C_prec = 240;   % ug/m3  about 1.34 x nominal
C_prec = C_prec * 0.95;

% Physical loss rate constants for CMU
k_wall = 1/(2.45*oneHour);  % particle wall loss rate constant
k_vap = 0;   % irreversible vapor deposition to wall 
k_dil = 0;% dilution relative to k_basis


timeArray = (-2.3:.02:2);
startsArray = timeArray(1:length(timeArray)-1);

injectTME = startsArray >= 0;

C_O3 = 880e-9 * oneAtm;
C_OH = 2e6 * injectTME;
aPin_OH_Yield = 0.8;

aPineneAgingTimeseries(timeArray*oneHour,C_prec,C_O3,C_OH,n_frag,...
    k_prec,k_basis,k_het,k_vap,k_wall,k_dil,aPin_OH_Yield);

results.tau_Array = timeArray;

[results.t_exp results.COA_exp results.nPart_exp] = textread('data/CMU.dat',...
    '%f %f %f','headerlines',1);results.t_OtoC_exp=nan;  results.OtoC_exp=nan;
[results.t_OtoC_exp results.OtoC_exp] = textread('data/CMU_OtoC.dat',...
    '%f %f','headerlines',1);

graphNames
% bar3graphs

figure
exptPlots_N(0,'cmu','MUCHACHAS_Paper/','time from start of OH aging (h)');


figure
chamberPlot_N('CMU_Chamber','MUCHACHAS_Paper/',...
    'time from start of OH aging (h)',[-3,2],'g-');

aPineneAgingTimeseries(timeArray*oneHour,C_prec,C_O3,0,n_frag,...
    k_prec,k_basis,k_het,k_vap,k_wall,k_dil,aPin_OH_Yield);
results.tau_Array = timeArray;


results.t_exp = nan;
results.COA_exp = nan;

chamberPlot_N('CMU_Chamber','MUCHACHAS_Paper/',...
    'time from start of OH aging (h)',[-3,2],'g--','CMUBoxes');