function SAPHIRExperiment(xrange)

global results

if nargin < 1, xrange = [-5,50]; end;

commonChamberConstants


% SAPHIR Experiments

% Physical loss rate consatnts for SAPHIR
k_wall = 1e-5;  % particle wall loss rate constant
k_wall_dark = 7.4e-6;
k_wall_dark = 10e-6;
k_wall_day = 2.5e-5 - k_wall_dark;
k_wall_day = 3e-5 - k_wall_dark;
k_wall_fan = 4.0e-5 - k_wall_dark;
k_vap = 0;      % irreversible vapor deposition to wall
k_dil = 9.35e-6;% dilution relative to k_basis



% The SAPHIR Experiment SA10

C_prec = 218; % 39 ppb
%C_prec = 275;   % ug/m3  1.26 x
%C_prec = C_prec * 0.88;
C_prec = C_prec * 0.92;
%C_prec = C_prec * 0.65;

% Times are adjusted to time from first roof open
% timeArray = (6-11:.02:64-11);
timeArray = (6-11:.02:21-11);
timeArray = (6-11:.02:21-11);
startsArray = timeArray(1:length(timeArray)-1);

% read in the chamber data
[results.t_exp results.apin_exp results.o3_exp results.rh_exp ...
    results.temp_exp results.COA_exp] = textread('data/SOA10v2.dat',...
    '%f %f %f %f %f %f','headerlines',1);

results.temp_exp = results.temp_exp + 273.15; % convert T to K.
results.t_OtoC_exp=nan;  results.OtoC_exp=nan;

Kelvins = interp1(results.t_exp,results.temp_exp,timeArray,'spline');
Kelvins = 300;

% This is the real roof open sequence
roofOpen = (startsArray >= 11-11 & startsArray <= 16-11) | ...
    (startsArray >= 33-11 & startsArray <= 40-11) | ...
    (startsArray >= 55-11 & startsArray <= 64-11);

% This is when turbulence in the chamber appears to be significant
turbulenceHigh = (startsArray >= 12-11 & startsArray <= 16-11) | ...
    (startsArray >= 33-11 & startsArray <= 40-11) | ...
    (startsArray >= 55-11 & startsArray <= 64-11);


% The OH isn't really very high for the whole roof open period...
highOH = (startsArray >= 12-11 & startsArray <= 15-11) | ...
    (startsArray >= 33-11 & startsArray <= 40-11) | ...
    (startsArray >= 55-11 & startsArray <= 64-11);
C_OH = 2.5e6 * highOH;



fanOn = (startsArray >= 9-11 & startsArray <= 12-11);

% wall loss is complex function of conditions
k_wall = k_wall_dark + roofOpen * k_wall_day + fanOn * k_wall_fan;
k_wall = k_wall_dark + turbulenceHigh * k_wall_day + fanOn * k_wall_fan;

C_OH = 1.6e6 * roofOpen;
C_OH = 2.5e6 * highOH;
C_OH = 1.6e6 * 5/3 * highOH;
%C_OH = 1.5e7 * highOH;
C_O3 = 80e-9 * oneAtm;

aPin_OH_Yield = 0.8;

aPineneAgingTimeseries(timeArray*oneHour,C_prec,C_O3,C_OH,n_frag,...
    k_prec,k_basis,k_het,k_vap,k_wall,k_dil,aPin_OH_Yield,Kelvins);

results.tau_Array = timeArray;



% Plot particle number vs time if needed
%figure
%semilogy(results.t_exp,results.nPart_exp,'.');


figure
exptPlots_N(0,'saphir','MUCHACHAS_Paper/','time (h)',xrange);


plotDir = 'MUCHACHAS_Paper/';
plotName = 'SAPHIR_Chamber';

exptPlots_N(0,'saphir',plotDir,'time from start of OH aging (h)',xrange);


figure
chamberPlot_N(plotName,plotDir,'time from start of OH aging (h)',xrange,'g-');



% A slightly doctored roofOpen sequence  to play with OH
roofOpen = (startsArray >= 11-11 & startsArray <= 13-11) | ...
    (startsArray >= 33-11 & startsArray <= 40-11) | ...
    (startsArray >= 55-11 & startsArray <= 64-11);
C_OH = 1.6e6 * roofOpen;
%but really we use
C_OH = 0;


aPineneAgingTimeseries(timeArray*oneHour,C_prec,C_O3,C_OH,n_frag,...
    k_prec,k_basis,k_het,k_vap,k_wall,k_dil,aPin_OH_Yield,Kelvins);

results.tau_Array = timeArray;


results.t_exp = nan;
results.COA_exp = nan;

chamberPlot_N(plotName,plotDir,'time from start of OH aging (h)',...
    xrange,'g--','SAPHIRBoxes');



