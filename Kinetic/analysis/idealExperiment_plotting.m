function idealExperiment_plotting(C_prec, time)

global results

commonChamberConstants; % constants for the chamber

if nargin < 1, C_prec = 200; end;  % ug/m3

% Physical loss rate constants 
k_wall = 0;  % particle wall loss rate constant
k_vap = 0;   % irreversible vapor deposition to wall 
k_dil = 0;  % dilution relative to k_basis

timeArray = (-2:.02:time); % time range of model experiment in hours. negative values is dark aging
startsArray = timeArray(1:length(timeArray)-1); % vector for length of time. timeArray less the end point

injectOH = startsArray >= 0; % OH starts when lights turn on

C_O3 = 300e-9 * oneAtm; % mlc O3/cm3 air
C_OH = 1e7 * injectOH; % mlc OH/cm3 air
aPin_OH_Yield = 0.8;
n_frag=1/4;

aPineneAgingTimeseries(timeArray*oneHour,C_prec,C_O3,C_OH,n_frag,...
    k_prec,k_basis,k_het,k_vap,k_wall,k_dil,aPin_OH_Yield);

results.tau_Array = timeArray;

results.t_exp      = nan;  results.COA_exp  = nan;
results.t_OtoC_exp = nan;  results.OtoC_exp = nan;

graphNames;

end