function aPineneAgingTimeseries(timeArray,C_prec,C_ozone,C_OH,fragExponent,...
    k_prec,k_basis,k_het,k_vap,k_wall,k_dil,OH_yield,T,j_base)


% The structure Cs holds the concentration information about the basis set
% in many different forms (ie, different dimensions, C*, etc, etc).
% The structure Param holds the parameters
% The structure results hold all the result variables
global Cs Param results
parameters; % list of parameters
initCStructure; % initialize variables for tracked carbon mass
initResultStructure; % initialize variables for resulting carbon/organic mass

% Some constants.
oneHour = 3600; % seconds in an hour

% Default values
if nargin < 1, timeArray = (-2:.1:3)*oneHour; end;
if nargin < 2, C_prec = 100; end; % ug/m3
if nargin < 3, C_ozone = 200e-9 * 2.5e19; end;
if nargin < 4, C_OH = 2e6; end;
if nargin < 5, fragExponent = 1/6; end;
if nargin < 6, k_prec = 8e-17; end; % k for precursor reaction
if nargin < 7, k_basis = 3e-11; end; % constant k for reactions of basis-set vapors
if nargin < 8, k_het = 6e-12; end; % k for heterogeneous (as gas phase...)
if nargin < 9, k_vap = 0; end; % k for irreversible vapor loss to the wall
if nargin < 10, k_wall = 1e-5; end; % k for particle wall loss
if nargin < 11, k_dil = 9.35e-6; end; % k for dilution
if nargin < 12, OH_yield = 0; end; % yield for extra terpene oxidation
if nargin < 13, T = 300; end; % temperature
if nargin < 14, j_base = 0; end; % base j value for O:C = 1

% The overall branching that results in an addition of a nitrate group
if Param.layersOfN > 0
    Cs.overallBranch0N = (1 - Param.branchRONO2) * (1-Cs.beta) + Cs.beta; 
    Cs.overallBranch1N = Param.branchRONO2 * (1-Cs.beta);
else
    Cs.overallBranch0N = 1;
    Cs.overallBranch1N = 0;
end

% Call the setup function for basics.  We are using a different subdir.
setupAPinene2D('MUCHACHAS');
agingCellBase(fragExponent);

% The easy way so far is to pass an array of times
dt = diff(timeArray);

% Initialize the precursor
Cs.C_prec = C_prec;
results.tau = timeArray(1);
computeYieldArray;

% All of the terms in the mass balance equations are set up so that
% The key drivers (ks, dt, oxidant levels) can be passed as either
% arrays or a single scalar, in the standard matlab way.  This way, things
% like variable wall loss rates and all sorts of other mean and nasty
% things can be accommodated.

dTau_prec = k_prec .* C_ozone .* dt;    %lifetimes of precursor aging
dTau_het = k_het .* C_OH .* dt;
dTau_wall = k_wall .* dt;               %lifetimes of wall loss
dTau_vap = k_vap .* dt;                 %lifetimes of vapor deposition 
dTau_dil = k_dil .* dt;                 %lifetimes of dilution loss

dTau_basis_new = C_OH .* dt;            % for vaporAging, used to get lifetimes of vapor aging, nonuniform
dTau_basis = k_basis .* C_OH .* dt;     % vaporAging, uniform
dTau_bas_phot = j_base .* dt;           %base photolysis lifetime...

% Take small time steps
for n = (1:length(dt))
    % Put time in tau here
    results.tau = results.tau + dt(n);
    
    % Precursor Loss Section
    precursorAging(dTau_prec(n),1+OH_yield);

    % Basis set
    switch Param.k_OHaging
        case 'uniform'
            vaporAging_old(dTau_basis(n));
        case 'non-uniform'
            vaporAging(dTau_basis_new(n));
        otherwise
            warning('VBS:parameters','Please enter valid choice for OH aging rate constant in parameters.m')
    end
    suspendedAging(dTau_het(n));
  
    % Photolysis
    vaporPhotolysis(dTau_bas_phot(n));
    suspendedPhotolysis(dTau_bas_phot(n));

    % Wall Loss Section
    particleWallLoss(dTau_wall(n));
    vaporWallLoss(dTau_vap(n));

    % Dilution Section
    diluteChamber(dTau_dil(n));
    
    % Partitioning 
    if length(T) > 1, Cs.T = T(n); else Cs.T = T; end;
    computeOrganicEquilibrium;

    % Collect some results for plotting
    computeYieldArray;    

end;

