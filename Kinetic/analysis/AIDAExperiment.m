function AIDAExperiment

global results

commonChamberConstants


% AIDA Experiments

% Physical loss rate constants for AIDA
k_wall = 0;  % particle wall loss rate constant
k_vap = 1e-4;   % irreversible vapor deposition to wall (1 - 7e-4 Saa09)
k_dil = 0;% dilution relative to k_basis


% The AIDA Experiment SOA08-14
C_prec = 184;  % 33 ppb x 5.59
%C_prec = 275;   % 1.49 x ug/m3
%C_prec = 200;  % 
C_prec = C_prec * 1.05;


timeArray = (-2.25:.02:5);
startsArray = timeArray(1:length(timeArray)-1);

injectTME = startsArray >= 0;

C_O3 = 500e-9 * oneAtm;
C_OH = 3.5e6 * injectTME;
aPin_OH_Yield = 0.8;

aPineneAgingTimeseries(timeArray*oneHour,C_prec,C_O3,C_OH,n_frag,...
    k_prec,k_basis,k_het,k_vap,k_wall,k_dil,aPin_OH_Yield);

graphNames;
% bar3graphs

results.tau_Array = timeArray;


[results.t_exp results.COA_exp] = textread('data/SOA08_14.dat',...
    '%f %f','headerlines',1);

[results.t_OtoC_exp results.OtoC_exp] = textread('data/SOA08_14_OtoC.dat',...
    '%f %f','headerlines',1);

figure
exptPlots_N(0,'aida','MUCHACHAS_Paper/',...
    'time from start of OH aging (h)', [-3,5]);

figure

plotDir = 'MUCHACHAS_Paper/';
plotName = 'AIDA_Chamber';


chamberPlot_N(plotName,plotDir,'time from start of OH aging (h)',...
    [-3,5],'g-');


aPineneAgingTimeseries(timeArray*oneHour,C_prec,C_O3,0,n_frag,...
    k_prec,k_basis,k_het,k_vap,k_wall,k_dil,aPin_OH_Yield);

results.tau_Array = timeArray;


results.t_exp = nan;
results.COA_exp = nan;

chamberPlot_N(plotName,plotDir,'time from start of OH aging (h)',...
    [-3,5],'g--','AIDABoxes');