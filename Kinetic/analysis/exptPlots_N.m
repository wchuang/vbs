function exptPlots_N( showCarbon, plotName, plotDir, xLabelStr, xlimits )

global results

if nargin < 1, showCarbon = 0; end;
if nargin < 2, plotName = ''; end;
if nargin < 3, plotDir = 'apin_exp/'; end;
if nargin < 4, xLabelStr = 'nondimensional time (lifetimes)'; end;
if nargin < 5, xlimits = []; end;

hold off;


set(gcf,'Units','inches');
set(gcf, 'PaperUnits', 'inches');
pos = get(gcf,'Position');
set(gcf,'Position',[pos(1) pos(2) 8.5 9]);
set(gcf,'PaperPosition',[0 0 7 10]);
set(gcf, 'PaperSize', [8.5 9]);

subplot('Position',[.1 .55 .85 .25])

% Plot the data array (if it is [nan nan] nothing shows).

pl_0 = plot(results.t_OtoC_exp, results.OtoC_exp,'go');
set(pl_0,'MarkerSize',8);
set(pl_0,'Linewidth',1.5);
set(pl_0,'MarkerEdgeColor',[.5 0 .5]);
hold on

pl = plot(results.tau_Array,results.OtoC_susp_Array_all,'m');
set(pl,'LineWidth',3);
legend(pl,'O:C','Location','NorthWest');

ax = gca;
set(ax,'FontSize',14);
set(get(ax,'ylabel'),'String','O:C');
set(get(ax,'Ylabel'),'FontSize',18);
set(ax,'LineWidth',1.5);


if xlimits, xlim(xlimits); end;


subplot('Position',[.1 .1 .85 .4])


% showCarbon is a boolean to plot OC to check mass conservation
if ~showCarbon 
    
    % Plot the data array (if it is [nan nan] nothing shows).

    pl_0 = plot(results.t_exp, results.COA_exp,'go');
    set(pl_0,'MarkerSize',8);
    set(pl_0,'Linewidth',1.5);
    set(pl_0,'MarkerEdgeColor',[0 .5 0]);
    hold on

    pl_1 = plot(results.tau_Array, results.C_OM_susp_Array_all,'g');
    set(pl_1,'LineWidth',3);

    hold on

    pl_2 = plot(results.tau_Array, results.C_OM_wall_Array_all,'g:');
    set(pl_2,'LineWidth',3);

    pl_3 = plot(results.tau_Array, results.C_OM_susp_Array_all + ...
       results.C_OM_wall_Array_all,'g--');
    set(pl_3,'LineWidth',3);
    
    pl_4 = plot(results.tau_Array, (results.C_OM_vap_Array_all)/10,'b:');
    set(pl_4,'LineWidth',3);
    
    pl_4a = plot(results.tau_Array, (results.C_OM_ads_Array_all)/10,'m:');
    set(pl_4a,'LineWidth',3);
    
    
    pl_5 = plot(results.tau_Array, (results.C_OM_susp_Array_all + ...
        results.C_OM_wall_Array_all + results.C_OM_vap_Array_all + results.C_OM_ads_Array_all)/10,'b--');
    set(pl_5,'LineWidth',3);
    
    legend([pl_1,pl_2,pl_3,pl_4a,pl_4,pl_5],...
        'Suspended OM','Wall OM','Total OM','Adsorb Orgs /10','Vapor Orgs /10','Total Orgs /10',...
        'Location','SouthEast');

else

    pl_1 = plot(results.tau_Array, results.C_OC_susp_Array_all,'g');
    set(pl_1,'LineWidth',3);
    
    hold on
    
    pl_2 = plot(results.tau_Array, results.C_OC_wall_Array_all,'g:');
    set(pl_2,'LineWidth',3);
    
    pl_3 = plot(results.tau_Array, results.C_OC_susp_Array_all + ...
        results.C_OC_wall_Array_all,'g--');
    set(pl_3,'LineWidth',3);
    

    pl_4 = plot(results.tau_Array, (results.C_OC_vap_Array_all)/10,'b:');
    set(pl_4,'LineWidth',4);
    
    pl_5 = plot(results.tau_Array, (results.C_OC_susp_Array_all + ...
       results.C_OC_wall_Array_all + results.C_OC_vap_Array_all)/10,'b--');
    set(pl_5,'LineWidth',2);
     
%    pl_6 = plot(results.tau_Array, results.C_prod_Array/10,'c');
%    set(pl_6,'LineWidth',2);
 
    legend([pl_1,pl_2,pl_3,pl_4,pl_5],...
        'Suspended OC','Wall OC','Total OC','Vapor Orgs /10',...
        'Total Orgs /10','Location','SouthEast');
end

if xlimits, xlim(xlimits); end;
  
ax = gca;
set(ax,'FontSize',14);
set(get(ax,'Xlabel'),'String',xLabelStr);
set(get(ax,'Xlabel'),'FontSize',18);
set(get(ax,'ylabel'),'String','C ({\mu}g m^{-3})');
set(get(ax,'Ylabel'),'FontSize',18);
set(ax,'LineWidth',1.5);

if plotName, saveas(gcf,['figs/' plotDir plotName '.pdf'],'pdf'); end;
