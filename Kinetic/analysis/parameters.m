function parameters

global Param

% The maximum number of groups of nitrates
Param.layersOfN = 1;

% branchRONO2 represents the fraction of reactions with NO that result in RONO2
Param.branchRONO2 = 0.3;

% The increase in volatility with an addition of a nitrate group
Param.NO2shift = -2.5;

% We are going to model photolysis as a fragmentation process
Param.photFractionFrag = 0;

% Set the bins
% volatilityRange = [-5, 9] -> CStarBasis = [1e-5 1e-4 1e-3 1e-2 1e-1 1e0 1e1 1e2 1e3 1e4 1e5 1e6 1e7 1e8 1e9]
Param.volatilityRange = [-5, 9];

% Set the O:C range
% OtoCRange = [0,1] -> O2CBasis = [1; .9; .8; .7; .6; .5; .4; .3; .2; .1; 0]
Param.O2CRange = [0,1];

% Gas phase kinetic parameters

% gas-phase kinetics
% func is k = kPerC .* (cNum + fO * oNum + fH * hNum)
% calculation is done in CNum2DModified file
Param.kPerC = 1.2e-12;	%kPerCarbon
Param.kFacPerO = 9;
Param.kFacPerH = -10;

% Change between different models for heat of vaporization
% original model is dH = -6*Cstar + 90. (kJ/mol)
% Epstein 2010 model is dH = -11*Cstar + 131. (kJ/mol) Full solution, eq.12
% 'orig', 'Epstein' 
Param.heatVap = 'orig';

% Change between versions of distributions of first generation products of
% alpha pinene. 
% MUCHACHAS graphs are v1.0
% 'v0.1', 'v0.2', ,'v0.3', 'v1.0'
Param.firstGen = 'v1.0';

% Change between uniform rate constant for OH aging or volatility/O:C-dependent
% rate constant from Donahue et al., 2013, Environ. Chem.
% MUCHACHAS uses 'uniform'
% 'uniform', 'non-uniform'
Param.k_OHaging = 'non-uniform';

