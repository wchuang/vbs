% PSI Boxes

grayLevel = 0.8;


% Draw boxses on the plot for the ideal experiment indicating no OH 


dark1 = rectangle('Position',[-5, 0, 5, 60],'Curvature',[0,0],...
          'FaceColor',[1 1 1]* grayLevel,'LineStyle','none');
uistack(dark1, 'bottom'); 


text(4,55,'PSI','HorizontalAlignment','center','FontSize',18);
text(-4.5,55,'c)','HorizontalAlignment','center','FontSize',18);


set(gca,'Layer','top');  




