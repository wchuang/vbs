function setupAPinene2D(figSubDirName)

if nargin < 1, figSubDirName = 'apin'; end;

global Cs

% Here are specific molecules we want to track in the plots
Cs.compoundList = {'aPinene'};

% Make sure there is a place for us, somewhere a place for us...
if ~exist('./figs','dir'); mkdir('./figs'); end;
if ~exist(['./figs/' figSubDirName],'dir'); mkdir(['./figs/' figSubDirName]); end;

% Initialize with a-pinene + ozone!
[Cs.C_Star300, Cs.O2C, Cs.dHv, Cs.CYield_2D_all] = aPineneCYields2DHOOH;

% At present we will have 3 pools to hold material: vapor, suspended, wall
% We are going to track the carbon mass concentration in those pools.
% We also have an "adsorbed" pool for (currently) irreversible wall loss
% just to be sure we have a good mass balance.

Cs.C_OC_tot_2D_all      = 0 * Cs.CYield_2D_all;  % Total organics in 2D (both phases)
Cs.C_OC_vap_2D_all      = Cs.C_OC_tot_2D_all;    % Org vapors in 2D
Cs.C_OC_susp_2D_all     = Cs.C_OC_tot_2D_all;    % Suspended particle OC in 2D 
Cs.C_OC_wall_2D_all     = Cs.C_OC_tot_2D_all;    % Wall deposited particle OC in 2D
Cs.C_OC_ads_2D_all      = Cs.C_OC_tot_2D_all;    % Wall removed organics (any phase) 2D
Cs.C_OC_vapDil_2D_all   = Cs.C_OC_tot_2D_all;    % Dilution removed orgs (vapor) 2D
Cs.C_OC_suspDil_2D_all  = Cs.C_OC_tot_2D_all;    % Dilution removed orgs (particle) 2D

% We are going to carry a C_Star as C_Star300 just for 
% backwards compatibility
Cs.T = 300;
Cs.C_Star = Cs.C_Star300;
