function [OC2D_all] = OM2OC(C_OM_2D_all)

global Cs Param

% We can generate a total mass yield matrix by making use of the OC matrix
OMOCmat    = ((16/12*Cs.O2CExtended)+(1/12*(2-Cs.O2CExtended))+ 1);

%% Create total mass yield matrix using the OC matrix

layerArray = 0:Param.layersOfN;

% Number of N's in each cell
layerArrayFull = layerArray(1, repmat(1:end, [Cs.rows2D * Cs.cols2D, 1]));

% NO2 mass per carbon mass in each cell
massEachNitrate = repmat(46 ./ (12*Cs.cNumArray1N), 1, Param.layersOfN + 1);

% OM (excluding NO2 mass) per OC
OMOCmat_base = repmat(OMOCmat, 1, Param.layersOfN + 1);

% OM (including NO2 mass) per OC
OMOCmat_all = OMOCmat_base + massEachNitrate .* layerArrayFull;

% OC in each cell
OC2D_all = C_OM_2D_all ./ OMOCmat_all;