function particleWallLoss(dtau)

% Transfers mass from suspended particle array to wall
% 
% dtau is the pseudo first-order timestep for the transfer

global Cs

dC_part_all = Cs.C_OC_susp_2D_all * (1-exp(-dtau));

% Take mass away from suspended array
Cs.C_OC_susp_2D_all = Cs.C_OC_susp_2D_all - dC_part_all;

% And give it to the wall array
Cs.C_OC_wall_2D_all = Cs.C_OC_wall_2D_all + dC_part_all;