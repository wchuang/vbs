function computeYieldArray

global Cs results

% Collect some values for a plot
% This function is called periodically, and when it is, values from the Cs
% structure are saved to a growing array for later plotting.

%results.tau_Array = [results.tau_Array results.tau];

Cs.C_OC_tot = Cs.C_OC_vap + Cs.C_OC_susp + Cs.C_OC_wall + Cs.C_OC_ads;
Cs.C_OM_tot = Cs.C_OM_vap + Cs.C_OM_susp + Cs.C_OM_wall + Cs.C_OM_ads;
Cs.C_OC_tot_N = Cs.C_OC_vap_N + Cs.C_OC_susp_N + Cs.C_OC_wall_N + Cs.C_OC_ads_N;
Cs.C_OM_tot_N = Cs.C_OM_vap_N + Cs.C_OM_susp_N + Cs.C_OM_wall_N + Cs.C_OM_ads_N;

results.C_OC_vap_Array_all  = [results.C_OC_vap_Array_all  Cs.C_OC_vap];
results.C_OC_susp_Array_all = [results.C_OC_susp_Array_all Cs.C_OC_susp];
results.C_OC_wall_Array_all = [results.C_OC_wall_Array_all Cs.C_OC_wall];
results.C_OC_ads_Array_all  = [results.C_OC_ads_Array_all  Cs.C_OC_ads];
results.C_OC_tot_Array_all  = [results.C_OC_tot_Array_all  Cs.C_OC_tot];

results.C_OC_vap_Array_N  = [results.C_OC_vap_Array_N  Cs.C_OC_vap_N'];
results.C_OC_susp_Array_N = [results.C_OC_susp_Array_N Cs.C_OC_susp_N'];
results.C_OC_wall_Array_N = [results.C_OC_wall_Array_N Cs.C_OC_wall_N'];
results.C_OC_ads_Array_N  = [results.C_OC_ads_Array_N  Cs.C_OC_ads_N'];
results.C_OC_tot_Array_N  = [results.C_OC_tot_Array_N  Cs.C_OC_tot_N'];

results.C_OM_vap_Array_all  = [results.C_OM_vap_Array_all  Cs.C_OM_vap];
results.C_OM_susp_Array_all = [results.C_OM_susp_Array_all Cs.C_OM_susp];
results.C_OM_wall_Array_all = [results.C_OM_wall_Array_all Cs.C_OM_wall];
results.C_OM_ads_Array_all  = [results.C_OM_ads_Array_all  Cs.C_OM_ads];
results.C_OM_tot_Array_all  = [results.C_OM_tot_Array_all  Cs.C_OM_tot];

results.C_OM_vap_Array_N  = [results.C_OM_vap_Array_N  Cs.C_OM_vap_N'];
results.C_OM_susp_Array_N = [results.C_OM_susp_Array_N Cs.C_OM_susp_N'];
results.C_OM_wall_Array_N = [results.C_OM_wall_Array_N Cs.C_OM_wall_N'];
results.C_OM_ads_Array_N  = [results.C_OM_ads_Array_N  Cs.C_OM_ads_N'];
results.C_OM_tot_Array_N  = [results.C_OM_tot_Array_N  Cs.C_OM_tot_N'];

results.OM_OC_susp_Array_N   = [results.OM_OC_susp_Array_N   Cs.OM_OC_susp_N];
results.OM_OC_susp_Array_all = [results.OM_OC_susp_Array_all Cs.OM_OC_susp_overall];

results.OtoC_susp_Array_N    = [results.OtoC_susp_Array_N Cs.OtoC_susp_N];
results.OtoC_susp_Array_all  = [results.OtoC_susp_Array_all Cs.OtoC_susp_all];

results.C_prec_Array = [results.C_prec_Array Cs.C_prec];
results.C_prod_Array = [results.C_prod_Array Cs.C_prod];

results.massOfNO2 = [results.massOfNO2 Cs.massOfNO2];
% results.OtoC_susp_Array_0N = [results.OtoC_susp_Array_0N Cs.OtoC_susp_0N];
% results.OtoC_susp_Array_1N = [results.OtoC_susp_Array_1N Cs.OtoC_susp_1N];
%results.AMF_Array = [results.AMF_Array Cs.AMF];


