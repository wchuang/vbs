function vaporAging_old(tau)

global Cs

% Assuming pseudo first-order kinetics,
% here are factors for what is lost and also what is left
firstOrderLeftGas = exp(-tau);
firstOrderLossGas = 1 - firstOrderLeftGas;

C_OC_new = firstOrderLossGas .* Cs.C_OC_vap_2D_all * cell2mat(Cs.agingCell) ...
            + firstOrderLeftGas .* Cs.C_OC_vap_2D_all;

Cs.C_OC_vap_2D_all = C_OC_new;
