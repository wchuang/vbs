function initResultStructure(zapFigs)

if nargin < 1, zapFigs = 1; end;

% initCStructure
%   Declare the basis substructure things and null them out

global results

% Arrays All N Included

% Arrays of carbon mass
results.C_OC_tot_Array     = [];   % total carbon mass

results.C_OC_vap_Array_all  = [];   % vapor carbon mass
results.C_OC_wall_Array_all = [];   % wall carbon mass
results.C_OC_susp_Array_all = [];   % suspended carbon mass
results.C_OC_ads_Array_all  = [];   % adsorbed carbon mass
results.C_OC_tot_Array_all  = [];   % total carbon mass

results.C_OC_vap_Array_N  = [];      % vapor carbon mass by N
results.C_OC_wall_Array_N = [];     % wall carbon mass by N
results.C_OC_susp_Array_N = [];     % suspended carbon mass by N
results.C_OC_ads_Array_N  = [];      % adsorbed carbon mass by N
results.C_OC_tot_Array_N  = [];      % total carbon mass by N

results.C_OM_vap_Array_all  = [];   % vapor organic mass
results.C_OM_wall_Array_all = [];   % wall organic mass
results.C_OM_susp_Array_all = [];   % suspended organic mass
results.C_OM_ads_Array_all  = [];   % adsorbed organic mass
results.C_OM_tot_Array_all  = [];   % total organic mass

results.C_OM_vap_Array_N  = [];      % vapor organic mass by N
results.C_OM_wall_Array_N = [];     % wall organic mass by N
results.C_OM_susp_Array_N = [];     % suspended organic mass by N
results.C_OM_ads_Array_N  = [];      % adsorbed organic mass by N
results.C_OM_tot_Array_N  = [];      % total organic mass by N

results.OM_OC_susp_Array_N   = [];  % OM to OC of suspended particles
results.OM_OC_susp_Array_all = [];
results.OtoC_susp_Array_N    = [];
results.OtoC_susp_Array_all  = [];

results.massOfNO2 = []; % to get total mass of NO2

results.tau_Array    = [];
results.C_prec_Array = [];
results.C_prod_Array = [];
results.AMF_Array    = [];

results.fig2D.arrow = 0;
results.fig2D.text = 0;
results.fig2D.title = 0;

if zapFigs
    results.fig2D.fig = 0;
    results.figContour = 0; 
end;

