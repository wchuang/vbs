function initCStructure


% initCStructure
%   Declare the basis substructure things and null them out

global Cs Param

% concentration of the precursor, if there is one, for SOA
Cs.C_prec = 0;

% concentration of the product
Cs.C_prod = 0;

% Start with zeros for all of the basis-set scalars

Cs.C_OC_vap  = 0;
Cs.C_OC_susp = 0;
Cs.C_OC_wall = 0;
Cs.C_OC_ads  = 0;
Cs.C_OC_tot  = 0;

Cs.C_OM_vap  = 0;
Cs.C_OM_susp = 0;
Cs.C_OM_wall = 0;
Cs.C_OM_ads  = 0;
Cs.C_OM_tot  = 0;

Cs.C_OC_susp_N = zeros(1, Param.layersOfN + 1);
Cs.C_OC_wall_N = zeros(1, Param.layersOfN + 1);
Cs.C_OC_vap_N  = zeros(1, Param.layersOfN + 1);
Cs.C_OC_ads_N  = zeros(1, Param.layersOfN + 1);
Cs.C_OC_tot_N  = zeros(1, Param.layersOfN + 1);

Cs.C_OM_susp_N = zeros(1, Param.layersOfN + 1);
Cs.C_OM_wall_N = zeros(1, Param.layersOfN + 1);
Cs.C_OM_vap_N  = zeros(1, Param.layersOfN + 1);
Cs.C_OM_ads_N  = zeros(1, Param.layersOfN + 1);
Cs.C_OM_tot_N  = zeros(1, Param.layersOfN + 1);

% undefined O:C for all N Included
Cs.OM_OC_susp_N = nan(Param.layersOfN + 1, 1);
Cs.OM_OC_susp_overall = nan; 

Cs.OtoC_susp = nan;
Cs.OtoC_susp_N = nan(Param.layersOfN + 1, 1);
Cs.OtoC_susp_all = nan;

% variable to get total mass of NO2
Cs.massOfNO2 = 0;