function suspendedPhotolysis(dTauBase,photExponent)

global Cs Param

if dTauBase == 0, return; end;
if nargin < 2, photExponent = 2; end;

% Assuming pseudo first-order kinetics,
% here are factors for what is lost and also what is left

% We need to know the branching ratio between 
% functionalization and fragmentation.
% As a first guess, we are going to make this br(O:C)
    
% The working assumption so far is that photolysis goes way up 
% with increasing O:C as more conjugation makes more fun
jRowScale = Cs.O2CExtended.^(photExponent);
dTau = dTauBase * jRowScale;
   
firstOrderLeftPart = exp(-dTau);
firstOrderLossPart = 1 - firstOrderLeftGas;

% We are going to model photolysis as a fragmentation process
% fractionFragmenting = 1;

C_OC_new_all = (firstOrderLossPart .* Cs.C_OC_susp_2D_all * Param.photFractionFrag) * cell2mat(Cs.photolysisCell) ...
            + firstOrderLeftPart .* Cs.C_OC_susp_2D_all;
        
Cs.C_OC_vap_all = C_OC_new_all;
