function agingCellBase(fragExponent)
% This creates the base cell array that contains all the aging kernals.
% This can be expanded to run inputs and create different matrices for 0N
% to 1N vs 1N to 2N.

global Cs Param

%% OH Aging
fracFrag = repmat(Cs.O2CExtended.^(fragExponent), Cs.cols2D * Cs.rows2D, 1)'; 
fracFunc = 1 - fracFrag; 

% Initialize cell for vapor, susp aging
Cs.agingCell = cell(Param.layersOfN + 1, Param.layersOfN + 1);
Cs.agingCell(1:end, 1:end) = {zeros(Cs.cols2D * Cs.rows2D)};

% Create aging for 0N to 0N
% oxOH2D_0N, splitOH2D_0N
Cs.agingCell{1,1} = Cs.oxProdArrayCombined_0t0N .* fracFunc * Cs.overallBranch0N + Cs.splitProdArrayCombined_0t0N .* fracFrag;

%% Photolysis

% Model photolysis as a fragmentation process, called "fractionFragmenting" in vaporPhotolysisv2_N and suspendedPhotolysisv2_N.
fracPhotFrag = 1;

%{
% Potential for future adjustments to photolysisCell
jRowScale = (repmat(Cs.O2CExtended.^(fragExponent), Cs.cols2D * Cs.rows2D, 1)')^(photExponent);
dTau = dTauBase * jRowScale; 
firstOrderLeftGasPhotolysis = exp(-dTau); 
firstOrderLossGasPhotolysis = 1 - firstOrderLeftGasPhotolysis;
%}

% Initialize cell for vapor, susp photolysis
Cs.photolysisCell = cell(Param.layersOfN + 1, Param.layersOfN + 1);
Cs.photolysisCell(1:end, 1:end) = {zeros(Cs.cols2D * Cs.rows2D)};

% Create photolysis for 0N to 0N
Cs.photolysisCell{1,1} = Cs.splitProdArrayCombined_0t0N * fracPhotFrag;

index = 1;

while index - 1 < Param.layersOfN
    %% Aging
    % Aging for 1N to 0N: fragment then oxidation, and fragment then fragment
    Cs.agingCell{index+1,index}   = Cs.oxProdArrayCombined_1t0N .* fracFrag .* fracFunc + Cs.splitProdArrayCombined_1t0N .* fracFrag .* fracFrag;
    % Aging for 0N to 1N: oxidation
    Cs.agingCell{index,index+1}   = Cs.oxProdArrayCombined_0t1N .* fracFunc * Cs.overallBranch1N;
    % Aging for 1N to 1N: oxidation
    Cs.agingCell{index+1,index+1} = Cs.oxProdArrayCombined_1t1N .* fracFunc * Cs.overallBranch0N;
    
    %% Photolysis
    % Photolysis for 1N to 0N: fragmentation from photolysis then oxidation and fragmentation
    Cs.photolysisCell{index+1,index} = Cs.oxProdArrayCombined_1t0N .* fracPhotFrag .* fracFunc + Cs.splitProdArrayCombined_1t0N .* fracPhotFrag .* fracFrag;
    
    % Each subsequent iteration increases the aging N by 1, so "1N to 0N" becomes "2N to 1N"
    index = index + 1;
    
end
% Carbon mass closure
Cs.agingCell{index,index} = Cs.agingCell{index, index} + Cs.oxProdArrayCombined_1t1N .* fracFunc * Cs.overallBranch1N;

Cs.agingCellMatrix = cell2mat(Cs.agingCell);
Cs.photolysisCellMatrix = cell2mat(Cs.photolysisCell);
