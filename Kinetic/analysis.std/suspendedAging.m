function suspendedAging(dTau)

global Cs

% Assuming pseudo first-order kinetics,
% here are factors for what is lost and also what is left
firstOrderLeftPart = exp(-dTau);
firstOrderLossPart = 1 - firstOrderLeftPart;

% Age the particles:
% The aging matrix conserves carbon mass, so just multiply by
% the amount of carbon in this bucket and spray away...
% and lastly put in the stuff that did not react this step

C_OC_new = firstOrderLossPart .* Cs.C_OC_susp_2D_all * cell2mat(Cs.agingCell) ...
            + firstOrderLeftPart .* Cs.C_OC_susp_2D_all;
        
Cs.C_OC_susp_2D_all = C_OC_new;
