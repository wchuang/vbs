function diluteChamber(dTau)

global Cs

% Chamber dilution affects suspended particles and vapors
% It is quasi kinetic -- they just go poof
firstOrderLeftDil = exp(-dTau);

dC_vap_all = Cs.C_OC_vap_2D_all * (1 - firstOrderLeftDil);
dC_susp_all = Cs.C_OC_susp_2D_all * (1 - firstOrderLeftDil);

% Dilution calcs are simple because we can just apply the exponential loss
% to the matrix.
Cs.C_OC_vap_2D_all  = Cs.C_OC_vap_2D_all  - dC_vap_all;
Cs.C_OC_susp_2D_all = Cs.C_OC_susp_2D_all - dC_susp_all;

% Give it to the dilution arrays
Cs.C_OC_vapDil_2D_all  = Cs.C_OC_vapDil_2D_all  + dC_vap_all;
Cs.C_OC_suspDil_2D_all = Cs.C_OC_suspDil_2D_all + dC_susp_all;