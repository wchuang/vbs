function oxOH2D_1N
% This file introduces RONO2 compounds via the RO2+NO pathway.
setUpOxOxidationKernel;
processAgingTensor;
end

function setUpOxOxidationKernel

global Cs

% Each row up is an added oxygen.
% Each column is a DROPPED decade in C*
% The perl 2D is [ rowNum * numCols + colNum ]

% This kernel is built around the idea that oxidation will add 1, 2, or 3 Os
% And the drop in logC* per O ranges from -1 to -2.25, with an average of -1.7

Cs.colsAging = 8;
Cs.rowsAging = 4;

Cs.oxOxidationKernel = zeros(1,Cs.rowsAging * Cs.colsAging);

Cs.oxOxidationKernel(1 * Cs.colsAging + 1 +1) = 0.09;
Cs.oxOxidationKernel(1 * Cs.colsAging + 2 +1) = 0.15;
Cs.oxOxidationKernel(1 * Cs.colsAging + 3 +1) = 0.06;
Cs.oxOxidationKernel(2 * Cs.colsAging + 2 +1) = 0.10;
Cs.oxOxidationKernel(2 * Cs.colsAging + 3 +1) = 0.20;
Cs.oxOxidationKernel(2 * Cs.colsAging + 4 +1) = 0.15;
Cs.oxOxidationKernel(2 * Cs.colsAging + 5 +1) = 0.05;
Cs.oxOxidationKernel(3 * Cs.colsAging + 3 +1) = 0.02;
Cs.oxOxidationKernel(3 * Cs.colsAging + 4 +1) = 0.04;
Cs.oxOxidationKernel(3 * Cs.colsAging + 5 +1) = 0.08;
Cs.oxOxidationKernel(3 * Cs.colsAging + 6 +1) = 0.04;
Cs.oxOxidationKernel(3 * Cs.colsAging + 7 +1) = 0.02;

end

function agePrecursor2D(precRow, precCol)

global Cs Temp % Param % Param to be used only if dinitrates are considered

cNum1N = Cs.cNumArray1N(precRow * Cs.cols2D + precCol +1);
unitOtoCRows1N = 1/cNum1N;
% Initialize the aging matrix to zeros
Temp.oxProdArray = zeros(1,length(Cs.cNumArray1N));

% Reactions resulting in 0N
for oxyAdd = 0:1:Cs.rowsAging-1
    avgRowUp = oxyAdd * unitOtoCRows1N * 10; % say 1.2
    rowUpLower = floor(avgRowUp);          % say 1
    rowSplitUpper = avgRowUp - rowUpLower; % say .2
    rowSplitLower = 1 - rowSplitUpper;     % say .8
    
    for cStarDown = 0:1:Cs.colsAging-1
        thisStoich = Cs.oxOxidationKernel(oxyAdd * Cs.colsAging + cStarDown +1);
        
        if thisStoich ~= 0
            % Figure out column and don't go out of bounds
            prodCol = precCol - cStarDown;
            if prodCol < 0
                prodCol = 0;
            end
            
            % The products will always be divided between an upper and lower row
            % This is confused because the VISUAL row number advances downward
            % (ie, the printed matrix[0,0] element is in the upper left
            % and to preserve visual consistency we are dropping row numbers with oxy.

            % Do the lower row
            % Don't go out of bounds
            prodRow = precRow - rowUpLower;
            if prodRow < 0
                prodRow = 0;
            end
            prodStoich = thisStoich * rowSplitLower;
            Temp.oxProdArray(prodRow * Cs.cols2D + prodCol + 1) = Temp.oxProdArray(prodRow * Cs.cols2D + prodCol + 1) + prodStoich;
            
            % Do the upper row
            % Don't go out of bounds
            prodRow = precRow - rowUpLower - 1;
            if prodRow < 0
                prodRow = 0;
            end
            prodStoich = thisStoich * rowSplitUpper;
            Temp.oxProdArray(prodRow * Cs.cols2D + prodCol + 1) = Temp.oxProdArray(prodRow * Cs.cols2D + prodCol + 1) + prodStoich;
        end
    end
end

end

function processAgingTensor

global Cs
global Temp

Cs.oxProdArrayCombined_1t1N = [];

for precRow = 0:1:Cs.rows2D-1
    for precCol = 0:1:Cs.cols2D-1
        agePrecursor2D(precRow, precCol);
        %To implement the combining of all ox aging matrices into a single
        %matrix (0t0N = 0N to 0N, 0t1N = 0N to 1N)
        Cs.oxProdArrayCombined_1t1N = [Cs.oxProdArrayCombined_1t1N; Temp.oxProdArray];
    end
end

end

