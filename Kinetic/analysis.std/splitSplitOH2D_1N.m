% Created by WKC 7/11/12
% RONO2 compounds tend to split NO2 off. 
% This file produces matrices for subsequent fragmentation.

function splitSplitOH2D_1N
setUpSplitOxidationKernel_1N;
processAgingTensor;
end

function setUpSplitOxidationKernel_1N

global Cs

% Each row up is an added oxygen.
% Each column is a DROPPED decade in C*
% The perl 2D is ( rowNum * numCols + colNum )

% This kernel is built around the idea that oxidation will add 1, 2, or 3 Os
% And the drop in logC* per O ranges from -1 to -2.25, with an average of -1.7

Cs.colsAging = 8;
Cs.rowsAging = 4;

Cs.splitOxidationKernel_1N = zeros(1,Cs.rowsAging * Cs.colsAging);

Cs.splitOxidationKernel_1N(0 * Cs.colsAging + 0 +1) = 0.50;
Cs.splitOxidationKernel_1N(1 * Cs.colsAging + 0 +1) = 0.09+.03/2;
Cs.splitOxidationKernel_1N(1 * Cs.colsAging + 1 +1) = 0.15+.05/2;
Cs.splitOxidationKernel_1N(1 * Cs.colsAging + 2 +1) = 0.06+.02/2;
Cs.splitOxidationKernel_1N(2 * Cs.colsAging + 1 +1) = 0.02;
Cs.splitOxidationKernel_1N(2 * Cs.colsAging + 2 +1) = 0.04;
Cs.splitOxidationKernel_1N(2 * Cs.colsAging + 3 +1) = 0.03;
Cs.splitOxidationKernel_1N(2 * Cs.colsAging + 4 +1) = 0.01;
Cs.splitOxidationKernel_1N(3 * Cs.colsAging + 2 +1) = 0.01/2;
Cs.splitOxidationKernel_1N(3 * Cs.colsAging + 3 +1) = 0.02/2;
Cs.splitOxidationKernel_1N(3 * Cs.colsAging + 4 +1) = 0.04/2;
Cs.splitOxidationKernel_1N(3 * Cs.colsAging + 5 +1) = 0.02/2;
Cs.splitOxidationKernel_1N(3 * Cs.colsAging + 6 +1) = 0.01/2;
% Add 1 because Matlab indices start at 1
end

function disperseFragment2D(precRow, precCol, crackStoich)

global Cs Param Temp

cNum = Cs.cNumArray9(precRow * Cs.cols2D9 + precCol +1);
unitOtoCRows = 1/cNum;

for oxyAdd = 0:1:Cs.rowsAging-1
    avgRowUp = oxyAdd * unitOtoCRows * 10 - 1; % say 1.2 - Subtract 1 because of removal of ONO2.
    rowUpLower = floor(avgRowUp);          % say 1
    rowSplitUpper = avgRowUp - rowUpLower; % say .2
    rowSplitLower = 1 - rowSplitUpper;     % say .8

   for cStarDown = 0:1:Cs.colsAging-1

        thisStoich = Cs.splitOxidationKernel_1N(oxyAdd * Cs.colsAging + cStarDown +1);
        if thisStoich ~= 0
            
            thisStoich = thisStoich * crackStoich;
            
            % Figure out column and don't go out of bounds. Products are
            % divided between two columns due to the removal of the -NO2
            % group.
            
            colLeftLower = floor(Param.NO2shift);
            colSplitUpper = Param.NO2shift - colLeftLower;
            colSplitLower = 1 - colSplitUpper;

            prodUpperCol = precCol - cStarDown - floor(Param.NO2shift);
            if prodUpperCol > Cs.cols2D9 - 1, prodUpperCol = Cs.cols2D9 - 1; end
            if prodUpperCol < 0,              prodUpperCol = 0;              end
            
            prodLowerCol = precCol - cStarDown - ceil(Param.NO2shift);
            if prodLowerCol > Cs.cols2D9 - 1, prodLowerCol = Cs.cols2D9 - 1; end
            if prodLowerCol < 0,              prodLowerCol = 0;              end
            
            % The products will always be divided between an upper and lower row
            % This is confusing because the VISUAL row number advances downward
            % (ie, the printed matrix[0,0] element is in the upper left
            % and to preserve visual consistency we are dropping row numbers with oxy.

            % Do the lower row
            % Don't go out of bounds
            prodLowerRow = precRow - rowUpLower;
            if prodLowerRow < 0,             prodLowerRow = 0;             end
            if prodLowerRow > Cs.rows2D - 1, prodLowerRow = Cs.rows2D - 1; end
            
            prodStoich = thisStoich * rowSplitLower * colSplitUpper;
            Temp.splitProdArray_1t0N(prodLowerRow * Cs.cols2D9 + prodUpperCol +1) = Temp.splitProdArray_1t0N(prodLowerRow * Cs.cols2D9 + prodUpperCol +1) + prodStoich;
            prodStoich = thisStoich * rowSplitLower * colSplitLower;
            Temp.splitProdArray_1t0N(prodLowerRow * Cs.cols2D9 + prodLowerCol +1) = Temp.splitProdArray_1t0N(prodLowerRow * Cs.cols2D9 + prodLowerCol +1) + prodStoich;
            % Do the upper row
            % Don't go out of bounds
            prodUpperRow = precRow - rowUpLower - 1;
            if prodUpperRow < 0,             prodUpperRow = 0;             end
            if prodUpperRow > Cs.rows2D - 1, prodUpperRow = Cs.rows2D - 1; end
            
            prodStoich = thisStoich * rowSplitUpper * colSplitUpper;
            Temp.splitProdArray_1t0N(prodUpperRow * Cs.cols2D9 + prodUpperCol +1) = Temp.splitProdArray_1t0N(prodUpperRow * Cs.cols2D9 + prodUpperCol +1) + prodStoich;
            prodStoich = thisStoich * rowSplitUpper * colSplitLower;
            Temp.splitProdArray_1t0N(prodUpperRow * Cs.cols2D9 + prodLowerCol +1) = Temp.splitProdArray_1t0N(prodUpperRow * Cs.cols2D9 + prodLowerCol +1) + prodStoich;
        
        end
    end
end

end

function crackPrecursor2D(preRow, preCol, colsToEnd, cBinEquiv)

global Cs
global Temp

% Initialize the aging matrix to zeros
Temp.splitProdArray_1t0N = zeros(1,length(Cs.cNumArray9));

if (colsToEnd == 0)
   Temp.splitProdArray_1t0N(0 * Cs.cols2D9 + Cs.cols2D9) = 1; 
end

% The cracking will turn toward the upper right at the 1/2 way point
halfWay = colsToEnd - floor(colsToEnd/2);
slope = (preRow+1)/halfWay;
    
% We are cracking, so we go from the precursor column forward
for crackCol = preCol+1:1:Cs.cols2D9-1
    % This is the carbon weighted stoichiometry
    crackStoich = colsToEnd / cBinEquiv;
    
    if colsToEnd > halfWay
       % until 1/2 way we just move to the right
       
       disperseFragment2D(preRow, crackCol, crackStoich);
       
    else
      % Beyond 1/2 way we slope up to the upper right
      % Frags will split between 2 rows.
      % This is due to averaging but it is desired anyway.
      % The hypothesis is that minor products will spread. 
      
      avgRowUp = slope * (halfWay - colsToEnd + 1); % say 1.2
      rowUpLower = floor(avgRowUp);                 % say 1
      rowSplitUpper = avgRowUp - rowUpLower;        % say .2
      rowSplitLower = 1 - rowSplitUpper;            % say .8
      
      % Do the lower row
      % Don't go out of bounds
      crackRow = preRow - rowUpLower;
      if crackRow < 0
          crackRow = 0;
      end
      
      fullCrackStoich = crackStoich;
      
      crackStoich = fullCrackStoich * rowSplitLower;
      disperseFragment2D(crackRow, crackCol, crackStoich);
      
      % Do the upper row
      % Don't go out of bounds
      crackRow = preRow - rowUpLower - 1;
      if crackRow < 0
          crackRow = 0;
      end
      
      crackStoich = fullCrackStoich * rowSplitUpper;
      disperseFragment2D(crackRow, crackCol, crackStoich);
    end

    colsToEnd = colsToEnd - 1;

end
    
end

function processAgingTensor

global Cs Temp Param

Cs.splitProdArrayCombined_1t0N = [];

for precRow = 0:1:Cs.rows2D-1

    for precCol = 0:1:Cs.cols2D-1
        
        % The basic cracking pattern depends on the columns to the end
        % NOT including the current column (ie, this can be zero).
        colsToEnd = Cs.cols2D9 - precCol - 1;
    
        % We assume that the fragments have equal molar yields
        % But this means that by mass they are heavily weighted toward
        % Larger compounds.
        % This should really be carbon number, but we will do bins.

        cBinEquiv = (colsToEnd + 1) * colsToEnd/2;
    
        crackPrecursor2D(precRow, precCol, colsToEnd, cBinEquiv);
        % To implement the combining of all split aging matrices into a single
        % matrix
        
        % sum up all the mass above the upper limit of volatility
        if Param.volatilityRange(2) < 9
            Temp.splitProdArray_1t0N = reshape(Temp.splitProdArray_1t0N, Cs.cols2D9, Cs.rows2D)';
            Temp.splitProdArray_1t0N = [Temp.splitProdArray_1t0N(:, 1:Cs.cols2D-1 ) sum(Temp.splitProdArray_1t0N(:, Cs.cols2D:Cs.cols2D9), 2)];
            Temp.splitProdArray_1t0N = reshape(Temp.splitProdArray_1t0N', Cs.cols2D * Cs.rows2D, [])';
        end
        
        Cs.splitProdArrayCombined_1t0N = [Cs.splitProdArrayCombined_1t0N; Temp.splitProdArray_1t0N];
    end
end

end