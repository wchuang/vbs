function [CStarBasis, O2CBasis, dHvapBasis] = CNum2DModified
% cNum2D
%   Return basic properties in the 2D basis set, esp avg carbon number
%   This is mostly for reference, but also important to OM calc

global Cs Param

% Creates the CStarBasis Array; e.g. CStarBasis = [1e-5 1e-4 1e-3 1e-2 1e-1 1e0 1e1 1e2 1e3 1e4 1e5 1e6 1e7 1e8 1e9]
CStarBasis = zeros(1, Param.volatilityRange(2)-Param.volatilityRange(1)+1);
for bin = 1:1:Param.volatilityRange(2)-Param.volatilityRange(1)+1
    CStarBasis(1,bin) = 10^(Param.volatilityRange(1)+bin-1);
end

% Creates the O:C Arrays
O2CBasis = zeros( ((Param.O2CRange(2)-Param.O2CRange(1)) / .1)+1 , 1);
for O2C = 1:1:((Param.O2CRange(2)-Param.O2CRange(1)) / .1)+1
    O2CBasis(O2C,1) = Param.O2CRange(2) - (O2C - 1)*.1;
end

% loop through O:C numbers from highest to lowest, then
% loop through volatility bins from lowest to highest.
% Start with highest O:C and go through all bins
totalRows = (Param.O2CRange(2)-Param.O2CRange(1))/.1 + 1;
totalColumns = Param.volatilityRange(2)-Param.volatilityRange(1) + 1;
Cs.rows2D = totalRows;
Cs.cols2D = totalColumns;
% number of columns if max C* = 9
Cs.cols2D9 = (9-Param.volatilityRange(1)+1);

Cs.cNumArray = [];
Cs.cNumArray1N = [];
Cs.cNumArray9 = [];
Cs.cNumArray1N9 = [];

for rowNumber = 0:1:totalRows-1
   for columnNumber = 0:1:totalColumns-1
       % Need different carbon numbers for the 1N layer because of the 2.5 decrease in volatility.
       [cNumElement, cNumElement1N] = carbonMatrix(Param.O2CRange(2)-.1*(rowNumber),Param.volatilityRange(1) + columnNumber);
       Cs.cNumArray = [Cs.cNumArray, cNumElement];
       Cs.cNumArray1N = [Cs.cNumArray1N, cNumElement1N];
       %Cs.cNumArray(1, (rowNumber * Cs.cols2D + (columnNumber+1))) = carbonMatrix(OtoCRange(2)-.1*(rowNumber),volatilityRange(1) + columnNumber);
   end
   
   % cNum array that extends to volatility bin of C*=9 to create
   % fragmentation aging matrix
   for columnNumber = 0:1:(9-Param.volatilityRange(1)+1)-1
       [cNumElement, cNumElement1N] = carbonMatrix(Param.O2CRange(2)-.1*(rowNumber),Param.volatilityRange(1) + columnNumber);
       Cs.cNumArray9 = [Cs.cNumArray9, cNumElement];
       Cs.cNumArray1N9 = [Cs.cNumArray1N9, cNumElement1N];
   end
end

switch Param.heatVap
    case 'orig'
        % At present the dH is only a function of C*.  That could change...  This is kJ/mole.
        dHvapBasis = zeros(1, Param.volatilityRange(2)-Param.volatilityRange(1)+1);
        for bin = 1:1:Param.volatilityRange(2)-Param.volatilityRange(1)+1
            dHvapBasis(1,bin) = -6*(Param.volatilityRange(1)+bin-1) + 90;
        end
    case 'Epstein'
        dHvapBasis = zeros(1, Param.volatilityRange(2)-Param.volatilityRange(1)+1);
        for bin = 1:1:Param.volatilityRange(2)-Param.volatilityRange(1)+1
            dHvapBasis(1,bin) = -11*(Param.volatilityRange(1)+bin-1) + 129;
        end
    otherwise
        warning('VBS:parameters','Please enter valid choice for heat of vaporization in parameters.m')
end

% This is to create the array used in calculating the fractionFragmenting
% array and fraction Functionalizing array in the aging functions.
% e.g. creates [1 1 1 1 .9 .9 .9 .9 .8 .8 .8 .8 ...]
% of size [1, totalRow*totalCol]

%% 1D array of O:C 
O2CBasisT = O2CBasis';
Cs.O2CExtended = O2CBasisT(repmat(1:end, [1 1]), repmat(1:end, [Cs.cols2D 1]));

% Rate Constant for Vapor Phase reactions dependent on carbon number and O:C.
k_basis = Param.kPerC * (Cs.cNumArray + Param.kFacPerO * (Cs.cNumArray .* Cs.O2CExtended) + Param.kFacPerH * Cs.O2CExtended.^2);
if Param.layersOfN > 0
    k_basis1N = Param.kPerC * (Cs.cNumArray1N + Param.kFacPerO * (Cs.cNumArray1N .* Cs.O2CExtended) + Param.kFacPerH * Cs.O2CExtended.^2);
else
    k_basis1N = [];
end
Cs.k_basis = [k_basis k_basis1N];
% SAR produces negative rate constants at high C* and high O:C in the 1N layer.
Cs.k_basis(Cs.k_basis < 0) = 5e-13; 
end

% calculate the carbon number in each bin
function [carbonNumber, carbonNumber1N] = carbonMatrix(OtoCValue, volatilityBin)

bin0Carbon = 25;
decadesPerCarbon = 0.5;
decadesPerOxygen = 1.7;
bin0Carbon1N = 25 - 2*2.5;

carbonNumber = (bin0Carbon*decadesPerCarbon - volatilityBin) / (decadesPerCarbon + decadesPerOxygen * OtoCValue);
carbonNumber1N = (bin0Carbon1N*decadesPerCarbon - volatilityBin) / (decadesPerCarbon + decadesPerOxygen * OtoCValue);

end

