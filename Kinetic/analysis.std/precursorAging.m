function precursorAging(dTau,stoich)

global Cs

if nargin < 2, stoich = 1; end; % Accounts for OH yield...

% Assuming first-order kinetics, here is the change in precursor
% in one short timestep.
dC_prec = (1 - exp(- dTau)) * Cs.C_prec * stoich;
 
Cs.C_prec = Cs.C_prec - dC_prec; % lose some precursor
Cs.C_prod = Cs.C_prod + dC_prec; % gain generic product, info only
    
% Distribute the product (carbon mass) in 2D to the vapor!

Cs.C_OC_vap_2D_all = Cs.C_OC_vap_2D_all + dC_prec * Cs.CYield_2D_all;