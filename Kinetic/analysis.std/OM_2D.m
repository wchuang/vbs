function [C_OM_part_2D_all, FMat_all] = OM_2D(Fs, C_OM_tot_2D_all)

global Cs Param

% make sure that the partitioning vector is a row vector
S = size(Fs);
if (S(2) == 1), Fs = Fs'; end;

FMat_all = repmat(Fs, 1,  Cs.rows2D * (Param.layersOfN+1));
C_OM_part_2D_all = C_OM_tot_2D_all .* FMat_all;