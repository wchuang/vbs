% This file creates the fragmentation matrices originally implemented
% in splitOH2D.pl, albeit without the matrix display. If you wish to see the
% exact fragmentation matrix, please run splitOH2D_modified.pl with
% @logVolatilityRange and @O2CBasisRange changed to your desired ranges

% Note: replaced Cs.cols2D with (9-Param.volatilityRange(1)+1) to make
% fragmentation independent of the upper limit of the volatility bins
% specified in the parameters.m file

function splitOH2D_0N
setUpSplitOxidationKernel;
processAgingTensor;
end

function setUpSplitOxidationKernel

global Cs

% Each row up is an added oxygen.
% Each column is a DROPPED decade in C*
% The perl 2D is ( rowNum * numCols + colNum )

% This kernel is built around the idea that oxidation will add 1, 2, or 3 Os
% And the drop in logC* per O ranges from -1 to -2.25, with an average of -1.7

Cs.colsAging = 8;
Cs.rowsAging = 4;

Cs.splitOxidationKernel = zeros(1,Cs.rowsAging * Cs.colsAging);

Cs.splitOxidationKernel(0 * Cs.colsAging + 0 +1) = 0.50;
Cs.splitOxidationKernel(1 * Cs.colsAging + 0 +1) = 0.09+.03/2;
Cs.splitOxidationKernel(1 * Cs.colsAging + 1 +1) = 0.15+.05/2;
Cs.splitOxidationKernel(1 * Cs.colsAging + 2 +1) = 0.06+.02/2;
Cs.splitOxidationKernel(2 * Cs.colsAging + 1 +1) = 0.02;
Cs.splitOxidationKernel(2 * Cs.colsAging + 2 +1) = 0.04;
Cs.splitOxidationKernel(2 * Cs.colsAging + 3 +1) = 0.03;
Cs.splitOxidationKernel(2 * Cs.colsAging + 4 +1) = 0.01;
Cs.splitOxidationKernel(3 * Cs.colsAging + 2 +1) = 0.01/2;
Cs.splitOxidationKernel(3 * Cs.colsAging + 3 +1) = 0.02/2;
Cs.splitOxidationKernel(3 * Cs.colsAging + 4 +1) = 0.04/2;
Cs.splitOxidationKernel(3 * Cs.colsAging + 5 +1) = 0.02/2;
Cs.splitOxidationKernel(3 * Cs.colsAging + 6 +1) = 0.01/2;
% Add 1 because Matlab indices start at 1
end

function disperseFragment2D(precRow, precCol, crackStoich)

global Cs Temp

% Using the suffix '9' at the end of the variable to indicate upper
% volatility limit of 9. This is to make the fragmentation kernel
% independent of the desired limit.
cNum = Cs.cNumArray9(precRow * Cs.cols2D9 + precCol +1);
unitOtoCRows = 1/cNum;

for oxyAdd = 0:1:Cs.rowsAging-1
    avgRowUp = oxyAdd * unitOtoCRows * 10; % say 1.2
    rowUpLower = floor(avgRowUp);          % say 1
    rowSplitUpper = avgRowUp - rowUpLower; % say .2
    rowSplitLower = 1 - rowSplitUpper;     % say .8

   for cStarDown = 0:1:Cs.colsAging-1

        thisStoich = Cs.splitOxidationKernel(oxyAdd * Cs.colsAging + cStarDown +1);
        if thisStoich ~= 0
            
            thisStoich = thisStoich * crackStoich;
            
            % Figure out column and don't go out of bounds
            prodCol = precCol - cStarDown;
            if prodCol < 0
                prodCol = 0;
            end
            
            % The products will always be divided between an upper and lower row
            % This is confused because the VISUAL row number advances downward
            % (ie, the printed matrix[0,0] element is in the upper left
            % and to preserve visual consistency we are dropping row numbers with oxy.

            % Do the lower row
            % Don't go out of bounds
            prodRow = precRow - rowUpLower;
            if prodRow < 0
                prodRow = 0;
            end
            prodStoich = thisStoich * rowSplitLower;
            Temp.splitProdArray(prodRow * Cs.cols2D9 + prodCol +1) = Temp.splitProdArray(prodRow * Cs.cols2D9 + prodCol +1) + prodStoich;
            
            % Do the upper row
            % Don't go out of bounds
            prodRow = precRow - rowUpLower - 1;
            if prodRow < 0
                prodRow = 0;
            end
            prodStoich = thisStoich * rowSplitUpper;
            Temp.splitProdArray(prodRow * Cs.cols2D9 + prodCol +1) = Temp.splitProdArray(prodRow * Cs.cols2D9 + prodCol +1) + prodStoich;
        end
   end
end

end

function crackPrecursor2D(preRow, preCol, colsToEnd, cBinEquiv)

global Cs
global Temp

% Initialize the aging matrix to zeros
Temp.splitProdArray = zeros(1,length(Cs.cNumArray9));

if (colsToEnd == 0)
   Temp.splitProdArray(0 * Cs.cols2D9 + Cs.cols2D9) = 1; 
end

% The cracking will turn toward the upper right at the 1/2 way point
halfWay = colsToEnd - floor(colsToEnd/2);
slope = (preRow+1)/halfWay;
    
% We are cracking, so we go from the precursor column forward
for crackCol = preCol+1:1:Cs.cols2D9-1
    % This is the carbon weighted stoichiometry
    crackStoich = colsToEnd / cBinEquiv;
    
    if colsToEnd > halfWay
       % until 1/2 way we just move to the right
       
       disperseFragment2D(preRow, crackCol, crackStoich);
       
    else
      % Beyond 1/2 way we slope up to the upper right
      % Frags will split between 2 rows.
      % This is due to averaging but it is desired anyway.
      % The hypothesis is that minor products will spread. 
      
      avgRowUp = slope * (halfWay - colsToEnd + 1); % say 1.2
      rowUpLower = floor(avgRowUp);                 % say 1
      rowSplitUpper = avgRowUp - rowUpLower;        % say .2
      rowSplitLower = 1 - rowSplitUpper;            % say .8
      
      % Do the lower row
      % Don't go out of bounds
      crackRow = preRow - rowUpLower;
      if crackRow < 0
          crackRow = 0;
      end
      
      fullCrackStoich = crackStoich;
      
      crackStoich = fullCrackStoich * rowSplitLower;
      disperseFragment2D(crackRow, crackCol, crackStoich);
      
      % Do the upper row
      % Don't go out of bounds
      crackRow = preRow - rowUpLower - 1;
      if crackRow < 0
          crackRow = 0;
      end
      
      crackStoich = fullCrackStoich * rowSplitUpper;
      disperseFragment2D(crackRow, crackCol, crackStoich);
    end

    colsToEnd = colsToEnd - 1;

end
    
end

function processAgingTensor

global Cs Temp Param

Cs.splitProdArrayCombined_0t0N = [];

for precRow = 0:1:Cs.rows2D-1

    for precCol = 0:1:Cs.cols2D-1
        
        % The basic cracking pattern depends on the columns to the end
        % NOT including the current column (ie, this can be zero).
        colsToEnd = Cs.cols2D9 - precCol - 1;
    
        % We assume that the fragments have equal molar yields
        % But this means that by mass they are heavily weighted toward
        % Larger compounds.
        % This should really be carbon number, but we will do bins.

        cBinEquiv = (colsToEnd + 1) * colsToEnd/2;
    
        crackPrecursor2D(precRow, precCol, colsToEnd, cBinEquiv);
        % To implement the combining of all split aging matrices into a single
        % matrix
        if Param.volatilityRange(2) < 9
            Temp.splitProdArray = reshape(Temp.splitProdArray, Cs.cols2D9, Cs.rows2D)';
            Temp.splitProdArray = [Temp.splitProdArray(:, 1:Cs.cols2D-1 ) sum(Temp.splitProdArray(:, Cs.cols2D:Cs.cols2D9), 2)];
            Temp.splitProdArray = reshape(Temp.splitProdArray', Cs.cols2D * Cs.rows2D, [])';
        end
        
        Cs.splitProdArrayCombined_0t0N = [Cs.splitProdArrayCombined_0t0N; Temp.splitProdArray];
    end
end

end
