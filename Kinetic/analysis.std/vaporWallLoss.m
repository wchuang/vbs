function vaporWallLoss(dtau)

% particleWallLoss( dtau )
%  
% Transfers mass from vapor array to wall
% 
% dtau is the pseudo first order timestep for the transfer

global Cs

dC_vap_all = Cs.C_OC_vap_2D_all * (1-exp(-dtau));

% Take mass away from vapor array
Cs.C_OC_vap_2D_all = Cs.C_OC_vap_2D_all - dC_vap_all;

% And give it to the adsorption array
Cs.C_OC_ads_2D_all = Cs.C_OC_ads_2D_all + dC_vap_all;