function computeOrganicEquilibrium

% computeOrganicEquilibrium
%    Figure out partitioning and OM levels in 1 and 2 D based on the 
%    `tracking property', which is 2D OC levels.
%    Tracking property is the property actually carried by the model

% This version is an equilibrium version based on the different carbon
% pools being the "tracking" variables, with total added up from them.
% It is just suitable for simulations of chamber experiments where there 
% has been wall deposition, etc...

global Cs Param

% Always run Clausius Clapeyron
R = 8.314472/1000;
Cs.C_Star = Cs.C_Star300 .* (Cs.T / 300) .* ...
    exp(- (Cs.dHv / (R * Cs.T) - Cs.dHv / (R * 300) ));

% The model is actually predicting organic carbon in various pools,
% including vapor, suspended particles, and wall-bound particles

% This particular algorithm figures out the total of these three along with
% the ratio of particle mass in suspension and on the walls.  With the
% total organic loading, the equilibrium partitioning can be calculated,
% and with the fraction on the walls the condensed-phase stuff can be
% divided between suspension and the walls

% The base array is the total carbon concentration
% while sub arrays deal with fractions in various reservoirs.

Cs.C_OC_tot_2D_all = Cs.C_OC_vap_2D_all + Cs.C_OC_susp_2D_all + Cs.C_OC_wall_2D_all;

% A few things we will need internally but not in the Cs structure
% The total particle mass is the sum of the suspended and wall carbon
% For equilibrium calculations we need the sum
% The mass fraction on the wall is total mass because this is a mixture

Cs.C_OC_susp = sum(Cs.C_OC_susp_2D_all);
Cs.C_OC_wall = sum(Cs.C_OC_wall_2D_all);

C_OC_part_2D_all = Cs.C_OC_susp_2D_all + Cs.C_OC_wall_2D_all;
C_OC_part = sum(C_OC_part_2D_all);

% Fraction of aerosols on the wall and suspended.
frac_OC_wall_all = Cs.C_OC_wall ./ (C_OC_part + eps);
frac_OC_susp_all = (1 - frac_OC_wall_all);

% Here are the carbon mass concentrations in 2 and 0 dimensions.
% 2D is the base, written in a 1D array, and collapsed into 0D (total).
Cs.C_OC_tot = sum(Cs.C_OC_tot_2D_all); % total carbon mass of all organics (should approach C_prec as time -> inf)

% Here are the total organic mass concentrations in 2 and 0 D
[Cs.C_OM_tot_2D_all] = OC2OM(Cs.C_OC_tot_2D_all);
Cs.C_OM_tot = sum(Cs.C_OM_tot_2D_all); % total mass of all organics

%% 1D carbon and organic mass concentrations.
summingMatrix2 = repmat(eye(Cs.cols2D), [Cs.rows2D *(Param.layersOfN + 1),1]);

Cs.C_OC_tot_1D_all = Cs.C_OC_tot_2D_all * summingMatrix2;
Cs.C_OM_tot_1D_all = Cs.C_OM_tot_2D_all * summingMatrix2;
%%

% Now find out what is in the condensed phase
% The basic partitioning script is from the 1D basis set
Fs = partitionAerosol(Cs.C_Star, Cs.C_OM_tot_1D_all);
Cs.Fs_1D = Fs;

% Figure out quantities in each pool in the 2D basis set
% First total organic mass, because this drives the partitioning
[C_OM_part_2D_all, Cs.Fs_2D_all] = OM_2D(Fs, Cs.C_OM_tot_2D_all);

% Partitioned quantities
Cs.C_OM_susp_2D_all = frac_OC_susp_all * C_OM_part_2D_all;
Cs.C_OM_wall_2D_all = frac_OC_wall_all * C_OM_part_2D_all;
Cs.C_OM_vap_2D_all = Cs.C_OM_tot_2D_all .* (1-Cs.Fs_2D_all);

% Next carbon mass, because this is what we actually conserve (we hope)
Cs.C_OC_vap_2D_all  = Cs.C_OC_tot_2D_all .* (1-Cs.Fs_2D_all);
   C_OC_part_2D_all = Cs.C_OC_tot_2D_all .* (Cs.Fs_2D_all);
Cs.C_OC_susp_2D_all = frac_OC_susp_all    * C_OC_part_2D_all;
Cs.C_OC_wall_2D_all = frac_OC_wall_all    * C_OC_part_2D_all;

% There are some other pools we care about
[Cs.C_OM_ads_2D_all] = OC2OM(Cs.C_OC_ads_2D_all);
% get the OC for susp particles in order to get the OM:OC later
% [Cs.C_OC_susp_2D_all] = OM2OC(Cs.C_OM_susp_2D_all);

% Find the total organic mass and carbon mass in each pool.
% Reshaped to (cols*rows) by (layersOfN+1)
C_OC_susp_reshaped = reshape(Cs.C_OC_susp_2D_all, Cs.cols2D * Cs.rows2D , []);
C_OC_wall_reshaped = reshape(Cs.C_OC_wall_2D_all, Cs.cols2D * Cs.rows2D , []);
C_OC_vap_reshaped  = reshape(Cs.C_OC_vap_2D_all , Cs.cols2D * Cs.rows2D , []);
C_OC_ads_reshaped  = reshape(Cs.C_OC_ads_2D_all , Cs.cols2D * Cs.rows2D , []);
C_OM_susp_reshaped = reshape(Cs.C_OM_susp_2D_all, Cs.cols2D * Cs.rows2D , []);
C_OM_wall_reshaped = reshape(Cs.C_OM_wall_2D_all, Cs.cols2D * Cs.rows2D , []);
C_OM_vap_reshaped  = reshape(Cs.C_OM_vap_2D_all , Cs.cols2D * Cs.rows2D , []);
C_OM_ads_reshaped  = reshape(Cs.C_OM_ads_2D_all , Cs.cols2D * Cs.rows2D , []);

% Sum of all OC(OM) within each state, regardless of N; 1 by 1
Cs.C_OC_susp = sum(Cs.C_OC_susp_2D_all);
Cs.C_OC_wall = sum(Cs.C_OC_wall_2D_all);
Cs.C_OC_vap  = sum(Cs.C_OC_vap_2D_all);
Cs.C_OC_ads  = sum(Cs.C_OC_ads_2D_all);
Cs.C_OM_susp = sum(Cs.C_OM_susp_2D_all);
Cs.C_OM_wall = sum(Cs.C_OM_wall_2D_all);
Cs.C_OM_vap  = sum(Cs.C_OM_vap_2D_all);
Cs.C_OM_ads  = sum(Cs.C_OM_ads_2D_all);
%%
if Param.layersOfN > 0
    % Sum all the OC(OM) within each N; 1 by layersOfN
    Cs.C_OC_susp_N = sum(C_OC_susp_reshaped);
    Cs.C_OC_wall_N = sum(C_OC_wall_reshaped);
    Cs.C_OC_vap_N  = sum(C_OC_vap_reshaped) ;
    Cs.C_OC_ads_N  = sum(C_OC_ads_reshaped) ;
    Cs.C_OM_susp_N = sum(C_OM_susp_reshaped);
    Cs.C_OM_wall_N = sum(C_OM_wall_reshaped);
    Cs.C_OM_vap_N  = sum(C_OM_vap_reshaped) ;
    Cs.C_OM_ads_N  = sum(C_OM_ads_reshaped) ;
    
    % Is accomplished by the reshape function
    layerArray = 0:Param.layersOfN;
    layerArrayFull = layerArray(1, repmat(1:end, [Cs.rows2D * Cs.cols2D, 1]));
    carbonMassArray = [12.*Cs.cNumArray, 12.*Cs.cNumArray1N];
    Cs.massOfNO2 = sum((Cs.C_OC_susp_2D_all./carbonMassArray .* layerArrayFull) * 46);

    % Matrix to be used for summing all volatilities & O:C for each N
    idenMat = eye(Param.layersOfN + 1);
    mat_sumPerN = idenMat(repmat(1:end, Cs.rows2D * Cs.cols2D ,1),repmat(1:end,1,1));

    % Of Size 1 by (layersOfN+1)
    Cs.C_OC_susp_all_N = Cs.C_OC_susp_2D_all * mat_sumPerN;
    Cs.C_OC_wall_all_N = Cs.C_OC_wall_2D_all * mat_sumPerN;
    Cs.C_OC_vap_all_N  = Cs.C_OC_vap_2D_all  * mat_sumPerN;
    Cs.C_OC_ads_all_N  = Cs.C_OC_ads_2D_all  * mat_sumPerN;
    Cs.C_OM_susp_all_N = Cs.C_OM_susp_2D_all * mat_sumPerN;
    Cs.C_OM_wall_all_N = Cs.C_OM_wall_2D_all * mat_sumPerN;
    Cs.C_OM_vap_all_N  = Cs.C_OM_vap_2D_all  * mat_sumPerN;
    Cs.C_OM_ads_all_N  = Cs.C_OM_ads_2D_all  * mat_sumPerN;

    % We also may be interested in the OM:OC
    Cs.OM_OC_susp_N = (Cs.C_OM_susp_N ./ Cs.C_OC_susp_N)';
    Cs.OM_OC_susp_overall = Cs.C_OM_susp / Cs.C_OC_susp;

%{
% Cs.AMF = Cs.C_OM / Cs.C_prod;
%}

    %% Getting the O:C of suspended stuff
    % This may also be useful for activity coefficient calculations
    % First add up the mass in each O:C and each N for all volatilities

    iden = eye(Cs.rows2D * (Param.layersOfN +1));
    volatilitySummingMat = iden(repmat(1:end, [Cs.cols2D 1]), repmat(1:end, [1 1]));
    Cs.OC_1DOx_susp_N = Cs.C_OC_susp_2D_all * volatilitySummingMat ; % mass in ea. O:C and ea. N for all volatilities
    Cs.OM_1DOx_susp_N = Cs.C_OM_susp_2D_all * volatilitySummingMat ; % mass in ea. O:C and ea. N for all volatilities

    % Get the susp O:C for each layer of N
    Cs.OC_1DOx_susp_N_layered  = reshape(Cs.OC_1DOx_susp_N, Cs.rows2D, Param.layersOfN + 1)';
    Cs.OtoC_susp_N = (Cs.OC_1DOx_susp_N_layered * Cs.O2C) ./ Cs.C_OC_susp_all_N';

    % Then add up the mass in each O:C for all N
    ident = eye(Cs.rows2D);
    Cs.NsummingMat = repmat(ident, Param.layersOfN + 1, 1);
    Cs.OC_1DOx_susp = Cs.OC_1DOx_susp_N * Cs.NsummingMat;
else
    iden = eye(Cs.rows2D * (Param.layersOfN + 1));
    volatilitySummingMat = iden(repmat(1:end, [Cs.cols2D 1]), repmat(1:end, [1 1]));
    Cs.OC_1DOx_susp = Cs.C_OC_susp_2D_all * volatilitySummingMat ; % mass in ea. O:C for all volatilities
end

% Then get a weighted average of O:C based on OM in each row
Cs.OtoC_susp_all = (Cs.OC_1DOx_susp * Cs.O2C) / sum(Cs.C_OC_susp_2D_all);
