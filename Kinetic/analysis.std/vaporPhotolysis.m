function vaporPhotolysis(dTauBase,photExponent)

global Cs Param

if dTauBase == 0, return; end;
if nargin < 2, photExponent = 2; end;

% Assuming pseudo first-order kinetics,
% here are factors for what is lost and also what is left

jRowScale = Cs.O2CExtended.^(photExponent); % 1 by cols*rows
dTau = dTauBase * jRowScale; 

firstOrderLeftGas = exp(-dTau); 
firstOrderLossGas = 1 - firstOrderLeftGas;

% We are going to model photolysis as a fragmentation process
% This can be adjusted and included in the following equations
% fractionFragmenting = 1;

% The aging matrix conserves carbon mass, so just multiply by
% the amount of carbon in this bucket and spray away...
% then finally put in the stuff that did not react this step

C_OC_new_all = (firstOrderLossGas .* Cs.C_OC_vap_2D_all * Param.photFractionFrag) * cell2mat(Cs.photolysisCell) ...
            + firstOrderLeftGas .* Cs.C_OC_vap_2D_all;
        
Cs.C_OC_vap_all = C_OC_new_all; 

