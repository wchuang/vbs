% Created by WKC 7/11/12
% RONO2 compounds tend to split NO2 off. 
% This file produces matrices for subsequent RO radical functionalization.

function splitOxOH2D_1N
setUpoxOxidationKernel_1N;
processAgingTensor;
end

function setUpoxOxidationKernel_1N

global Cs

% Each row up is an added oxygen.
% Each column is a DROPPED decade in C*
% The perl 2D is [ rowNum * numCols + colNum ]

% This kernel is built around the idea that oxidation will add 1, 2, or 3 Os
% And the drop in logC* per O ranges from -1 to -2.25, with an average of -1.7

Cs.colsAging = 8;
Cs.rowsAging = 4;

Cs.oxOxidationKernel_1N = zeros(1,Cs.rowsAging * Cs.colsAging);

Cs.oxOxidationKernel_1N(1 * Cs.colsAging + 1 +1) = 0.09;
Cs.oxOxidationKernel_1N(1 * Cs.colsAging + 2 +1) = 0.15;
Cs.oxOxidationKernel_1N(1 * Cs.colsAging + 3 +1) = 0.06;
Cs.oxOxidationKernel_1N(2 * Cs.colsAging + 2 +1) = 0.10;
Cs.oxOxidationKernel_1N(2 * Cs.colsAging + 3 +1) = 0.20;
Cs.oxOxidationKernel_1N(2 * Cs.colsAging + 4 +1) = 0.15;
Cs.oxOxidationKernel_1N(2 * Cs.colsAging + 5 +1) = 0.05;
Cs.oxOxidationKernel_1N(3 * Cs.colsAging + 3 +1) = 0.02;
Cs.oxOxidationKernel_1N(3 * Cs.colsAging + 4 +1) = 0.04;
Cs.oxOxidationKernel_1N(3 * Cs.colsAging + 5 +1) = 0.08;
Cs.oxOxidationKernel_1N(3 * Cs.colsAging + 6 +1) = 0.04;
Cs.oxOxidationKernel_1N(3 * Cs.colsAging + 7 +1) = 0.02;

end

function agePrecursor2D(precRow, precCol)

global Cs
global Param
global Temp

cNum = Cs.cNumArray9(precRow * Cs.cols2D9 + precCol +1);
unitOtoCRows = 1/cNum;
% Initialize the aging matrix to zeros
Temp.oxProdArray_1t0N = zeros(1,length(Cs.cNumArray9));

for oxyAdd = 0:1:Cs.rowsAging-1
    avgRowUp = oxyAdd * unitOtoCRows * 10 - 1; % say 1.2 - Subtract 1 for removal of ONO2.
    rowUpLower = floor(avgRowUp);          % say 1
    rowSplitUpper = avgRowUp - rowUpLower; % say .2
    rowSplitLower = 1 - rowSplitUpper;     % say .8

    for cStarDown = 0:1:Cs.colsAging-1
        thisStoich = Cs.oxOxidationKernel_1N(oxyAdd * Cs.colsAging + cStarDown +1);

        if thisStoich ~= 0
            % Figure out column and don't go out of bounds. Differs from
            % original by an increase in bins by -2.5.
            colLeftLower = floor(Param.NO2shift);
            colSplitUpper = Param.NO2shift - colLeftLower;
            colSplitLower = 1 - colSplitUpper;

            prodUpperCol = precCol - cStarDown - floor(Param.NO2shift);
            if prodUpperCol < 0
                prodUpperCol = 0;
            end
            if prodUpperCol > Cs.cols2D9 - 1
                prodUpperCol = Cs.cols2D9 - 1;
            end
            
            prodLowerCol = precCol - cStarDown - ceil(Param.NO2shift);
            if prodLowerCol < 0
                prodLowerCol = 0;
            end
            if prodLowerCol > Cs.cols2D9 - 1
                prodLowerCol = Cs.cols2D9 - 1;
            end
            
            % The products will always be divided between an upper and lower row
            % This is confusing because the VISUAL row number advances downward
            % (ie, the printed matrix[0,0] element is in the upper left
            % and to preserve visual consistency we are dropping row numbers with oxy.

            % Do the lower row
            % Don't go out of bounds
            prodLowerRow = precRow - rowUpLower;
            if prodLowerRow < 0,             prodLowerRow = 0;             end
            if prodLowerRow > Cs.rows2D - 1, prodLowerRow = Cs.rows2D - 1; end
            
            prodStoich = thisStoich * rowSplitLower * colSplitUpper;
            Temp.oxProdArray_1t0N(prodLowerRow * Cs.cols2D9 + prodUpperCol + 1) = Temp.oxProdArray_1t0N(prodLowerRow * Cs.cols2D9 + prodUpperCol + 1) + prodStoich;
            prodStoich = thisStoich * rowSplitLower * colSplitLower;
            Temp.oxProdArray_1t0N(prodLowerRow * Cs.cols2D9 + prodLowerCol + 1) = Temp.oxProdArray_1t0N(prodLowerRow * Cs.cols2D9 + prodLowerCol + 1) + prodStoich;            
            
            % Do the upper row
            % Don't go out of bounds
            prodUpperRow = precRow - rowUpLower - 1;
            if prodUpperRow < 0,             prodUpperRow = 0;             end
            if prodUpperRow > Cs.rows2D - 1, prodUpperRow = Cs.rows2D - 1; end
            
            prodStoich = thisStoich * rowSplitUpper * colSplitUpper;
            Temp.oxProdArray_1t0N(prodUpperRow * Cs.cols2D9 + prodUpperCol + 1) = Temp.oxProdArray_1t0N(prodUpperRow * Cs.cols2D9 + prodUpperCol + 1) + prodStoich;
            prodStoich = thisStoich * rowSplitUpper * colSplitLower;
            Temp.oxProdArray_1t0N(prodUpperRow * Cs.cols2D9 + prodLowerCol + 1) = Temp.oxProdArray_1t0N(prodUpperRow * Cs.cols2D9 + prodLowerCol + 1) + prodStoich;
        end
    end
end

end

function processAgingTensor

global Cs Temp Param

Cs.oxProdArrayCombined_1t0N = [];

for precRow = 0:1:Cs.rows2D-1
    for precCol = 0:1:Cs.cols2D-1
        
        agePrecursor2D(precRow, precCol);
        % To implement the combining of all ox aging matrices into a single
        % matrix
        
        % sum up all the mass above the upper limit of volatility
        if Param.volatilityRange(2) < 9
            Temp.oxProdArray_1t0N = reshape(Temp.oxProdArray_1t0N, Cs.cols2D9, Cs.rows2D)';
            Temp.oxProdArray_1t0N = [Temp.oxProdArray_1t0N(:, 1:Cs.cols2D-1 ) sum(Temp.oxProdArray_1t0N(:, Cs.cols2D:Cs.cols2D9), 2)];
            Temp.oxProdArray_1t0N = reshape(Temp.oxProdArray_1t0N', Cs.cols2D * Cs.rows2D, [])';
        end
        
        Cs.oxProdArrayCombined_1t0N = [Cs.oxProdArrayCombined_1t0N; Temp.oxProdArray_1t0N];
    end
end

end

