
disp('Local startup stuff')

%%%%%%%%%%
% Standard path construction
% All analysis scripts in the donahue group are designed to be
% run within a standardized directory structure.

% This is where all of the CMU packages live.
% Change it as necessary to reflect where you put them!
% Or, open MATLAB from the current folder

currentDir = pwd;
scriptDirCMU = [currentDir '\scripts\'];

if exist([scriptDirCMU  '\whereTheyAre'], 'dir') == 7
  addpath([scriptDirCMU '\whereTheyAre']);
end

addpath(genpath([currentDir '\Kinetic']));

whereThingsAreCMU

