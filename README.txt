Guide to the VBS
    Welcome! The VBS code presented here models the aging process of organic species, including organonitrates. 

    I. Basic instructions
    II. Recent changes and file dependency
	III. Available parameters
    IV. Test runs for current work
    V. Test runs for previous works
	VI. A note on future work 

	
I. Basic Instructions
Start MATLAB from the VBS folder. The startup.m file will run automatically.

Start by checking out the parameters.m file. Within this file are the easily changeable variables. You are encouraged to alter these variables to fit experimental conditions. Another file is aPineneCYields2DHOOH.m, which holds the first generation distribution for alpha-pinene. These are changeable with new experimental data.

Now look at some run files. The file with an ideal chamber (no wall losses) idealExperiment.m is the simplest to run, and can test to make sure all files are in place and run correctly. 
To reproduce the plots within this paper (Chuang and Donahue), look at the file testPlot.m.
The sections commented with the words "Graph for paper" are the plots to run to reproduce the graphs.


II. Recent changes and file dependency

New for v1.0:
    Organonitrate formation incorporated
        Organonitrate alpha-pinene first generation yields from Pathak et al., 2007
    Vapor phase kinetics (k_basis) is C* and O:C dependent.

Dependency of code to set up Matlab:
startup
    whereThingsAreCMU
        startupAnalysis
            whereThingsAreSemiVolatile
                whereThingsAreFitting
                
The dependency of the aging code is as follows:
*ExperimentType* 
    commonChamberConstants 
    aPineneAgingTimeSeries 
        parameters
        initCStructure
        initResultStructure
        setupAPinene2D
            aPineneCYields2DHOOH
                CNum2DModified
                oxOH2D_0N
                splitOH2D_0N
                splitSplitOH2D_1N
                splitOx2D_1N
        agingCellBase
        computeYieldArray
        precursorAging
        vaporAging
        suspendedAging
        vaporPhotolysis
        suspendedPhotolysis
        particleWallLoss
        vaporWallLoss
        diluteChamber
        computeOrganicEquilibrium
            OC2OM
            partitionAerosol
            OM_2D
            OC2OM
            OM2OC
        computeYieldArray
    graphNames
    exptPlots_N 
    chamberPlot_N
    
Additional plotting files are under
    bar3graphs (for creating 3D bar graphs of C_OM vs. O:C vs. C*)

	
III. Introduction to available parameters
    The parameters.m file contains the variables available for testing.
	These parameters include:
		Number of organonitrate layers
		Fraction of RO2+NO reactions that form RONO2
		Shift in volatility from the addition of an organonitrate group
		Range of volatility 
		Range of O:C ratios 
		Parameters for component kinetics
		Type of model for heat of vaporization
		Versions of first generation a-pinene distribution
		Type of model for OH aging

		
IV. Test runs for current work
Test runs for the current paper may be run from the file testPlot.m. Within the file, figures that are included in this work are noted as "Graph for paper". This encompasses all figures modeling first generation dark aging distributions and 10 hour chamber results. 
Additional code for reproducing figures is located in bar3graphs.m, which produces a 3D plot of concentration (ug/m3) on VBS cells.


V. Test runs for previous work
Test runs are available for previous published work by Donahue et al., 2012, PNAS (doi:10.1073/pnas.1115186109), for all chamber experiments conducted: CMU, AIDA, PSI, and SAPHIR. 
Slight changes forming the current version of the VBS code accounts for the minor differences in the resulting figures. To reproduce these figures, run these files for these chambers:
CMU: CMUExperiment.m
AIDA: AIDAExperiment.m
PSI: PSIExperiment.m
SAPHIR: SAPHIRExperiment.m
Pdfs of the figures are in the code, under ~\vbs\Kinetic\figs\MUCHACHAS_Paper. 


VI. A note on future work 
I hope that the code can continue to be well-maintained and updated, with further progress to be available for other researchers to use and collaborate with. Undoubtedly, as research goals expand, multiple branches will occur, each tailored for its intended use. However, I would like to allow modifications and improvements to be merged with the original, creating an increasingly robust VBS model as time goes on. Branches that diverge far from this code will (likely) be given its own master and development branches.
As you begin writing your own code and modifying the code within this repository, you can familiarize yourself with the version control system (git), including branching, merging, pulling, pushing, and committing. When you finish/publish your work, my hope is that you will also push your work to the online repository and make it to be available for others to build upon. While the system can be confusing, I hope that at the very least, the current setup provides a basis upon which researchers can have easy access to the model, allowing everyone to work together to further scientific progress. At this point, I am the (novice) moderator for this repository, and welcome enthusiastic/knowledgeable/curious scientists to join in this effort.

My resources for understanding git and the gitflow model:
http://nvie.com/posts/a-successful-git-branching-model/
https://www.atlassian.com/git/tutorials/comparing-workflows