function f = faerosolFrac(X,a,n,p)

% f = faerosolFracLog(X,a,n,p)
% Basic fractional partition 


if nargin < 3, n = 0; end;
if nargin < 4, p = 0; end;

COA = X;
CStar = a;


f = 1./(1+ CStar./(COA));

