function f = faerosolFracLog(X,a,n,p)

% f = faerosolFracLog(X,a,n,p)
% Basic fractional partition sigmoid in 10^x rep


if nargin < 3, n = 0; end;
if nargin < 4, p = 0; end;

log10COA = X;
CStar = a;


f = 1./(1+ CStar./(10.^log10COA));

