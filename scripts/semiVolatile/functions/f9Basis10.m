function f = f9Basis10(X,a,n,p)


% function f = f9Basis10(X,a,n,p)
%
% CStars = [.01, .1, 1, 10, 100, 1000, 1e4, 1e5, 1e6];

global data



if nargin < 3, n = 0; end;
if nargin < 4, p = 0; end;

log10COA = X;
alphas = a;


% This is the characteristic for 9 Basis 10
CStars = [.01, .1, 1, 10, 100, 1000, 1e4, 1e5, 1e6];

% However, we can sneak a new basis vector in through a global definition!
if isfield(data,'CStarBasis'), CStars = data.CStarBasis; end;



f = fSVOCLog(log10COA,alphas,CStars,n,p);
