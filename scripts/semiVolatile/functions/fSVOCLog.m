function f = fSVOCLog(X,a,CStars,n,p)

% note that the x data (log10COA) will be an array and that the 
% alphas and CStars will be a different sized array.
% so we must do some looping.


if nargin < 4, n = 0; end;
if nargin < 5, p = 0; end;

log10COA = X;
alphas = a;


switch n
    case 1; f = 1; return;
end

if p > 0
    alpha = alphas(p);
    CStar = CStars(p);
    f = sign(alpha) * faerosolFracLog(log10COA,CStar);
    return;
end

f = zeros(size(log10COA));
i = 0;
for CStar = CStars
    i = i+1;
    alpha = abs(alphas(i));
    condensedFracArray = faerosolFracLog(log10COA,CStar);
    f = f +  alpha .* condensedFracArray;
end

