function f = f6Basis10(X,a,n,p)


% function f = f6Basis10(X,a,n,p)
%
% CStars = [.01, .1, 1, 10, 100, 1000];



if nargin < 3, n = 0; end;
if nargin < 4, p = 0; end;

log10COA = X;
alphas = a;


% This is the characteristic for 6 Basis 10
CStars = [.01, .1, 1, 10, 100, 1000];


f = fSVOCLog(log10COA,alphas,CStars,n,p);
