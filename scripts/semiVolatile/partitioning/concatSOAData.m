function concatSOAData

% concatSOAData
% 
% requires that present data be in 
% data.COA     OA mass conc
% data.AMF     OA mass frac (yield) COA/dCVOC
% data.sigAMF  OA mass frac unc
% data.ph      plot handle
% data.leg     plot legend

global data

% return if there are no data to concatenate!
if isempty(data.COA), return; end;

sz = size(data.COA);
if sz(2) > 1, data.COA = data.COA'; end;

sz = size(data.AMF);
if sz(2) > 1, data.AMF = data.AMF'; end;

sz = size(data.sigAMF);
if sz(2) > 1, data.sigAMF = data.sigAMF'; end;



% Put the data in the data array
data.all.COA = [data.all.COA; data.COA];
data.all.AMF = [data.all.AMF; data.AMF];
data.all.sigAMF = [data.all.sigAMF; data.sigAMF];

% Put the plot legend information in the right place too
data.plotHandles = [data.plotHandles; data.ph];
hn = length(data.plotHandles);
data.plotLegends(hn).str = data.leg;
