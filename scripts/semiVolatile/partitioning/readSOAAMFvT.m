function [T COA AMF] = readSOAAMFvT(fname,fStr,headerlines,path)

if nargin < 4, path = './prod/'; end;
if nargin < 3, headerlines = 2; end;
if nargin < 2, fStr = '%f %f %f'; end;


[T COA AMF] = textread([path fname '.dat'],fStr,'headerlines',headerlines);

