function alphaOut = processBasisDistribution(alphaIn,alphaProc,CStarProc)

% alphaIn is the inputBasisDistribution
% alphaProc is the processing distribution on the basis set
% CStarProc is the CStar of the precursor leading to alphaProc

CStarBasis = [.01, .1, 1, 10, 100, 1000, 1e4, 1e5, 1e6];

% Set up the output array to accept the processed material
alphaOut = zeros(size(CStarBasis));

for i = 1:length(CStarBasis)
  alphaOut = alphaOut + ...
    alphaIn(i) * processBasis(CStarBasis(i),alphaProc,CStarProc);
end
