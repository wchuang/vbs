function plotBasisOnlyWide(aFlux,CStarBasis,basisColor)

global data

% Here is the basis set
if nargin < 2, CStarBasis = [.01, .1, 1, 10, 100, 1000, 1e4, 1e5, 1e6]; end;
if nargin < 3, basisColor = [0 .5 0]; end;

% Set up the figure as a wide aspect ratio
set(gcf,'Units','inches');
set(gcf,'Position',[1 1 10 6]);
set(gcf,'PaperPosition',[0 0 10 6]);
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperSize', [10 6]);


% Now make the range of the data equally broad --
% this is the standard range for these wide figures so they are uniform
logX = (-3:.1:12)';
XPlot = 10.^logX;

aFlux = aFlux + eps;

semilogx(1e-3,.00001,'k.');
hold on;

% Plot all of the parms as blue bars 
br = stem(CStarBasis,aFlux,'b.');
set(br,'Marker','none');
set(br,'Color',basisColor);
set(br,'LineWidth',30);



