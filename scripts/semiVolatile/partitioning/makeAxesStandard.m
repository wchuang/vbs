function makeAxesStandard(yRange,xRange,subStr,drawLegend)

% Make the range and the fonts, etc, normal
global data

if nargin < 4, drawLegend = 1; end;
if nargin < 3, subStr = []; end;
if nargin < 2
    if isfield(data,'xRange')
        xRange = data.xRange;
    else
        xRange = [1e-2,1e3];
    end
end;
if nargin < 1
    if isfield(data,'yRange')
        yRange = data.yRange;
    else
        yRange = [0,1];
    end
end;
if length(yRange) < 2, yRange = [0, yRange]; end;
    



ax = gca;
set(ax,'FontSize',14);
set(get(ax,'Xlabel'),'String','C_{OA} ({\mu}g m^{-3})');
set(get(ax,'Xlabel'),'FontSize',18);
set(get(ax,'ylabel'),'String','\xi\prime (Normalized Aerosol Mass Fraction)');
set(get(ax,'Ylabel'),'FontSize',18);
set(ax,'YMinorTick','on');
set(ax,'Color','none');
set(ax,'TickLength',[0.025,0.01]);
set(ax,'LineWidth',1.5);
set(gcf,'Color','none');

% Build up the plot legend from the available info
if isfield(data,'plotHandles')
  if ~isempty(data.plotHandles) && drawLegend
    legStr = 'legend(data.plotHandles,';
    for h = 1:length(data.plotHandles)
      legStr = [legStr, 'data.plotLegends(', int2str(h), ').str,'];
    end
    loc = 'Location';
    locStr = 'NorthWest';
    legStr = [legStr, 'loc, locStr);'];
    eval(legStr);
  end
end

ylim(yRange);
xlim(xRange);

if subStr, return; end;
  
  axPos = get(ax,'Position');
  axLeft = axPos(1);
  axWidth = axPos(3);

Xa = (.925 * axWidth) + axLeft;
  ann = annotation('textbox',[Xa,.85,.05,.05]);
  set(ann,'String',subStr)
  set(ann,'FontSize',16)
  set(ann,'FontWeight','bold')
  set(ann,'EdgeColor',[1 1 1])
  set(ann,'LineWidth',0);
  set(ann,'Clipping','off');
  set(ann,'LineStyle','none');