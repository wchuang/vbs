function basisFitFig

% The data structure is central to this analysis
% so all functions declare it.
global data

% This function clears some arrays in the data structure
% and also starts a new figure
initBasisFig;

% The meat of the data plotting
% You can plot whatever, including prior functions
% as well as new and old data

% The first thing plotted should be followed with a 
% hold on to make sure the figure works out OK.
feval(data.readFcn);

% Actually call the basis-set fit
[a, da] = fitBasisSet(data.a0,data.da0);

% Write the fit results as a basis set function
fName = [data.fileName, 'FitBasis'];
writeBasis(fName,a,data.CStarPrec);


% Set the axes, using AMF = 1.2 as ymax
makeAxesStandard;
%(1.4,[.1, 1e3]);


% Save the figure (with no background to help annotation programs)
if ~exist('figs','dir'), mkdir './figs'; end;
fName = ['./figs/', data.fileName, '.eps'];
saveas(gcf,fName,'epsc2');
set(gcf,'Color','w');

