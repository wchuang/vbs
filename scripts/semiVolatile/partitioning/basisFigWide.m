function basisFigWide(plotData)

if nargin < 1, plotData = 1; end;

% The data structure is central to this analysis
% so all functions declare it.
global data

% This function clears some arrays in the data structure
% and also starts a new figure
initBasisFig;

% Read in some data and plot it if that is what we want to do
if plotData == 1, feval(data.readFcn,[.6 .6 .6]); end;

% Let's recover the results of the fitting along with other 
% basis-set properties

fName = [data.fileName, 'FitBasis'];
[aFit CStarPrec CStarBasis dHBasis] = eval(fName);
data.CStarPrec = CStarPrec;

% Add in the high volatility parameters
fName = [data.fileName, 'TotalBasis'];
writeBasis(fName,aFit+data.aAdd,CStarPrec);


% Plot the curve and histograms of the basis-set yields
plotBasisFitWide(aFit,data.aAdd,data.CStarPrec);

makeAxesStandard(1.75,[1e-3, 1e9],[],0);

% Set up the figure as a wide aspect ratio
set(gcf,'Units','inches');
set(gcf,'Position',[1 1 10 6]);
set(gcf,'PaperPosition',[0 0 10 6]);
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperSize', [10 6]);

fName = ['./figs/', data.fileName, 'BasisWide.eps'];
saveas(gcf,fName,'epsc2');
set(gcf,'Color','w');
