function markRealTimeEndPoint(mkStr);

global data

if length(data.AMF) == 0, return; end;

% mark terminal point
yl = data.AMF(length(data.AMF));
xl = data.COA(length(data.AMF));

yerr = sqrt((yl * 0.10)^2 + 0.01^2);



pe = plot([xl xl],[yl-yerr yl+yerr],mkStr);
set(pe,'LineWidth',2);

pe = plot(xl,yl,'wx');
set(pe,'LineWidth',2);
set(pe,'MarkerSize',10);
