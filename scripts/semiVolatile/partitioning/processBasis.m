function alphaOut = processBasis(CStarIn,alphaProc,CStarProc)

% CStarIn is the CStar of the material being processed
% alphaProc is the processing distribution on the basis set
% CStarProc is the CStar of the precursor leading to alphaProc

CStarBasis = [.01, .1, 1, 10, 100, 1000, 1e4, 1e5, 1e6];

% Set up the output array to accept the processed material
alphaOut = zeros(size(CStarBasis));


logCStarIn = log10(CStarIn);
logCStarProc = log10(CStarProc);
logCStarBasis = log10(CStarBasis);

minLogBasis = min(logCStarBasis);
maxLogBasis = max(logCStarBasis);

% Now, we cycle through the basis set and disburse the input
for i = 1:length(logCStarBasis)
  alphaThis = alphaProc(i);
  logCStarNew = logCStarIn - logCStarProc + logCStarBasis(i);

  % Calculate the index of the new 
  idxNew = logCStarNew - minLogBasis + 1

  if idxNew < 1
    % low vapor pressure products all go in the lowest bin
    alphaOut(1) = alphaOut(1) + alphaThis;
  else
    idxLow = floor(idxNew);
    fracLow = 1 - (idxNew - idxLow);
    alphaOut(idxLow) = alphaOut(idxLow) + alphaThis * fracLow;
    alphaOut(idxLow+1) = alphaOut(idxLow+1) + alphaThis * fracLow;
  end
end