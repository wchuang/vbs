function [COA dCROG T] = readSOAppb(fname,MolWt,path,headerlines)

if nargin < 2, MolWt = 136.23; end; % g mole-1
if nargin < 3, path = './prod/'; end;
if nargin < 4, headerlines = 2; end;


[COA dROG] = textread([path fname '.dat'],'%f %f','headerlines',headerlines);

%must add back in the conversion to dROG!!!  Erased by accident


dCROG = dROG * 1e-9 * 2.5e19; % this is how many molecules cm-3
dCROG = dCROG / 6.02e23; %this is moles cm-3
dCROG = dCROG * MolWt * 1e12; %this is gm cm-3 


T = 300 + zeros(size(COA));
