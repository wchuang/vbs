function [COA dCROG COSeed AMF T] = readSOAvT(fname,fStr,headerlines,path)

if nargin < 4, path = './prod/'; end;
if nargin < 3, headerlines = 2; end;
if nargin < 2, fStr = '%f %f %f %f'; end;


[COA, dCROG, COSeed T] = textread([path fname '.dat'],fStr,'headerlines',headerlines);

% convert to T if necessary
if (mean(T) < 200), T = T + 273.15; end;

% calculate the AMF
AMF = COA ./ dCROG;
