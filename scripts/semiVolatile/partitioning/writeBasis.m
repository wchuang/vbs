function writeBasis(fileName,alphas,CStarPrec,CStarBasis,dHBasis)

if nargin < 5, dHBasis = [102, 96, 90, 84, 78, 72, 66, 60, 54]; end;
if nargin < 4, CStarBasis = [.01, .1, 1, 10, 100, 1000, 1e4, 1e5, 1e6]; end;
if nargin < 3, CStarPrec = 4.0e7; end;

outName = ['./analysis/' fileName '.m' ];
fid = fopen(outName,'w');

fprintf(fid,'function [alpha, CStarPrec, CStarBasis, dHBasis] = %s\n\n',fileName);

fprintf(fid,['dHBasis = [' sprintf('%.1e ',dHBasis) '];\n']);
fprintf(fid,['CStarBasis = [' sprintf('%.1e ',CStarBasis) '];\n']);
fprintf(fid,['CStarPrec = [' sprintf('%.2e ',CStarPrec) '];\n']);
fprintf(fid,['alpha = [' sprintf('%.3f ',alphas) '];\n']);

fclose(fid);

