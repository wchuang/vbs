function plotBasisFitWide(aFit,aAdd,CStarPrec,CStarBasis,fcnColor)

global data

% Here is the basis set
if nargin < 4, CStarBasis = [.01, .1, 1, 10, 100, 1000, 1e4, 1e5, 1e6]; end;
if nargin < 5, fcnColor = [0 .5 0]; end;

% Set up the figure as a wide aspect ratio
set(gcf,'Units','inches');
set(gcf,'Position',[1 1 10 6]);
set(gcf,'PaperPosition',[0 0 10 6]);
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperSize', [10 6]);


% Now make the range of the data equally broad --
% this is the standard range for these wide figures so they are uniform
logX = (-3:.1:9)';
XPlot = 10.^logX;

aFit = aFit + eps;

% Now combine the parameters and the additional parameters for the whole
% function
oldY = f9Basis10(logX,aFit+aAdd);

% Plot all of the parms as dark green bars 
br = stem(CStarBasis,aFit + aAdd,'g.');
set(br,'Marker','none');
set(br,'Color',[0 .5 0]);
set(br,'LineWidth',30);

% Put the plot legend information in the right place too
data.plotHandles = [data.plotHandles; br];
hn = length(data.plotHandles);
data.plotLegends(hn).str = 'Estimated parameters';


% Plot only the fit parameters in bright green
br = stem(CStarBasis,aFit + eps,'g.');
set(br,'Marker','none');
set(br,'LineWidth',30);

% Put the plot legend information in the right place too
data.plotHandles = [data.plotHandles; br];
hn = length(data.plotHandles);
data.plotLegends(hn).str = 'Fit parameters';


% This is the saturation concentration of the precursor
ap = stem(CStarPrec,1,'r.');
set(ap,'LineWidth',30);
set(ap,'Marker','none');

% Put the plot legend information in the right place too
data.plotHandles = [data.plotHandles; ap];
hn = length(data.plotHandles);
data.plotLegends(hn).str = 'Precursor volatility';


% Plot the actual function
pb = semilogx(XPlot,oldY,'k-');
set(pb,'LineWidth',3);
set(pb,'Color',fcnColor);

% Put the plot legend information in the right place too
data.plotHandles = [data.plotHandles; pb];
hn = length(data.plotHandles);
data.plotLegends(hn).str = 'Full basis-set curve';



