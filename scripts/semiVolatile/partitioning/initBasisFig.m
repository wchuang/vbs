function initBasisFig

global data

figure;

% set the plot handles array to nil (this is for legends)
data.plotHandles = [];

% initialized data array
data.all.COA = [];
data.all.AMF = [];
data.all.sigAMF = [];
data.all.T = [];

data.basisFig = gcf;
