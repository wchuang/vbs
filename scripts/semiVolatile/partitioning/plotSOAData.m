function plotSOAData(legStr,markerStr,colorStr,subColor)


global data

if isempty(data.COA), return; end;

data.ph = semilogx(data.COA,data.AMF,markerStr);
set(data.ph,'MarkerSize',12);
set(data.ph,'MarkerFaceColor',colorStr);
if subColor, set(data.ph,'MarkerFaceColor',subColor); end;

data.leg = legStr;

concatSOAData;

hold on
