function [COA AMF T] = readSOAAMF(fname,fStr,headerlines,path)

if nargin < 4, path = './prod/'; end;
if nargin < 3, headerlines = 2; end;
if nargin < 2, fStr = '%f %f'; end;


[COA AMF] = textread([path fname '.dat'],fStr,'headerlines',headerlines);

T = 300 + zeros(size(COA));
