function selectSOATRange(T,TRange)

if nargin < 1, T = 300; end;
if nargin < 2, TRange = 5; end;

global data

sel = (data.T >= (T - TRange) & data.T <= (T + TRange));

data.COA = data.COA(sel);
data.AMF = data.AMF(sel);
data.T = data.T(sel);
data.sigAMF = data.sigAMF(sel);