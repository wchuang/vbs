function plotBasisWide(alphas,CStarPrec,CStarBasis,fcnColor,xShift)

global data

% Here is the basis set
if nargin < 3, CStarBasis = [.01, .1, 1, 10, 100, 1000, 1e4, 1e5, 1e6]; end;
if nargin < 4, fcnColor = [0 .5 0]; end;
if nargin < 5, xShift = 1; end; % xShift is a factor by which to shift the basis bars

% Set up the figure as a wide aspect ratio
set(gcf,'Units','inches');
set(gcf,'Position',[1 1 10 6]);
set(gcf,'PaperPosition',[0 0 10 6]);
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperSize', [10 6]);



% Now make the range of the data equally broad --
% this is the standard range for these wide figures so they are uniform
% the range is from the CStars and then some

logX = (min(log10(CStarBasis))-1:.1:max(log10(CStarBasis))+3)';
XPlot = 10.^logX;

% However, we can sneak a new range in through a global definition!
if isfield(data,'XPlot')
    XPlot = data.XPlot; 
    logX = log10(XPlot);
end;


alphas = alphas + eps;

% Now combine the parameters and the additional parameters for the whole
% function
amf = f9Basis10(logX,alphas);

% Plot the actual function
pb = semilogx(XPlot,amf,'k-');
set(pb,'LineWidth',3);
set(pb,'Color',fcnColor);

hold on;

if exist('plotBasisWideHook') == 2, plotBasisWideHook, end;

% Plot all of the parms as dark green bars 
br = stem(CStarBasis*xShift,alphas,'g.');
set(br,'Marker','none');
set(br,'Color',fcnColor);
set(br,'LineWidth',30);

% Put the plot legend information in the right place too
data.plotHandles = [data.plotHandles; br];
hn = length(data.plotHandles);
data.plotLegends(hn).str = 'Estimated parameters';

% This is the saturation concentration of the precursor
ap = stem(CStarPrec,1,'r.');
set(ap,'LineWidth',30);
set(ap,'Marker','none');

% Put the plot legend information in the right place too
data.plotHandles = [data.plotHandles; ap];
hn = length(data.plotHandles);
data.plotLegends(hn).str = 'Precursor volatility';



% Put the plot legend information in the right place too
data.plotHandles = [data.plotHandles; pb];
hn = length(data.plotHandles);
data.plotLegends(hn).str = 'Full basis-set curve';



