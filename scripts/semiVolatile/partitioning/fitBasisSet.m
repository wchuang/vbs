function [a da] = fitBasisSet(a0,da0)

global data

% if there are no uncertainties provided they are assumed to be infinity
if nargin < 2, da0 = inf * (a0 + eps); end;

% we can pass any number of parameters we want, but the upper params will
% be set to zero to get to nine.
bMax = length(a0);
if bMax < 9
sm = 1e-5;
    for i = length(a0)+1:9
        a0(i) = 0;
        da0(i) = sm;
    end
end

% put the guess into the data structure
data.all.alphas = a0;
data.all.dalphas = da0;

a = a0;

% now pull out the data to fit (X,Y,dY)
XFit = log10(data.all.COA);
YFit = data.all.AMF;
dYFit = data.all.sigAMF;
fFit = @f9Basis10;


%Call the least squares and save the result
maxSteps = 100;
[a, da] = lstsqAnalCorr(fFit,XFit,[],YFit,dYFit,a,a0,da0,maxSteps);
data.all.a = a;
data.all.da = da;

% make a range for plotting data based on input basis set
logX = (-2:.1:bMax-3)';
XPlot = 10.^logX;

% Evaluate the function for the plot
fY = feval(fFit,logX,a);

% Plot and put stuff in the legend.
pfit = semilogx(10.^logX,fY,'k-');
set(pfit,'LineWidth',2.5);
lfit = 'basis-set fit';


data.plotHandles = [data.plotHandles; pfit];
hn = length(data.plotHandles);
data.plotLegends(hn).str = lfit;

% Calculate and plot the confidense interval
sig = 1 * sqrt(confSq(fFit,logX,a));
p = semilogx(XPlot,fY+sig,'k--');
set(p,'LineWidth',1.5);
p = semilogx(XPlot,fY-sig,'k--');
set(p,'LineWidth',1.5);
