function partCoeff = partitionAerosol(CStar, C_OM_1D_all)

% Here we need to figure out whether the whole system is above saturation
% or not -- for now I assume that it is.

% Cumulative sum from low volatility to high volatility
sumConc_all = cumsum(C_OM_1D_all);

% Guess the partitioning coefficient
xiGuess1 = sumConc_all ./ CStar - 1;
xiGuess1 = min(xiGuess1,1);
xiGuess1 = max(xiGuess1,0);
xi = xiGuess1;

% Determine the partitioning coefficient
err = 1;
maxErr = 1e-4;
while err > maxErr
  CsOA = xi .* C_OM_1D_all;
  COA = sum(CsOA);
  xiNew = 1 ./ (CStar / COA + 1);
  err = max(abs(log(xi ./ xiNew)));
  xi = xiNew;
end

partCoeff = xi;