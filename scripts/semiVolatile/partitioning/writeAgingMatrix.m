function Tr = writeAgingMatrix(alpha,firstDec)

% alpha is the transform basis set, which starts at some decade
% firstDec away from the precursor vapor pressure
% 	this is a log10, so -1 means a factor of 10 below, etc

if nargin < 2, firstDec = -1; end;

s = size(alpha);
if s(1) ~= 1, alpha = alpha'; end;

n = length(alpha);

Tr = [];

% for now this assumes that we are just going down one decade for starters.

subVec = alpha;
for col = 1:n
  % get the value off of the beginning (the lowest vapor pressure part)
  sumOff = subVec(1);
  subVec = subVec(2:n);
  subVec = [subVec 0];
  subVec(1) = subVec(1) + sumOff;
  
  Tr = [subVec' Tr];
end