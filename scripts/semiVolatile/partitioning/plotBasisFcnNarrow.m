function plotBasisFcnNarrow(alphas,CStarPrec,CStarBasis,fcnColor,xShift)

global data

% Here is the basis set
if nargin < 3, CStarBasis = [.01, .1, 1, 10, 100, 1000, 1e4, 1e5, 1e6]; end;
if nargin < 4, fcnColor = [0 .5 0]; end;
if nargin < 5, xShift = 1; end; % xShift is a factor by which to shift the basis bars

% Set up the figure as a wide aspect ratio
set(gcf,'Units','inches');
set(gcf,'Position',[1 1 6 6]);
set(gcf,'PaperPosition',[0 0 6 6]);
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperSize', [6 6]);


% Now make the range of the data equally broad --
% this is the standard range for these wide figures so they are uniform
logX = (-1:.1:3)';
XPlot = 10.^logX;

alphas = alphas + eps;

% Now combine the parameters and the additional parameters for the whole
% function
oldY = f9Basis10(logX,alphas);

% Plot the actual function
pb = semilogx(XPlot,oldY,'k-');
set(pb,'LineWidth',3);
set(pb,'Color',fcnColor);


% Put the plot legend information in the right place too
data.plotHandles = [data.plotHandles; pb];
hn = length(data.plotHandles);
data.plotLegends(hn).str = 'Full basis-set curve';


hold on;

