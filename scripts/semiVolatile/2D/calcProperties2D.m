function  calcProperties2D

global orgProp

xInt = .1;
yInt = 0.01;




% Here are the basis parameters for the two dimensions
orgProp.CStarBasis = [1e-5 1e-4 1e-3 1e-2 1e-1 1e0 1e1 1e2 1e3 1e4 1e5 1e6 1e7 1e8 1e9];
orgProp.logCStarBasis = (-5:xInt:9);
orgProp.CStarBasis = 10.^(orgProp.logCStarBasis);

orgProp.O2CBasis = [1; .9; .8; .7; .6; .5; .4; .3; .2; .1; 0];
orgProp.O2CBasis = (1:-yInt:0)';


orgProp.CStarBasis2D = ones(size(orgProp.O2CBasis)) * orgProp.CStarBasis;
orgProp.O2CBasis2D = orgProp.O2CBasis * ones(size(orgProp.CStarBasis));

% At present the dH is only a function of C*.  That could change...  This is kJ/mole.
% Note that this is now a function of the SAR.
orgProp.dHvapBasis = [120  114  108  102  96   90  84  78  72  66  60  54  48  42  36 ];

orgProp.cNums = cNumFunc2D(log10(orgProp.CStarBasis2D),orgProp.O2CBasis2D);
orgProp.oNums = orgProp.cNums .* orgProp.O2CBasis2D;
orgProp.mNums = orgProp.cNums + orgProp.oNums;

% We are going to assume that 
% Given OSc = 2 O:C - H:C, but also dH/dO = -1

orgProp.hNums = orgProp.cNums * orgProp.HCIntercept ...
    - orgProp.oNums * orgProp.HCSlope;


orgProp.molWeight = orgProp.cNums * 12 + orgProp.oNums * 16 + orgProp.hNums * 1;

% Gas-Phase Rate Constants
orgProp.kOHGas = orgProp.kPerC .* (orgProp.cNums + ...
    orgProp.kFacPerO.*orgProp.oNums + ...
    orgProp.kFacPerH.*(orgProp.O2CBasis2D.^2));