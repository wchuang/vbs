
% 2D SAR properties -- these are the basic parameters for the 2D VBS

% CStar(O:C,Cnum) = (CStarNull - Cnum) * decadesPerCarbon - decadesPerOxygen * O:C * Cnum


decadesPerCarbon = 0.475;    % mean decades per carbon added
decadesPerOxygen = 1.7;     % mean decades per oxygen added
decadesPerCarbonyl = 1.0;
decadesPerAcid = 3.5;
Cnum1mugAlkane = 25;        % carbonNumber of 1 ug/m3 alkane

% H:C = HCIntercept - HCSlope * O:C

HCSlope = 1.0;          % van Krevlen slope for H:C vs O:C
HCIntercept = 2.0;      % van Krevlen intercept for H:C vs O:C


% Rate constant properties
