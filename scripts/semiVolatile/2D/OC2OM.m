function OM2D = OC2OM(C_OC2D,argCatcher)

if nargin < 2, argCatcher = 0; end;

% There was a superseded version that passed O2C...

% We have matrices for the C, H, and O numbers in the 2D space.

OM2D = C_OC2D .* (1 + (1/12)*HNum2D./CNum2D + (16/12)*ONum2D./CNum2D);

% This can really be simpler because H:C and O:C are functions of the y value in the 2D VBS only, but the full 2D version above works.