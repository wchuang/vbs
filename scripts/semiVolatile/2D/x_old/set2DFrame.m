% Figure out where it is
set(gcf,'Units','inches');
pos = get(gcf,'Position');
set(gcf,'Position',[pos(1) pos(2) 15 6]);
set(gcf,'PaperPosition',[0 0 15 6]);
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperSize', [15 6]);

ax = gca;
set(ax,'Position',[ .05 .1 .94 .89]);
set(ax,'YAxisLocation','right');
set(ax,'FontSize',14);
set(get(ax,'Xlabel'),'String','$$\log_{10} (C^*)$$ (saturation concentration, $${\mu}g \, m^{-3}$$)','Interpreter','LaTex');
set(get(ax,'Xlabel'),'FontSize',18);
% set(get(ax,'ylabel'),'String','O:C (atomic ratio)','Interpreter','LaTex');
set(get(ax,'Ylabel'),'FontSize',18);
set(ax,'YMinorTick','off');
%set(ax,'Color','none');
set(ax,'TickLength',[0.01,0.005]);
set(ax,'LineWidth',1.5);
%set(gcf,'Color','none');

% Note that the axis limits etc are grid numbers
% We take care of the scaling on the axis labels.
% The basic grid here is 16 grid boxes going from -5 to +10 in log space
xMinValue = -5;
xMaxValue = +10;
xSpacing = 1;
xSlop = 0.25;
set(gca,'XLim',[xMinValue-xSlop,xMaxValue+xSlop]);
set(gca,'XTick',(xMinValue:xSpacing:xMaxValue));
set(gca,'XTickLabel',(xMinValue:xSpacing:xMaxValue));


%set(gca,'XTickLabel',['0.001'; '  1  '; ' 1000'; ' 10^6'; ' 10^9'; ]);
%set(ax,'XMinorTick','on');
yMinValue = 0;
yMaxValue = 1;
ySpacing = .1;
ySlop = 0.05;

set(gca,'YLim',[yMinValue-ySlop,yMaxValue+ySlop]);
set(gca,'YTick',[]);
%set(gca,'YTick',(yMinValue:ySpacing:yMaxValue));
%set(gca,'YTickLabel',(yMinValue:ySpacing:yMaxValue));



axRight = axes('Position',get(ax,'Position'),...
           'XAxisLocation','top',...
           'YAxisLocation','left',...
           'Color','none',...
           'XColor','k','YColor','k');
set(axRight,'box','on');
       
%Mark the NVOC, LVOC, SVOC, and IVOC ranges 
grayLevel = 0.9;
NVOC = rectangle('Position',[-5.25, -2.15, 1.75, 3.3],'Curvature',[0,0],...
          'FaceColor',[1 1 1]* grayLevel,'LineStyle','none');
uistack(NVOC, 'bottom');  
      
LVOC = rectangle('Position',[-3.5, -2.15, 3, 3.3],'Curvature',[0,0],...
          'FaceColor',[1 grayLevel grayLevel],'LineStyle','none');
uistack(LVOC, 'bottom');  

SVOC = rectangle('Position',[-0.5, -2.15, 3, 3.3],'Curvature',[0,0],...
          'FaceColor',[grayLevel 1 grayLevel],'LineStyle','none');
uistack(SVOC, 'bottom');  

IVOC = rectangle('Position',[2.5, -2.15, 4, 3.3],'Curvature',[0,0],...
          'FaceColor',[grayLevel grayLevel 1],'LineStyle','none');       
% set(IVOC,'layer', 'bottom');        

 set(axRight,'layer','top');
 set(ax,'layer','top');
       
set(axRight,'FontSize',14);
set(get(axRight,'ylabel'),'String','$$\rm \overline{OS}_C$$','Interpreter','LaTex');
set(get(axRight,'Ylabel'),'FontSize',18);
set(axRight,'YMinorTick','off');
%set(ax,'Color','none');
set(axRight,'TickLength',[0.01,0.005]);
set(axRight,'LineWidth',1.5);
%set(gcf,'Color','none');

% Note that the axis limits etc are grid numbers
% We take care of the scaling on the axis labels.
% The basic grid here is 16 grid boxes going from -5 to +10 in log space
set(axRight,'XLim',[xMinValue-xSlop,xMaxValue+xSlop]);
set(axRight,'XTick',(xMinValue:xMaxValue));
set(axRight,'XTickLabel','');


SARProperties2D;

set(gca,'YLim',[3*(yMinValue-ySlop)-2,3*(yMaxValue+ySlop)-2]);
set(gca,'YTick',(-2:.5:1));
set(gca,'YTickLabel',(-2:.5:1));



axes(ax);