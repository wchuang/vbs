function logCstar =  CstarFunc2D(Cnum,OtoC)

% logCstar =  CstarFunc2D(Cnum,OtoC)
%
% returns the log of C* given C# and O:C

SARProperties2D;

logCstar = (Cnum1mugAlkane - Cnum) .* decadesPerCarbon -  (decadesPerOxygen .* OtoC .* Cnum);

