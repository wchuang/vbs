function cNum =  cNumFunc2D(logCStar,OtoC)

SARProperties2D;

cNum = (Cnum1mugAlkane * decadesPerCarbon - logCStar) ./ (decadesPerCarbon + decadesPerOxygen * OtoC);
