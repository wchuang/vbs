function [s2D, CStarBasis, O2CBasis] = initOrganics2D(col,row,initval)

% initOrganics2D(col,row,initval)
%    Put initival in row,col of intial matrix
%    col [ 9] This is C* = 1000
%    row [11] Really length(O2CBasis), this is fully reduced
%    initval [100] ug/m3



CStarBasis = [1e-5 1e-4 1e-3 1e-2 1e-1 1e0 1e1 1e2 1e3 1e4 1e5 1e6 1e7 1e8 1e9];

O2CBasis = [1; .9; .8; .7; .6; .5; .4; .3; .2; .1; 0];

if nargin < 3, initval = 100; end;
if nargin < 2, row = length(O2CBasis); end;
if nargin < 1, col = 9; end;




% This is a carbon stoichiometry matrix spread very wide...
%  -5     -4     -3     -2    -1     0     1     2     3     4     5     6     7     8     9
s2D = [ ...
    0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % 1
    0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .9
    0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .8
    0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .7
    0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .6
    0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .5
    0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .4
    0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .3  
    0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .2  
    0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % .1
    0      0      0      0     0     0     0     0     0     0     0     0     0     0     0 ;  ... % 0
];

% Out of bounds calls just return a matrix of zeros

if row < 1 || row > length(O2CBasis), return; end;
if col < 1 || col > length(CStarBasis), return; end;


s2D(row,col) = initval;