function cNum =  cNumFunc2D(logCStar,OtoC)

global orgProp

cNum = (orgProp.Cnum1mugAlkane * orgProp.decadesPerCarbon - logCStar) ./ ...
    (orgProp.decadesPerCarbon + orgProp.decadesPerOxygen * OtoC + ...
    (2./(1 + 1./OtoC) * orgProp.decadesPerCONonIdeal));
