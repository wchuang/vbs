function simulateSOAExpt(fileName)

if nargin < 1, fileName = 'toluene'; end;

global data

% This is the function that will translate from  Cs array to  data array
data.readFcn = @translateSOASim;
data.fileName = fileName;
data.a0 = [.005,.005,.05,.10,.10]';
data.da0 = [.002,.003,.1,.1,.1]';


data.CStarPrec = 1e8;
data.yRange = [0 .5];
data.xRange = [0.1 100];

basisFitFig;

basisFigWide;