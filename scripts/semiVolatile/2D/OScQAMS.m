function  OSc = OtoCQAMS(percent44)

% OScQAMS(percent44)
%  Allison Aikens formula 
%  NOTE -- this will be changed

OC = 0.0794 + percent44 * 0.0382;
OSc = 3*OC - 2;
