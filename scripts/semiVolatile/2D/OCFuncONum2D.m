function OC =  OCFuncONum2D(logCStar,Onum)

% cNum =  cNumFunc2D(logCStar,OtoC)
%
% returns the carbon number for a given C* (log 10 in ug/m3) and O:C

SARProperties2D;

OC = (decadesPerCarbon * Onum) ./ (decadesPerCarbon * Cnum1mugAlkane - decadesPerOxygen * Onum - logCStar);

