function setSARProperties2D(nonIdealLog,skewFact)

global orgProp


if nargin < 1, nonIdealLog = -0.3; end;
if nargin < 2, skewFact = 2; end;   %2 trusts initial o slope
% skewFact = 2 puts most of the slope in the SAR on the high-O side
% skewFact is something strange to look at a plot -- don't use it!

% 2D SAR properties -- these are the basic parameters for the 2D VBS

% CStar(O:C,Cnum) = (CStarNull - Cnum) * decadesPerCarbon - decadesPerOxygen * Onum


orgProp.kelvinsPerDecade = 690;

orgProp.decadesPerCarbon = 0.475;    	% mean decades per carbon added
orgProp.decadesPerOxygenIdeal = 1.7;    % mean decades per oxygen added
orgProp.decadesPerCONonIdeal = nonIdealLog;	% nonideality term
%orgProp.decadesPerCONonIdeal = -.29;	% nonideality term
%orgProp.decadesPerCONonIdeal = -.44;	% nonideality term

orgProp.decadesPerOxygen = orgProp.decadesPerOxygenIdeal - skewFact * orgProp.decadesPerCONonIdeal;



orgProp.Cnum1mugAlkane = 25;        % carbonNumber of 1 ug/m3 alkane

% H:C = HCIntercept - HCSlope * O:C

orgProp.HCSlope = 1.0;          % van Krevlen slope for H:C vs O:C
orgProp.HCIntercept = 2.0;      % van Krevlen intercept for H:C vs O:C


% gas-phase kinetics
% func is k = kPerC .* (cNum + fO * oNum + fH * hNum)
orgProp.kPerC = 1.3e-12;	%kPerCarbon
orgProp.kFacPerO = 4; orgProp.kFacPerO = 9; 
orgProp.kFacPerH = -5; orgProp.kFacPerH = -10;
