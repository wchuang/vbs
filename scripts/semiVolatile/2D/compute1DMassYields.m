function [MassYield_1D, MassYield] = compute1DMassYields

CYield_2D = aPineneCYields2DNew;

[CStar300_prec, dHv_prec, OSc_prec, OtoC_prec, Cnum_prec, Hnum_prec, Onum_prec, MW_prec, molColor] = aPineneProperties;


MassYield_2D = CYield_2D .* (1 + (1/12)*HNum2D./CNum2D + (16/12)*ONum2D./CNum2D) / ...
    (1 + (1/12)*Hnum_prec/Cnum_prec + (16/12)*Onum_prec/Cnum_prec);

MassYield_1D = sum(MassYield_2D);

MassYield = sum(MassYield_1D);