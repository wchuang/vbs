function logCstar =  CstarFunc2D(Cnum,Onum)

% logCstar =  CstarFunc2D(Cnum,OtoC)
%
% returns the log of C* given C# and O:C

global orgProp

logCstar = (orgProp.Cnum1mugAlkane - Cnum) .* orgProp.decadesPerCarbon - ...
    (orgProp.decadesPerOxygen .*  Onum) - ...
    2 * (Cnum .* Onum)./(Cnum+Onum) * orgProp.decadesPerCONonIdeal;

