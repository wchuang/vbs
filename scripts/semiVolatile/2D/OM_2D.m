function [C_OM_2D, FMat] = OM_2D(C_Ox,Fs)

% make sure that the partitioning vector is a row vector
S = size(Fs);
if (S(2) == 1), Fs = Fs'; end;



% We can generate a total mass yield matrix by making use of the OC matrix
% Note that the Fs is a column, and we want 15 of them.
FMat = repmat(Fs,11,1);

C_OM_2D = C_Ox .* FMat;

