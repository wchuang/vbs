function translateSOASim(plotColor);

if nargin < 1, plotColor = 'r'; end;

global Cs data


selector = Cs.C_OM_GenArray ~= 0;

data.COA = Cs.C_OM_GenArray(selector);
data.AMF = Cs.AMF_Array(selector);
data.sigAMF = data.AMF/10;
data.aAdd = 0;



plotSOAData('simulatedData','o',plotColor,plotColor);
