function logCStar =  CStarFuncONum2D(OC,Onum)

% cNum =  cNumFunc2D(logCStar,OtoC)
%
% returns the carbon number for a given C* (log 10 in ug/m3) and O:C

SARProperties2D;

logCStar =   decadesPerCarbon * (Cnum1mugAlkane  -  Onum./OC)  - decadesPerOxygen * Onum;

