function [OSc, CStarBasis, OScBasis] = OSc2D

% OSc2D
%    Return basic properties in the 2D basis set, esp avg carbn number
%    This is mostly for reference, but also important to OM calc.
%    The basic relations here are
%   d log10 C* / dO = -1.7 (assumed average, rougly for acids)
%   H:C = 2 - O:C
%   OSc = 2O:C - H:C = 3 O:C - 2
%   O:C = (1/3) (OSc + 2);

CStarBasis = [1e-5 1e-4 1e-3 1e-2 1e-1 1e0 1e1 1e2 1e3 1e4 1e5 1e6 1e7 1e8 1e9];

OScBasis = [+1.0; +0.7; +0.4; +0.1; -0.2; -0.5; -0.8; -1.1; -1.4; -1.7; -2.0];

OSc = repmat(OScBasis,1,length(CStarBasis));
