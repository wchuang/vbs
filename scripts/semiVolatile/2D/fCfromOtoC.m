function fC = fCfromOtoC(OtoC)

fC =  1 ./ (1 + OtoC);