function plot2DAerosolHistogram(plotZMax)

global Cs figs


% Plot 2D histogram of the condensed-phase organic mass
bar3(Cs.C_OM_2D);
set(gca,'ZLim',[0,plotZMax]);

grayLevel = 0.9;
colormap([[1 1 1]* grayLevel; [1 1 1]* grayLevel;  ...
    [1 grayLevel grayLevel];  [1 grayLevel grayLevel]; [1 grayLevel grayLevel];...
    [grayLevel 1 grayLevel];  [grayLevel 1 grayLevel]; [grayLevel 1 grayLevel];...
    [grayLevel grayLevel 1];  [grayLevel grayLevel 1]; [grayLevel grayLevel 1]; [grayLevel grayLevel 1];...
    [1 1 grayLevel]; [1 1 grayLevel]; [1 1 grayLevel]]);

ax = gca;
set(ax,'FontSize',14);
set(get(ax,'Xlabel'),'String','$$\rm \log_{10} C^* \, ({\mu}g \, m^{-3})$$','Interpreter','LaTex');
set(get(ax,'Xlabel'),'FontSize',18);
set(get(ax,'ylabel'),'String','$$\rm \overline{OS}_C$$','Interpreter','LaTex');
set(get(ax,'Ylabel'),'FontSize',18);
set(get(ax,'Zlabel'),'String','$$\rm C_{OM} \, ({\mu}g \, m^{-3})$$','Interpreter','LaTex');
set(get(ax,'Zlabel'),'FontSize',18);

set(ax,'LineWidth',1.5);


% Note that the axis limits etc are grid numbers
% We take care of the scaling on the axis labels.
set(gca,'XLim',[.5,15.5]);
set(gca,'XTick',(3:3:15));
set(gca,'XTickLabel',(-3:3:9));


% Ticks for OSc (y axis)
set(gca,'YLim',[.5 11.5]);
set(gca,'YTick',(1:10/3:11));
set(gca,'YTickLabel',[1 0 -1 -2]);


