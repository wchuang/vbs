function contourResults(contourOM,contourType)

% contourResults(contourOM)
%  
% Make a contour plot of the 2-D basis set showing distributions of OM or
% total oxidized organics (both phases), depending on contourOM boolean
% param.


if nargin < 1, contourOM = 1; end;
if nargin < 2, contourType = 'g-'; end;

global Cs figs
   
% Set the figure where this is going
if figs.fig2D
    figure(figs.fig2D);
else
    figs.fig2D = figure;
    set2DFrameTest;
end

axes(figs.axGrid);

% Use flipud to flip the matrix for viewing (O:C incr upwards in view)
if contourOM
    
    maxZ = max(max(Cs.C_OM_2D));
    contour(flipud(Cs.C_OM_2D),[maxZ/sqrt(2); maxZ/8;],contourType,'LineWidth',2);

    avgKern = Cs.C_OM_1D .* log10(Cs.C_Star);
    avglogCStar = sum(avgKern) / sum(log10(Cs.C_Star));
    avgOC = Cs.OM_OC - 1;
    
    %pl = plot(avglogCStar+6,avgOC*10 + 1,'kp');
    %set(pl,'MarkerSize',20);
    
else
    maxZ = max(max(Cs.C_Ox_2D));
    %contour(flipud(Cs.C_Ox_2D),[maxZ/sqrt(2); maxZ/8;],'b-','LineWidth',2);
    contour(flipud(Cs.C_Ox_2D),'b-','LineWidth',1);
end




  