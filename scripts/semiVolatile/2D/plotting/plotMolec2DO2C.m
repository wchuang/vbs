function plotMolec2DO2C(molName)

if iscellstr(molName), molName = char(molName); end;

[CStar300, dHv, OSc, OtoC, Cnum, Hnum, Onum, MW, molColor] = eval([molName 'Properties']);


pl = plot(log10(CStar300),OtoC,'ko');
set(pl,'MarkerSize',20);
set(pl,'MarkerFaceColor',molColor);
