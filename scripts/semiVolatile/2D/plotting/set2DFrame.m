function set2DFrame(tickNumSize,yTicks,yStyle)

if nargin < 1, tickNumSize = 14; end;
if nargin < 2, yTicks = 1; end;
if nargin < 3
	yStyle = 1; 
	OCTicks = -1;
	OSCTicks = 1;
else
	OCTicks = 1;
	OSCTicks = 0;
end;

% The 2D VBS currently has 4 overlapping sets of axes.
% 
% axOC   O:C vs log 10 C*
% axOSc  OSc vs log 10 C*, where OSc is the mean oxidation state of C
% axOScBkg this is for background stuff like the IVOC rectangle, etc.
% axGrid a gridded space with the 2D array



% Make sure there is a place for figures
if ~exist('./figs','dir'); mkdir('./figs'); end;


% The information about the frame and ranges is kept in a structure figs.

global figs
initFigStructure;



figs.fig2D = figure;
figs.tickNumSize = tickNumSize;
figs.yTicks = yTicks;

% Figure out where it is
set(gcf,'Units','inches');
pos = get(gcf,'Position');
set(gcf,'Position',[[pos(1) pos(2)]  figs.paperSizeTop]);
set(gcf,'PaperPosition',[[0 0] figs.paperSizeTop]);
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperSize', figs.paperSizeTop);
%set(gcf,'Color','none');


% Note -- the "new axes" label lets either OC or OSc axes 
% know that they are build on already declared, so 0 means
% no need to make a new one.
set2DFrameOC(0,OCTicks);
set2DFrameOScBkg;
set2DFrameOSc(1,OSCTicks);
set2DFrameGrid;
set2DFrameFineGrid;


% We are going to start with the OSc axis in front, but we will need to
% move around a lot...
axes(figs.axOSc);