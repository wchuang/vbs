function plotMolecProp2DOSc(CStar300,OSc,molColor)


pl = plot(log10(CStar300),OSc,'ko');
set(pl,'MarkerSize',20);
set(pl,'MarkerFaceColor',molColor);
