function set2DFrameFineGrid(newAxes)

global figs

% Lay down an axis for the gridded space

if nargin < 1, newAxes = 1; end;

if newAxes
    ax = axes('Position',figs.posRectTop,'box','on',...
           'XAxisLocation','bottom','YAxisLocation','left',...
           'Color','none','XColor','k','YColor','k');
else
    ax = gca;
end

       
figs.axFineGrid = ax;
hold on;
       

set(ax,'layer','top');
       
xGridMin = 1;
xGridRange = 140;
xGridMax = xGridMin+xGridRange;

xSlop = 0.5/14 * xGridRange;
set(ax,'XLim',[xGridMin-xSlop,xGridMax+xSlop]);
set(ax,'XTick',[]);
set(ax,'XTickLabel','');


yGridMin = 1;
yGridRange = 100;
yGridMax = yGridMin+yGridRange; 
ySlop = 0.5/10 * yGridRange;
set(ax,'YLim',[yGridMin-ySlop,yGridMax+ySlop]);
set(ax,'YTick',[]);
set(ax,'YTickLabel','');
