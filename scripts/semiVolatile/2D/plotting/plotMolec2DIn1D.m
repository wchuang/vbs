function plotMolec2DIn1D(molName)

global Cs

if iscellstr(molName), molName = char(molName); end;

[C_Star300, dHv, OtoC, Cnum, MW, molColor] = eval([molName 'Properties']);

% Always run Clausius Clapeyron
% Note that the Temperature is hiding in the Cs structure
R = 8.314472/1000;
C_Star = C_Star300 .* (Cs.T / 300) .* ...
   exp(- (dHv / (R * Cs.T) - dHv / (R * 300) ));


pl = plot(log10(C_Star),0,'ko');
set(pl,'MarkerSize',12);
set(pl,'MarkerFaceColor',molColor);
