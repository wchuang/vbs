function plotMolec2D(molName)

if iscellstr(molName), molName = char(molName); end;

[CStar300, dHv, OtoC, Cnum, MW, molColor] = eval([molName 'Properties']);


pl = plot(log10(CStar300)+6,OtoC*10+1,'ko');
set(pl,'MarkerSize',20);
set(pl,'MarkerFaceColor',molColor);
