function show1DBasis(CStar,Ctot,Cparticle)

% show1DBasis(CStar,Ctot,Cparticle)
%
%  CStar     : Array of saturation concentrations
%  Ctot      : Total mass concentration in each bin (all phases)
%  Cparticle : Condensed-phase mass concentration in each bin

C_OM = sum(Cparticle);

br = bar(log10(C_Star)',[(Cparticle)',(Cs.C_Ox_1D - Cparticle)'],'Stack');
  
set(br(1),'FaceColor','green');
set(br(1),'LineWidth',2);
set(br(2),'FaceColor','white');
set(br(2),'LineWidth',2);
ax = gca;
set(get(ax,'Xlabel'),'String','C^* ( {\mu}g m^{-3})');
%  set(gca,'XTickLabel',['0.01';'0.1 ';'  1 ';' 10 ';'100 ';'1000';'10^4';'10^5';'10^6']);
set(get(ax,'Xlabel'),'FontSize',18);
set(ax,'FontSize',14);
set(get(ax,'ylabel'),'String','Organic Mass Conc ({\mu}g m^{-3})');
set(get(ax,'Ylabel'),'FontSize',18);
set(ax,'LineWidth',1.5);

minLogX = -6;
rangeX = 16;

xlim([minLogX,minLogX+rangeX]);
ylim([0,plotZMax*10]);
  
if C_OM <= 0; return; end;

% figure out where to put the arrow
% the range of the x axis (log) is 10
Xa = (log10(C_OM)-minLogX)/rangeX;

% Now, we need to get this into the axes, left = pos(1), width = pos(3);
axPos = get(gca,'Position');
axLeft = axPos(1);
axWidth = axPos(3);
  
% OK, if the arrow is at 0, it is really at axLeft 
% and the width of the field is really (axWidth)
Xa = (Xa * axWidth) + axLeft;
X = [Xa, Xa]; Y = [.9/2, .7/2];

ann = annotation('arrow',X,Y);
Cs.fig2D.arrow = ann;
set(ann,'LineWidth',5);
set(ann,'Color','green'); 
set(ann,'HeadWidth',20); 
set(ann,'HeadLength',30); 

ann = annotation('textbox',[Xa+.01,.8/2,.25,.05]);
Cs.fig2D.text = ann;
set(ann,'String',sprintf('C_{OA} = %.1f {\\mu}g m^{-3}',C_OM));
set(ann,'FontSize',16)
set(ann,'FontWeight','bold')
set(ann,'EdgeColor','none')  
