function set2DFrameOScBkg(newAxes)

global figs

if nargin < 1, newAxes = 1; end;


if newAxes
    ax = axes('Position',figs.posRectTop,'box','on',...
           'XAxisLocation','bottom','YAxisLocation','left',...
           'Color','none','XColor','k','YColor','k');
else
    ax = gca;
end

       
figs.axOScBkg = ax;
hold on;
       
xMinValue = -5;
xRange = 14;
xMaxValue = xMinValue+xRange;
xSpacing = 1;

xSlop = 0.5/14 * xRange;

set(gca,'XLim',[xMinValue-xSlop,xMaxValue+xSlop]);

set(ax,'XLim',[xMinValue-xSlop,xMaxValue+xSlop]);
set(ax,'XTick',[]);
set(ax,'XTickLabel','');


% Range in O:C space
yMinValue = -2;
yRange = 3;
yMaxValue = yMinValue+yRange;
ySpacing = .5;

ySlop = 0.5/10 * yRange;


set(ax,'YLim',[yMinValue-ySlop,yMaxValue+ySlop]);
set(ax,'YTick',[]);
set(ax,'YTickLabel','');

