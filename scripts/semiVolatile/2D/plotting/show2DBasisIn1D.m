function show2DBasisIn1D(yMax)

if nargin < 1, yMax = 50; end;

global Cs figs


% Plot the bar graph using the 1D C arrays
br = bar(log10(Cs.C_Star)',[(Cs.C_OM_1D)',(Cs.C_Ox_1D - Cs.C_OM_1D)'],'Stack');
  
set(br(1),'FaceColor','green');
set(br(1),'LineWidth',2);
set(br(2),'FaceColor','white');
set(br(2),'LineWidth',2);

ax = gca;
set(ax,'FontSize',14);
set(get(ax,'Xlabel'),'String','$$\log_{10} \rm (C^*)$$ (saturation concentration, $${\mu}g \, m^{-3}$$)','Interpreter','LaTex');
set(get(ax,'Xlabel'),'FontSize',18);
set(ax,'XTick',(-5:9));


set(get(ax,'ylabel'),'String','Organic Mass Conc $$\rm ({\mu}g \, m^{-3})$$','Interpreter','LaTex');
set(get(ax,'Ylabel'),'FontSize',18);
set(ax,'LineWidth',1.5);


set(ax,'Color','none');

minLogX = -5.5;
rangeX = 15;

xlim([minLogX,minLogX+rangeX]);
ylim([0,yMax]);
  
if Cs.C_OM <= 0; return; end;

% figure out where to put the arrow
% the range of the x axis (log) is 10
Xa = (log10(Cs.C_OM)-minLogX)/rangeX;

% Now, we need to get this into the axes, left = pos(1), width = pos(3);
axPos = get(gca,'Position');
axLeft = axPos(1);
axWidth = axPos(3);
axHeight = axPos(4);
axBottom = axPos(2);
  
% OK, if the arrow is at 0, it is really at axLeft 
% and the width of the field is really (axWidth)
Xa = (Xa * axWidth) + axLeft;
X = [Xa, Xa]; Y = [axBottom + .9*axHeight, axBottom + .7*axHeight];

ann = annotation('textbox',[axLeft+.01,axBottom + .865*axHeight,.25,.05]);
figs.fig1DVBS.title = ann;
set(ann,'String',...
    sprintf('$$\\rm %d K, %.1f \\mu g$$ $$\\rm m^{-3}$$ $$\\alpha$$-pinene',Cs.T,Cs.C_prec),'Interpreter','LaTex');
set(ann,'FontSize',16)
set(ann,'FontWeight','bold')
set(ann,'EdgeColor','none') 


ann = annotation('arrow',X,Y);
figs.fig1DVBS.arrow = ann;
set(ann,'LineWidth',5);
set(ann,'Color','green'); 
set(ann,'HeadWidth',20); 
set(ann,'HeadLength',30); 

ann = annotation('textbox',[Xa+.01,axBottom + .865*axHeight,.015,.05]);
figs.fig1DVBS.text = ann;
set(ann,'String',sprintf('$$\\rm C_{OA} = %.1f {\\mu}g$$  $$\\rm m^{-3}$$',Cs.C_OM),'Interpreter','LaTex');
set(ann,'FontSize',16)
set(ann,'FontWeight','bold')
set(ann,'EdgeColor','none')  




