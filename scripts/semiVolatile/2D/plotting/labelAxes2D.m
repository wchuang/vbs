set(gca,'FontSize',figs.tickNumSize);
set(gca,'TickLength',[0.01,0.005]);
set(gca,'LineWidth',1.5);

% There are some situations where we want a blank x axis
set(gca,'XTick',(xMinValue:xSpacing:xMaxValue));

if figs.xLabel
  set(get(gca,'Xlabel'),'String',figs.xString,'Interpreter','LaTex');
  set(get(gca,'Xlabel'),'FontSize',18);
  set(gca,'XTickLabel',(xMinValue:xSpacing:xMaxValue));
else
  set(get(gca,'Xlabel'),'String','','Interpreter','LaTex');
  set(gca,'XTickLabel',[]);
end


% There are some situations where we want a blank y axis
if figs.yTicks
    set(gca,'YAxisLocation',yAxisLocation);
    set(get(gca,'ylabel'),'String',yString,'Interpreter','LaTex');
    set(get(gca,'Ylabel'),'FontSize',18);
    set(gca,'YMinorTick','off');
    set(gca,'YTick',(yMinValue:ySpacing:yMaxValue));
    set(gca,'YTickLabel',yTickLabelArray);
end
