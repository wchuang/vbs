function set2DFrameOC(newAxes,labelAxes)

global figs

if nargin < 1, newAxes = 1; end;
if nargin < 2, labelAxes = 0; end;

if newAxes
    ax = axes('Position',figs.posRectTop,'box','on',...
           'XAxisLocation','bottom','YAxisLocation','left',...
           'Color','none','XColor','k','YColor','k');
else
    ax = gca;
    set(gca,'Position',figs.posRectTop,'box','on',...
           'XAxisLocation','bottom','YAxisLocation','left',...
           'Color','none','XColor','k','YColor','k');
end

figs.axOC = ax;
hold on;

set(ax,'Position',figs.posRectTop);
%set(ax,'YAxisLocation','left');


% x Range in log C* space

xMinValue = -5;
xRange = 14;
xMaxValue = xMinValue+xRange;
xSpacing = 1;
xSlop = 0.5/14 * xRange;

set(gca,'XLim',[xMinValue-xSlop,xMaxValue+xSlop]);

% y Range in O:C space
yMinValue = 0;
yRange = 1;
yMaxValue = yMinValue+yRange;
ySpacing = 1/3;
ySlop = 0.5/10 * yRange;

set(gca,'YLim',[yMinValue-ySlop,yMaxValue+ySlop]);


set(gca,'XTick',[]);
set(gca,'YTick',[]);

if labelAxes 
    yString = 'O:C';
    yTickLabelArray = ['  0';'1:3';'2:3';'1:1'];
end

yAxisLocation = 'left';
if labelAxes == -1
    yAxisLocation = 'right';
    yString = 'approximate O:C';
end

if labelAxes, labelAxes2D; end;


set(ax,'layer','top');
set(ax,'Color','none');
