function initFigStructure

global figs

figs.fig2D = 0;                         % Contour plot figure
figs.figHist.handle = 0;                % Histogram figure
figs.hist1D.axes = 0;

figs.paperSizeTop = [15.5 6.25];
%figs.posRectTop = [ .055 .11 .94 .825];
figs.posRectTop = [ .055 .11 .90 .825];

figs.paperSize = [15.5 6.0];
%figs.posRect = [ .055 .10 .94 .89];
figs.posRect = [ .055 .10 .90 .89];

if ~isfield(figs,'xString')
  figs.xString = '$$\log_{10} \rm (C^o)$$ (saturation concentration, $${\mu}g \, m^{-3}$$)';
end

if ~isfield(figs,'xLabel')
  figs.xLabel = 1;
end
