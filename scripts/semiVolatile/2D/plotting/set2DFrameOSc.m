function set2DFrameOSc(newAxes,labelAxes)

global figs

if nargin < 1, newAxes = 1; end;
if nargin < 2, labelAxes = 1; end;

if newAxes
    ax = axes('Position',figs.posRectTop,'box','on',...
           'XAxisLocation','bottom','YAxisLocation','left',...
           'Color','none','XColor','k','YColor','k');
else
    ax = gca;
    set(gca,'Position',figs.posRectTop,'box','on',...
           'XAxisLocation','bottom','YAxisLocation','left',...
           'Color','none','XColor','k','YColor','k');
end


figs.axOSc = ax;
hold on;
       

set(ax,'layer','top');


xMinValue = -5;
xRange = 14;
xMaxValue = xMinValue+xRange;
xSpacing = 1;

xSlop = 0.5/14 * xRange;

set(ax,'XLim',[xMinValue-xSlop,xMaxValue+xSlop]);
set(ax,'XTick',[]);
set(ax,'XTickLabel','');


% Range in O:C space
yMinValue = -2;
yRange = 3;
yMaxValue = yMinValue+yRange;
ySpacing = .5;

ySlop = 0.5/10 * yRange;

set(ax,'YLim',[yMinValue-ySlop,yMaxValue+ySlop]);
set(ax,'YTick',[]);
set(ax,'YTickLabel','');

if labelAxes == 1
    yAxisLocation = 'left';
    yString = '$$\rm \overline{OS}_C$$';
    yTickLabelArray = (yMinValue:ySpacing:yMaxValue);
    labelAxes2D;
end


end