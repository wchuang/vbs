function contourComposition

global figs orgProp

axes(figs.axFineGrid)

% Not showing h num for now, but it is in the set
%cRange = (2:2:80);
%[C,h] = contour(flipud(orgProp.hNums),cRange,'b:','LineWidth',1.5);

%clabel(C,h,[2:2:6],'Rotation',0,'FontSize',14,'LabelSpacing',200,...
%    'BackgroundColor','w','EdgeColor','b');

cRange = (2:2:8);
[C,h] = contour(flipud(orgProp.oNums),cRange,'g','LineWidth',1.5);

clabel(C,h,cRange,'Rotation',0,'FontSize',14,'LabelSpacing',300,...
    'BackgroundColor','w','EdgeColor','g');


%cRange = (2:2:40);
%[C,h] = contour(flipud(orgProp.mNums),cRange,'c','LineWidth',1.5);

%clabel(C,h,cRange,'Rotation',0,'FontSize',14,'LabelSpacing',100,...
%    'BackgroundColor','w','EdgeColor','k');


cRange = (2:2:40);
[C,h] = contour(flipud(orgProp.cNums),cRange,'k','LineWidth',1.5);

clabel(C,h,cRange,'Rotation',0,'FontSize',14,'LabelSpacing',200,...
    'BackgroundColor','w','EdgeColor','k');




axes(figs.axOC);
