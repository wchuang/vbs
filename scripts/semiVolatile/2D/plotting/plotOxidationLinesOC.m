function plotOxidationLinesOC(molName,lineSpec,lineWidth,nLines)

if iscellstr(molName), molName = char(molName); end;

if nargin < 2, lineSpec = 'r-'; end;
if nargin < 3, lineWidth = 2; end;
if nargin < 4, nLines = 3; end;

[CStar300, dHv, OSc, OtoC, Cnum, Hnum, Onum, MW, molColor] = eval([molName 'Properties']);

% The general formula is 
% d O:C / d log C*  = 1 / (nC * (bk + 0.04))
% this is derived by differentiating the C* from pVap SAR rules)


bk_OH = -2.3;
bk_acid = -3.5;
bk_carb = -1;

dO = 1;
dH = 0;
cStarLine(CStar300,OtoC,bk_OH,Cnum,dH,dO,lineSpec,lineWidth);

if nLines < 2, return; end;

dO = 1;
dH = -2;
cStarLine(CStar300,OtoC,bk_carb,Cnum,dH,dO,lineSpec,lineWidth);

if nLines < 3, return; end;

dO = 2;
dH = -2;
cStarLine(CStar300,OtoC,bk_acid,Cnum,dH,dO,lineSpec,lineWidth);


function cStarLine(CStar300, OtoC, bk, C_num, dH, dO, lineSpec, lineWidth)
% x is a decade number, with offset from rastering.


dOC =  dO / C_num;
dlogCstar = bk;

slope = dOC / dlogCstar;

x1 = log10(CStar300);
y1 = OtoC;

dx = -8;
dy = slope * dx;
x2 = x1 + dx;
y2 = y1 + dy;


pl = plot([x1, x2],[y1, y2],lineSpec);
set(pl,'Linewidth',lineWidth);

