function plotAMSSecondaryFactors(just)

if nargin < 1, just = 'center'; end;

global figs

axes(figs.axOC);
rectangle('Position',[-6, .67, 6.5, .33],'Curvature',.75,...
          'FaceColor','none','EdgeColor',[0 .5 0],...
          'LineWidth',3,'LineStyle','--');
text(-3,.825,'LV-OOA','HorizontalAlignment',just,'FontSize',25);

rectangle('Position',[-2.5, .33, 4, .33],'Curvature',.75,...
          'EdgeColor',[.5 1 .5],'LineWidth',3,'LineStyle','--');
text(-.5,.5,'SV-OOA','HorizontalAlignment',just,'FontSize',25);


