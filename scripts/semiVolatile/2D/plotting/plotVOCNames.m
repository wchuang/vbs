function plotVOCNames(nameY)

global figs

if nargin < 1, nameY = 0.975; end;

axes(figs.axOC);

text(-4.5,nameY,'ELVOC','HorizontalAlignment','center','FontSize',25);
text(-2,nameY,'LVOC','HorizontalAlignment','center','FontSize',25);
text(1,nameY,'SVOC','HorizontalAlignment','center','FontSize',25);
text(4.5,nameY,'IVOC','HorizontalAlignment','center','FontSize',25);
text(8.0,nameY,'VOC','HorizontalAlignment','center','FontSize',25);
