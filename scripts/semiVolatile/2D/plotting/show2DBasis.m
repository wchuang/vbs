function show2DBasis(plotZMax,titleString,fileName,writePlots)

% This function makes bar graphs of the 2D basis set organic mass.  It shows
% both the total mass and the condensed phase mass in separate graphs

% The first figure is of the condensed phase organic mass (C_OM)

if nargin < 1, plotZMax = .1; end;
if nargin < 2, titleString = ''; end;
if nargin < 3, fileName = ''; end;
if nargin < 4, writePlots = 0; end;

global Cs figs

if figs.figHist.handle == 0
    startHistFig;
else
    try
        figure(figs.figHist.handle);
    catch
        startHistFig;
    end
end



set(gcf,'Color','w');

ann = figs.figHist.title;
set(ann,'String',titleString,'Interpreter','LaTex');
set(ann,'FontSize',24);
set(ann,'FontWeight','bold');
set(ann,'EdgeColor','none');
set(ann,'HorizontalAlignment','center');



% Set up the figure as a wide aspect ratio, with double height for joined
% histograms
set(gcf,'Units','inches');
% Figure out where it is
pos = get(gcf,'Position');
set(gcf,'Position',[pos(1) pos(2) figs.paperSize .* [1 2]]);
set(gcf,'PaperPosition',[0 0 figs.paperSize .* [1 2]]);
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperSize', figs.paperSize .* [1 2]);

% Put an OM 3-D bar plot in the upper left
subplot('Position',[.075 .575 .4 .4]);
hold off;

plot2DAerosolHistogram(plotZMax);


% Now plot the total organic material (all phases) in upper right   
subplot('Position',[.575 .575 .4 .4]);
hold off;

plot2DTotalOrganicHistogram(plotZMax*10);

%thisFrame = getframe(gca);
%Cs.C_Ox_Movie = [Cs.C_Ox_Movie thisFrame];


subplot('Position',figs.posRect .* [1 .5 1 .5]);

% Erase it
hold off;
yMax1D = plotZMax*20;

show2DBasisIn1D(yMax1D);

if (~strcmp(fileName,'')) && writePlots
  set(gcf,'Color','none');
  saveas(gcf,fileName,'pdf');
end
set(gcf,'Color','w');
pause;  



function startHistFig

global figs

figs.figHist.handle = figure;
figs.figHist.title = annotation('textbox',[.25,.95,.5,.05]);

