function drawXVOCRectangles

global figs

%Mark the NVOC, LVOC, SVOC, and IVOC ranges 

% the rectangles are devined in OSc space

NVOCmin = -5.5;
NVOCwidth = 2.0;
LVOCmin = -3.5;
LVOCwidth = 3.0;
SVOCmin = -0.5;
SVOCwidth = 3.0;
IVOCmin = 2.5;
IVOCwidth = 4.0;

OScmin = -2.15;
OScwidth = 3.3;


axes(figs.axOScBkg);

grayLevel = 0.9;
NVOC = rectangle('Position',[NVOCmin, OScmin, NVOCwidth, OScwidth],'Curvature',[0,0],...
          'FaceColor',[1 1 1]* grayLevel,'LineStyle','none');
uistack(NVOC, 'bottom');  
      
LVOC = rectangle('Position',[LVOCmin, OScmin, LVOCwidth, OScwidth],'Curvature',[0,0],...
          'FaceColor',[1 grayLevel grayLevel],'LineStyle','none');
uistack(LVOC, 'bottom');  

SVOC = rectangle('Position',[SVOCmin, OScmin, SVOCwidth, OScwidth],'Curvature',[0,0],...
          'FaceColor',[grayLevel 1 grayLevel],'LineStyle','none');
uistack(SVOC, 'bottom');  

IVOC = rectangle('Position',[IVOCmin, OScmin, IVOCwidth, OScwidth],'Curvature',[0,0],...
          'FaceColor',[grayLevel grayLevel 1],'LineStyle','none');       
uistack(IVOC, 'bottom');         
