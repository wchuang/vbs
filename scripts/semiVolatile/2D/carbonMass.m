function massCarbon = carbonMass(molName,massConc)

% carbonMass(molName,massConc)
%
%   molName  string with name
%   massConc [1] ug/m3, unit default allows conversion outside.
%
% Return the carbon mass of a compound given its total mass and identity.
% Right now restricted to CHO compounds
% 

if nargin < 2, massConc = 1; end;

if iscellstr(molName), molName = char(molName); end;

[CStar300, dHv, OSc, OtoC, Cnum, Hnum, Onum, MW, molColor] = eval([molName 'Properties']);


massCarbon = massConc / ( 1 + (1/12) * Hnum/Cnum + (16/12) * Onum/Cnum );