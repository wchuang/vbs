function OC =  OCFunc2D(logCStar,Cnum)

% cNum =  cNumFunc2D(logCStar,OtoC)
%
% returns the carbon number for a given C* (log 10 in ug/m3) and O:C

SARProperties2D;

OC = ((Cnum1mugAlkane - Cnum) * decadesPerCarbon - logCStar) ./ (decadesPerOxygen * Cnum);

