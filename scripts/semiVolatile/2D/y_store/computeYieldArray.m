function computeYieldArray

global Cs


% Collect some values for a plot
% This function is called periodically, and when it is values from the Cs
% structure are saved to a growing array for later plotting.

Cs.tau_Array = [Cs.tau_Array Cs.tau];
Cs.C_OM_GenArray = [Cs.C_OM_GenArray Cs.C_OM];
Cs.OM_OC_GenArray = [Cs.OM_OC_GenArray Cs.OM_OC];
Cs.OSc_GenArray = [Cs.OSc_GenArray Cs.OSc];

Cs.C_prec_Array = [Cs.C_prec_Array Cs.C_prec];
Cs.DC_prec_Array = [Cs.DC_prec_Array Cs.DC_prec];
Cs.AMF_Array = [Cs.AMF_Array Cs.AMF];
