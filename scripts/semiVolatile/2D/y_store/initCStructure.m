function initCStructure(zapFigs)

if nargin < 1, zapFigs = 1; end;

% initCStructure
%   Declare the bacis substructure things and null them out

global Cs

% concentration of the precursor, if there is one, for SOA
Cs.C_prec = 0;
Cs.DC_prec = 0;
Cs.tau = 0;

% Array of organic mass
Cs.C_OM_Array = [];
Cs.OM_OC_Array = [];
Cs.OC_Array = [];
Cs.OSc_Array = [];

Cs.C_OM_GenArray = [];
Cs.OM_OC_GenArray = [];
Cs.OSc_GenArray = [];

Cs.tau_Array = [];
Cs.C_prec_Array = [];
Cs.DC_prec_Array = [];
Cs.AMF_Array = [];

Cs.C_OM_Movie = [];
Cs.C_Ox_Movie = [];



Cs.fig2D.arrow = 0;
Cs.fig2D.text = 0;
Cs.fig2D.title = 0;

if zapFigs
    Cs.fig2D.fig = 0;
    Cs.figContour = 0; 
end;