function computeOrganicProperties

% computeOrganicProperties
%    Figure out partitioning and OM levels in 1 and 2 D based on the 
%    tracking property, which is 2D OC levels.

global Cs

% Always run Clausius Clapeyron
R = 8.314472/1000;
Cs.C_Star = Cs.C_Star300 .* (Cs.T / 300) .* ...
    exp(- (Cs.dHv / (R * Cs.T) - Cs.dHv / (R * 300) ));

% The base array is the total carbon mass concentration
% while sub arrays deal with fractions in various reservoirs.

% Here are the carbon mass concentrations in 2, 1, and 0 dimensions.
% 2D is the base, and the arrays are collapsed to 1 and 0 (total).
Cs.C_C_1D = sum(Cs.C_C_2D);
Cs.C_C_C = sum(Cs.C_C_1D);
    
% Here are the organic mass concentrations in 2, 1, and 0 d
% Note that C_Ox should probably be C_org
% But also note that this is in any phase
Cs.C_Ox_2D = OC2OM(Cs.C_C_2D);
Cs.C_Ox_1D = sum(Cs.C_Ox_2D);
Cs.C_Ox = sum(Cs.C_Ox_1D);
    
% Now find out what is in the condensed phase
Fs = partitionAerosol(Cs.C_Ox_1D,Cs.C_Star);
C.Fs_1D = Fs;

% C_OM is the organic material in the particle phase
Cs.C_OM_1D = Fs .* Cs.C_Ox_1D;
Cs.C_OC_1D = Fs .* Cs.C_C_1D;

[Cs.C_OM_2D, Cs.Fs_2D] = OM_2D(Cs.C_Ox_2D,Fs);
    
% Find the total organic mass and carbon mass.
Cs.C_OM = sum(Cs.C_OM_1D);
Cs.C_OC = sum(Cs.C_OC_1D);
    
Cs.OM_OC = Cs.C_OM / Cs.C_OC;

% Fid the mass weighted oxidation state in the condensed phase
OSc2DWeight = Cs.C_OM_2D .* OSc2D;
Cs.OSc = sum(sum(OSc2DWeight)) / Cs.C_OM;


Cs.AMF = Cs.C_OM / Cs.DC_prec;
