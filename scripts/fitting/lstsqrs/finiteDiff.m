function f=finiteDiff(fcn,X,a,n,p,s)

% finiteDiff(fcn,X,a,n,p,s)
% 
% finite difference of fitting function fcn
% in either  X or a space
% note that X *must* be a column vector

if size(a,1) == 1, a = a'; end;
if nargin < 4, n = 0; end
if nargin < 5, p = 0; end
if nargin < 6, s = 1; end

f0 = feval(fcn,X,a);

if n > 0
   sz = size(X);
   dX = s * ones(sz(1),1);
   X = X + dX;
end
if p > 0
   a(p) = a(p) + s;
end

f1 = feval(fcn,X,a);

f = (f1 - f0)/s;

