function [A, Unc, sVar, Corr] = lstsqAnalCorr(fcn,X,dX,Y,dY,a,a0,da0,imax)

% lstsqAnalCorr(fcn,X,dX,Y,dY,a,a0,da0) Least Squares Analysis with correlation
%
%
global n p m F Fa L C V S ARec SRec Corr

% Do a little conditioning
if nargin < 7, a0 = a; end;
if nargin < 8, da0 = inf * a0 ; end;
if nargin < 9, imax = inf; end;


[A, Unc, sVar, Corr] = lstsqAnal(fcn,X,dX,Y,dY,a,a0,da0,imax);

[U Lam] = eig(Corr);

fprintf('\nCorrelation Coefficients:\n\n');
disp(Corr)

fprintf('Corelation Eigenvectors and Eigenvalues:\n\n');
disp(U)
disp(diag(Lam)');

% if there was an interp fcn things can get long, so put a demarcation in
if exist('interpName') == 2, fprintf('---------------------------------------------------------\n\n'); end;
