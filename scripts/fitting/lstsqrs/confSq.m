function sig2 = confSq(fcn,X,a,varBool)

% confSq
%
% Return confidence interval after fitting converged
% assumes global variables hold currently valid values
% For Cinv to be properly 

global n p m F Fa L C V S ARec SRec Cinv

% varBool is a boolean to use the variance instead of S/n-p
if nargin < 4, varBool = 0; end;

if size(a,1) == 1, a = a'; end;
nX = size(X,1);
p = size(a,1);

% calculate the derivatives at the provided X values
% note that nX is distinct from n, as n was the number of data points
% and nX is the number of points where we are calculating conf
Fa = zeros(nX,p);
for k = 1:p
    Fa(:,k) = - feval(fcn,X,a,0,k);
end


sig2 = zeros(nX,1);
for j = 1:p
    for k = 1:p
        sig2 = sig2 + Fa(:,j) .* Fa(:,k) .* Cinv(j,k);
    end
end

% Adjust for chi^2 if n-p > 0 and if it is requested
if ~varBool & (n-p > 0), sig2 = S/(n-p) * abs(sig2); end;
