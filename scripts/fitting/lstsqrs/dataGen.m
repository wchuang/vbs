a0 = [5,3];
X = squeeze((10:.1:11)');
Y = ones(size(X,1),2);
Y(:,1) = a0(1) + a0(2) * X + randn(size(X));
