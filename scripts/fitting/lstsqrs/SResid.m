function S = SResid(fcn,X,dX,Y,dY,a,a0,da)
% SResid(X,Y,a) Squared Residual Function
% SResid returns the summed squared residuals
% The arrays X,Y,and a may all contain uncertainty
%    
F = Y-feval(fcn,X,a);
L = dY.^2;
Sarray = F.^2 ./ L;
S = sum(Sarray) + sum(((a-a0)./da).^2);
