function da = lstsqInv(fcn,X,dX,Y,dY,a,a0,da0)
% lstsqrs(X,Y,a) Least Squares Inversion
% 
% This is a straightforward invocation of the standard
%  leastsquares inversion
%  dA = C' V
% The parameter update vector dA should be regarded as
% conditional.
%

global F Fa L C V

% Do a little conditioning
if nargin < 7, a0 = a; end;
if nargin < 8, da0 = a0 / 0; end;

if size(a,1) == 1, a = a'; end;
if size(a0,1) == 1, a0 = a0'; end;
if size(da0,1) == 1, da0 = da0'; end;

p = size(a,1);
n = size(Y,1);
m = size(X,2);


F = Y - feval(fcn,X,a);
Fa = zeros(n,p);
for k = 1:p
    Fa(:,k) = - feval(fcn,X,a,0,k);
end

% L is the matrix of weights
L = 1;
if size(dY,1) ~= 0
    L = dY.^2;
    % if there is X uncertainty, it must be added in
    if size(dX,1) ~= 0
        for j = 1:m
            L = L + dX(:,j).^2 .* feval(fcn,X,a,m).^2;
        end
    end
end

CVResid(a,a0,da0);

da = -C\V;