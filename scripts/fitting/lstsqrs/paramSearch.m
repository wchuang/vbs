function f = paramSearch(fcn,X,dX,Y,dY,a,a0,da0,kl,nsig,res,imax)

% paramSearch(fcn,X,dX,Y,dY,a,a0,da0,kl,nsig,res) 
%
% Show surface plot of exp(-S) in 2 parameter dimensions
%   most args are lstsqrs args
%   kl = param nums to show [1 2]
%   nsig = nsigma range of each param [2] can be vec : [1 2]
%   res = number of bins, [40] or vec [20 50]
%

global n p m F Fa L C V S ARec SRec

if nargin<9, kl = [1 2]; end;
if nargin<10, nsig = 2; end;
if nargin<11, res = 40; end;
if nargin<12, imax = inf; end;

if size(a,1) == 1, a = a'; end;
if size(a0,1) == 1, a0 = a0'; end;
if size(da0,1) == 1, da0 = da0'; end;

[A dA] = lstsqrs(fcn,X,dX,Y,dY,a,a0,da0,imax);
delA = [0 0];
delA(1) = dA(kl(1));
delA(2) = dA(kl(2));

pridelA(1) = da0(kl(1));
pridelA(2) = da0(kl(2));
priA(1) = a0(kl(1));
priA(2) = a0(kl(2));


rangeA = delA .* nsig;
stepA = delA ./ res;

% get the actual parameter values
pA1 = A(kl(1));
pA2 = A(kl(2));

er1L = pA1 - delA(1);
er1H = pA1 + delA(1);
er2L = pA2 - delA(2);
er2H = pA2 + delA(2);

A1L = pA1 - rangeA(1);
A1H = pA1 + rangeA(1);
A1I = stepA(1);
A2L = pA2 - rangeA(2);
A2H = pA2 + rangeA(2);
A2I = stepA(2);

[A1 A2] = meshgrid((A1L:A1I:A1H),(A2L:A2I:A2H));

Sgrid = zeros(size(A1));
for k = 1:size(A1,1)
    for m = 1:size(A1,2)
        A(kl(1)) = A1(k,m);
        A(kl(2)) = A2(k,m);
        Sgrid(k,m) = SResid(fcn,X,dX,Y,dY,A,a0,da0);
    end
end

Prob = exp(-Sgrid)/exp(-S);
alph = sqrt(Prob);

f = figure;
hold on
surf(A1,A2,Prob,'FaceColor','interp','EdgeColor','none',...
  'FaceLighting','phong','FaceAlpha','interp',...
  'AlphaDataMapping','none','AlphaData',alph)
contour3(A1,A2,Prob,[1,exp(-1),exp(-4)])

% Put the parameters and uncertainties along the axes with lines going into
% the optimal value
plot(A1L,pA2,'ro','MarkerSize',[10],'LineWidth',[1]); 
plot([A1L A1L],[er2L er2H],'r-','LineWidth',[1.5]);
plot([A1L pA1],[pA2 pA2],'r--','LineWidth',[1]);
plot(pA1,A2L,'ro','MarkerSize',[10],'LineWidth',[1]);
plot([er1L er1H],[A2L A2L],'r-','LineWidth',[1.5]);
plot([pA1 pA1],[A2L pA2],'r--','LineWidth',[1]);
plot(pA1,pA2,'wo','MarkerSize',[10],'LineWidth',[1]);

% If there  are a-priori constraints on BOTH params, show them
if pridelA(1) ~= inf & pridelA(2) ~= inf
  plot(priA(1),priA(2),'kx','MarkerSize',[10],'LineWidth',[2]);
  Prigrid = zeros(size(A1));
  ER1 = (A1 - priA(1)).^2 / pridelA(1)^2;
  ER2 = (A2 - priA(2)).^2 / pridelA(2)^2;
  Prigrid = exp(-(ER1+ER2));
  contour3(A1,A2,Prigrid,[exp(-.1),exp(-1),exp(-4)])
end

ax = gca;

lab = get(ax,'XLabel');
set(lab,'String',sprintf('a%d',kl(1)));
lab = get(ax,'YLabel');
set(lab,'String',sprintf('a%d',kl(2)));

ax = gca;
set(ax,'Box','on');
set(ax,'FontSize',14);
xl = get(ax,'XLabel');
set(xl,'FontSize',16);
yl = get(ax,'YLabel');
set(yl,'FontSize',16);


hold off