function f = testDeriv(fcn,X,a,n,p,s)

if size(a,1) == 1, a = a'; end;
if nargin < 4, n = 0; end
if nargin < 5, p = 0; end
if nargin < 6, s = 1; end

fd = finiteDiff(fcn,X,a,n,p,s);
f = ( fd - feval(fcn,X,a,n,p) )./ fd;