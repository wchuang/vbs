function [A, Unc, sVar, Corr] = lstsqAnal(fcn,X,dX,Y,dY,a,a0,da0,imax)

% lstsqAnal(fcn,X,dX,Y,dY,a,a0,da0) Least Squares Analysis
%
%
global n p m F Fa L C V S ARec SRec Cinv

% Do a little conditioning
if nargin < 7, a0 = a; end;
if nargin < 8, da0 = inf * a0 ; end;
if nargin < 9, imax = inf; end;

% Parameters need to be column vectors
if size(a,1) == 1, a = a'; end;
if size(a0,1) == 1, a0 = a0'; end;
if size(da0,1) == 1, da0 = da0'; end;

% Actually find the new parameters
A = lstsqrs(fcn,X,dX,Y,dY,a,a0,da0,imax);

% The var-covar matrix, Cinv, is calcualted in lstsqrs
Corr = Cinv;

Var = diag(Cinv);
sVar = sqrt(Var);

Unc = sqrt(Var * (S/(n-p) ));
for k = 1:p
    Corr(:,k) = Corr(:,k) ./ sVar;
    Corr(k,:) = Corr(k,:) ./ sVar';
end



disp('');
disp('Least Squares Results for the Function:')
fcnName = func2str(fcn);
disp(fcnName)
fprintf('Sum of squared residuals (S):%.2f\n',S);
fprintf('Number of degrees of freedom (n-p):%d\n',n-p);
fprintf('\n\t%9s %10s %10s %9s %10s\n','<a>','<da>','<var>1/2','<a0>','<da0>');
fprintf('\t%9.4g %10.5g %10.5g %9.4g %10.5g\n',...
  [A'; Unc'; sVar';a0';da0']);
disp(' ');

% look for an interpretation function and execute it if it exists
interpName = [fcnName 'Interp'];
if exist(interpName) == 2, feval(interpName,A,Unc,sVar); end;

