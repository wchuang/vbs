a0 = [10,-3];
X = squeeze((3:.1:13)');
Y = ones(size(X,1),1);
dY = 3 * ones(size(X,1),1);
Y(:,1) = a0(1) * exp(X/a0(2)) + 3 * randn(size(X));
