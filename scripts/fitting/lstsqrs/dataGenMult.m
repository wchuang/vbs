a0 = [10,-1,0,0]';
da0 = [Inf, Inf, 1, 1]';
a = [10, -1, -0.75, .75]';
dy = 0.25;

X1 = (2:6)';
Y1 = poly1(X1,a) + a(3) + dy * randn(size(X1,1),1);
dY1 = dy * ones(size(X1,1),1);

X2 = (5:9)';
Y2 = poly1(X2,a) + a(4) + dy * randn(size(X2,1),1);
dY2 = dy * ones(size(X2,1),1);

X = [X1 ones(size(X1,1),1); X2 2 * ones(size(X2,1),1) ];
Y = [Y1; Y2];
dY =[dY1; dY2];
