function [a, da] = lstsqrs(fcn,X,dX,Y,dY,a,a0,da0,imax)

% lstsqrs(X,Y,a) Least Squares Treatment
%
%
global n p m F Fa L C V S ARec SRec Cinv

% Default arguments
if nargin < 7, a0 = a; end;
if nargin < 8, da0 = inf * a0 ; end;
if nargin < 9, imax = inf; end;

% Do a little conditioning
if size(a,1) == 1, a = a'; end;
if size(a0,1) == 1, a0 = a0'; end;
if size(da0,1) == 1, da0 = da0'; end;

% p is num parameters
% n is num data points
% m is num dimensions in ind variable (X)
p = size(a,1);
n = size(Y,1);
m = size(X,2);


S0 = SResid(fcn,X,dX,Y,dY,a,a0,da0);
ARec = a;
SRec = S0;

% Simple-minded minimization search for objective function
% S = weighted sum squares
% This has a line search step, at least
condError = 1;
errorLimit = 1e-4;
icnt = 0;
while (condError > errorLimit) & (icnt < imax)
    % Estimate changes in parameters (dA) to get direction
    dA0 = lstsqInv(fcn,X,dX,Y,dY,a,a0,da0);
    dA = dA0;
    condError = sum( abs(dA ./ (a+eps)));
    S = SResid(fcn,X,dX,Y,dY,a+dA,a0,da0);
    lam = 0.5;
    % S0 is original for this step, so if things got worse
    % go in the estimated direction but cut back on step size
    while (S > S0 & lam > 0.01)
      dA = lam*dA0; lam = 0.5 * lam;
      S = SResid(fcn,X,dX,Y,dY,a+dA,a0,da0);
    end
    % now update parameters and objFcn and record
    a = a+dA;
    S0 = S;
    ARec = [ARec a];
    SRec = [SRec S];
    icnt = icnt + 1;
end

% Calculate var-covar matrix (inv(C) and uncertainty in parameters 
Cinv = inv(C);
da = sqrt(diag(Cinv) * S / (n-p));
