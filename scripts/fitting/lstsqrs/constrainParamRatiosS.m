function Ssq = constrainParamRatiosS(a,rat,i,j,daij)

% constraint derived from
% ai = rat * a(j)
% so ai / aj = rat
% so rat - ai/aj = 0
% but quadratic constraint is
% (rat - ai/aj)^2 / sig^2

% Note -- it is a pretty darn good idea to have the first guess with
% constraints obey the ratio constraints!!


Ssq = ((rat - a(i)/a(j))/daij)^2;
