function Ssq = constrainParamRatiosCV(a,rat,i,j,daij)

% constraint derived from
% ai = rat * a(j)
% so ai / aj = rat
% so rat - ai/aj = 0
% but quadratic constraint is
% (rat - ai/aj)^2 / sig^2

% Note -- it is a pretty darn good idea to have the first guess with
% constraints obey the ratio constraints!!

global F Fa L C V



% add constraints on the ratios
aij = (rat-a(i)/a(j));

% First derivatives -- 1/2 dS/da
V(i) = V(i) - (1/a(2))* aij / (daij^2);
V(j) = V(j) + a(1)/(a(2)^2) * aij /  (daij^2);

% Second derivatives -- 1/2 d^2S / da^2
C(i,i) = C(i,i) + 1 / (a(j)^2) /  (daij^2);
C(j,j) = C(j,j) + ( a(i)^2/(a(j)^4) - 2 * (a(i)/ (a(j)^3)) * aij)  /  (daij^2);

% Note, we fill the C matric in the upper left, so we need to make sure j>i
if (j>1)
  C(i,j) = C(i,j) + ( aij / a(j)^2 - a(i)/(a(j)^3) )  /  (daij^2);
else
  C(j,i) = C(j,i) + ( aij / a(j)^2 - a(i)/(a(j)^3) )  /  (daij^2);
end
