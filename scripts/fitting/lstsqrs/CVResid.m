function CVResid(a,a0,da0)

% Build up the coefficient matrix and offset array 
% Cij is (1/2) (d^2 S / dai daj) (neglecting 2nd order terms)
% Vi is (1/2) (d S / dai)
% This can be calculated from any objective function!
%

global F Fa L C V

p = size(a,1);


C = zeros(p,p);
V = zeros(p,1);

for k = 1:p
    for l = p:-1:k
        C(k,l) = sum(Fa(:,k).*Fa(:,l)./L);
    end
    V(k) = sum(Fa(:,k).*F./L);
    if da0(k) ~= inf
        V(k) = V(k) + (a(k)-a0(k))/da0(k)^2; 
        C(k,k) = C(k,k) + da0(k)^-2;
    end
end

% Get the symmetric elements of the coefficient matrix
for k = 1:p
    for l = k-1:-1:1
        C(k,l) = C(l,k);
    end
end
