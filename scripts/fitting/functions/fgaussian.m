function f = fgaussian(X,a,n,p)
% fgaussian gaussian function
% fgaussian(X,a) returns y = a(1)exp(-X/2 a(2)^2)
% 

if size(a,1) == 1, a = a'; end;
if nargin < 3, n = 0; end
if nargin < 4, p = 0; end

% The x derivatives of the function
switch n
    case 1, f = -a(1)/a(2)^2 * X .* exp(-1/2 * (X/a(2)).^2); return;
end

% The a derivatives of the function
switch p
    case 1, f = exp(-1/2 .* (X/a(2)).^2); return;
    case 2, f = a(1)/(a(2)^3) * X.^2 .* exp(-1/2 * (X/a(2)).^2); return; 
end

% The function
f = a(1) * exp(-1/2 * (X/a(2)).^2);
