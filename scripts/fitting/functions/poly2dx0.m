function f = poly2dx0(X,a,n,p)
% POLY2dx0 Second order polynomial with offset
% poly2dx0(X,a) returns y = a(2) + a(3) * (X-a(1))^2 + ...
% poly2dx0(X,a,n) returns the derivative of poly2dx0 at X

if size(a,1) == 1, a = a'; end;
if nargin < 3, n = 0; end
if nargin < 4, p = 0; end


if (p~=2) 
    Xa = X-a(1);
end

% The x derivatives of the function
switch n
    case 1, f = 2*a(3)*Xa;, return;
end

% The a derivatives of the function
switch p
    case 1, f = -2*a(3)*Xa;, return;
    case 2, f = ones(size(X));, return;
    case 3, f = Xa.^2;, return; 
end

% The function
f = a(2) + a(3)*Xa.^2;
