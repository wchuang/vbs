function f = poly1Offset(X,a,n,p)
% poly1Offset First order polynomial with offset
% poly1Offset(X,a) = a(1) + a(2) * X(:,1) + off(a(2...))
%    off is an offset, off = a(n), n = X(:,2)+2
%    in general, the 2nd dimension of X is an index
%       identifying a dataset.

if size(a,1) == 1, a = a'; end;
if nargin < 3, n = 0; end
if nargin < 4, p = 0; end

ndata = size(X,1);
nparams = size(a,1);

% The x derivatives of the function
switch n
    case 1, f = a(2); return;
end

% The a derivatives of the function
switch p
    case 1, f = ones(size(X,1),1); return;
    case 2, f = X(:,1); return;
end
if p > 2
    f = ~(X(:,2) + 2 - p);
    return;
end

% The function
f =  a(1) + a(2) * X(:,1) + a(X(:,2)+2);
