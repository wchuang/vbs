function f = fexpgau(X,a,n,p)
% fgaussian exponential + gaussian sum
% 

if size(a,1) == 1, a = a'; end;
if nargin < 3, n = 0; end
if nargin < 4, p = 0; end

ae = a(1:2);
ag = a(3:4);

% The x derivatives of the function
switch n
    case 1, f = fexp(X,ae,1) + fgaussian(X,ag,1); return
end

% The a derivatives of the function
switch p
    case 1, f = fexp(X,ae,0,1); return;
    case 2, f = fexp(X,ae,0,2); return; 
    case 3, f = fgaussian(X,ag,0,1); return;
    case 4, f = fgaussian(X,ag,0,2); return;
end

% The function
f = fexp(X,ae) + fgaussian(X,ag);
