function f = poly1(X,a,n,p)
% POLY1 First order polynomial
% poly1(X,a) returns y = a(n+2) d(X(2))(a(1) + a(2) * X(1))
% poly1(X,a,n) returns the derivative of poly1 at X

if size(a,1) == 1, a = a'; end;
if nargin < 3, n = 0; end
if nargin < 4, p = 0; end

ndata = size(X,1);
nparams = size(a,1);
nsets = nparams - 2;

% Most of the parameters constitute a multiplier
%  The first thing we want to do is fill a multipier array
%  The second dimension of the X vector comprises indices
% The multiplier depends on the derivative being examined
% For the function or derivatives in X(:,1), a(1) or a(2),
% the multiplier is a(index+2), where index is X(:,2)
% For derivatives of the indexed multipliers a(3,...)
% The multiplier is either 1 or 0, depending on whether
% the index equals the parameter number being sought
if p < 3
    m = a(X(:,2) + 2);
else
    m = ~(X(:,2) + 2 - p);
end

% The x derivatives of the function
switch n
    case 1, f = m .* a(2); return;
end

% The a derivatives of the function
switch p
    case 1, f = m; return;
    case 2, f = m .* X; return;
end


% The function
%   Note that the p>2 derivatives are handled here too
f = m .* (a(1) + a(2) * X(:,1));
