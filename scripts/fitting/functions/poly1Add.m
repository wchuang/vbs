function f = poly1Add(X,a,n,p)
% POLY1 First order polynomial
% poly1(X,a) returns y = a(n+2) d(X(2))(a(1) + a(2) * X(1))
% poly1(X,a,n) returns the derivative of poly1 at X

if size(a,1) == 1, a = a'; end;
if nargin < 3, n = 0; end
if nargin < 4, p = 0; end

ndata = size(X,1);
nparams = size(a,1);

% The x derivatives of the function
switch n
    case 1, f = a(2); return;
end

% The a derivatives of the function
switch p
    case 1, f = ones(size(X,1),1); return;
    case 2, f = X(:,1); return;
end
if p > 2
    f = ~(X(:,2) + 2 - p);
    return;
end

% The function
f =  a(1) + a(2) * X(:,1) + a(X(:,2)+2);
