function f = poly0(X,a,n,p)
% POLY1 Zero order polynomial
% poly1(X,a) returns y = a(1) 
% poly1(X,a,n) returns the derivative of poly0 at X

if size(a,1) == 1, a = a'; end;
if nargin < 3, n = 0; end
if nargin < 4, p = 0; end


% The x derivatives of the function
switch n
    case 1, f = 0 .* X;, return;
end

% The a derivatives of the function
switch p
    case 1, f = ones(size(X));, return;
end

% The function
f = a(1) + 0 .* X;
