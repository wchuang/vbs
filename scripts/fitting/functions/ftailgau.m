function f = ftailgau(X,a,n,p)

% effing tailing gaussian
% 

if size(a,1) == 1, a = a'; end;
if nargin < 3, n = 0; end
if nargin < 4, p = 0; end


A = a(1);
x0 = a(2);
sig = a(3);
Ts = a(4);
Tex = a(5);
if Tex ~= 0, Ts = abs(Ts); end;

Xi = (X - x0)/(2 * sig);

rhs = X - x0 > 0;
T = 1 + (Ts*Xi.^Tex .* rhs);


% The x derivatives of the function
switch n
    case 1,
    f = finiteDiff(@ftailgau,X,a,n,p,1e-6);
    return;
end

% The a derivatives of the function
switch p
    case 1, f = exp(-Xi.^2./T); return;
    case 2, f = finiteDiff(@ftailgau,X,a,n,p,1e-3); return; 
    case 3, f = finiteDiff(@ftailgau,X,a,n,p,1e-3); return;
    case 4, f = finiteDiff(@ftailgau,X,a,n,p,1e-3); return;
    case 5, f = finiteDiff(@ftailgau,X,a,n,p,1e-3); return;
end

% The function
f = A * exp(-Xi.^2./T);
