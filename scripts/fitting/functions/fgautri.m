function f = fgautri(X,a,n,p)

% fgaussian gaussian + triangle sum
% 

if size(a,1) == 1, a = a'; end;
if nargin < 3, n = 0; end
if nargin < 4, p = 0; end

ag = a(1:2);
ap = a(3:4);

% The x derivatives of the function
switch n
    case 1, f = fgaussian(X,ag,1) + ftri(X,ap,1); return
end

% The a derivatives of the function
switch p
    case 1, f = fgaussian(X,ag,0,1); return;
    case 2, f = fgaussian(X,ag,0,2); return;
    case 3, f = ftri(X,ap,0,1); return;
    case 4, f = ftri(X,ap,0,2); return; 
end

% The function
f = fgaussian(X,ag) + ftri(X,ap);
