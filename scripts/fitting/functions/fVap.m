function f = fVap(X,a,n,p)
% 
% 
% 

if size(a,1) == 1, a = a'; end;
if nargin < 3, n = 0; end
if nargin < 4, p = 0; end

% Make the variables easier to read, even if less efficient
T = X(:,1);
A = a(1);
Ea = a(2);
Ao = 1e20;

% Again, make some functional substitutions
% this will help efficiency and readability
eEa = exp(-Ea./T);

% The x derivatives of the function
switch n
    case 1, f = Ea ./ T.^2 .* A .* Ao .* eEa; return;
end

% The a derivatives of the function
switch p
    case 1, f = Ao .* eEa; return;
    case 2, f = - 1 ./ T .* A .* Ao .* eEa;
end

% The function
f = A .* Ao .* eEa;
