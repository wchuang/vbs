function f = fexppow(X,a,n,p)
% fexppow exponential power function
% fgaussian(X,a) returns y = a(1)exp(X/2 a(2)^2)
% 

if size(a,1) == 1, a = a'; end;
if nargin < 3, n = 0; end
if nargin < 4, p = 0; end

% The x derivatives of the function
switch n
  case 1, f = -(a(3)-1) * a(1)/a(2)^a(3) * X .* ...
    exp(-(X/a(2)).^a(3)); return;
end

% The a derivatives of the function
switch p
  case 1, f = exp(-(X/a(2)).^a(3)); return;
  case 2, f = a(1)*a(2)^(-(a(3)+1)) * X.^a(3) .* ...
    exp(-(X/a(2)).^a(3)); return;
  case 3, f = -a(1)*(X/a(2)).^a(3) .* log(X/a(2)) .* ...
    exp(-(X/a(2)).^a(3)); return;
end

% The function
f = a(1) * exp(-(X/a(2)).^a(3));
