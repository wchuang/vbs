function f = ftri(X,a,n,p)
% tri First order polynomial triangle
% poly1(X,a) returns y = a(1) + a(2) * X
% poly1(X,a,n) returns the derivative of poly1 at X

if size(a,1) == 1, a = a'; end;
if nargin < 3, n = 0; end
if nargin < 4, p = 0; end

trimask = X < a(2);

% The x derivatives of the function
switch n
    case 1, f = - a(1) / a(2) .* trimask; return;
end

% The a derivatives of the function
switch p
    case 1, f = (1 - X/a(2)) .* trimask; return;
    case 2, f = -a(1)*X/a(2)^2 .* trimask; return; 
end

% The function
f = a(1) * (1 - X/a(2)) .* trimask;
