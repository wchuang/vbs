addpath( [scriptDirCMU, 'ftir/instr']);
addpath( [scriptDirCMU, 'ftir/igm']);
addpath( [scriptDirCMU, 'ftir/spect']);
addpath( [scriptDirCMU, 'ftir/lines']);
addpath( [scriptDirCMU, 'ftir/hitran']);
addpath( [scriptDirCMU, 'ftir/sigma']);
addpath( [scriptDirCMU, 'ftir/mols']);
addpath( [scriptDirCMU, 'ftir/anal']);
addpath( [scriptDirCMU, 'ftir/anal/lag']);
addpath( [scriptDirCMU, 'ftir/anal/offset']);
addpath( [scriptDirCMU, 'ftir/anal/onion']);
addpath( [scriptDirCMU, 'ftir/anal/ILF']);
addpath( [scriptDirCMU, 'ftir/anal/resids']);
addpath( [scriptDirCMU, 'ftir/process']);
addpath( [scriptDirCMU, 'ftir/rms']);

