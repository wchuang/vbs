whereThingsAreFitting

% Add to the end so that functions in analysis and analysis.std override
addpath ([scriptDirCMU, 'semiVolatile/functions'],'-end');
addpath ([scriptDirCMU, 'semiVolatile/partitioning']);

addpath ([scriptDirCMU, 'semiVolatile/2D'],'-end');
addpath ([scriptDirCMU, 'semiVolatile/2D/plotting'],'-end');



