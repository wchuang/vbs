if exist('./analysis.std') == 7
    if exist('./analysis.std/matlab') == 7
        addpath ./analysis.std/matlab
    end
    addpath ./analysis.std
end
if exist('./analysis') == 7
    if exist('./analysis/matlab') == 7
        addpath ./analysis/matlab
    end
    addpath ./analysis
end

if exist('./results.std') == 7
    addpath ./results.std
end

if exist('./results') == 7
    addpath ./results
end

if exist('./overrides') == 7
    addpath ./overrides
end

startupAnalysis;
